﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    public abstract class AnimParam
    {
        protected Anim.Controller _controller;

        public bool IsUpdated { get; private set; }
        public virtual void Update() { }
        public virtual void TerminateAnimation() { }

        internal void MakeNeedUpdate()
        {
            IsUpdated = false;
            if (_controller != null)
                _controller.MakeNeedUpdate();
        }
    }

    public class AnimFloat : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat>).AnimParam = this;
            }
            return _controller as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        private float _value;
        public float Value
        {
            get { return _value; }
            set { _value = value; _oldValue = value; }
        }

        float _oldValue;
        public override void Update()
        {
            base.Update();
            if (_controller != null) { _controller.Update(); _oldValue = _value; }
            else { Value = _oldValue; }
        }
        public AnimFloat()
        { _value = 0.0f; }
        public AnimFloat(float v)
        { Value = v; }
        public static implicit operator AnimFloat(float v)
        { return new AnimFloat(v); }
    }
    public class AnimFloat2 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat2>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat2>).AnimParam = this;
            }
            return _controller as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        private Vector2 _value;
        public Vector2 Value
        {
            get { return _value; }
            set { _value = value; _oldValue = value; }
        }
        public float X { get { return _value.X; } set { _value.X = value; _oldValue = _value;} }
        public float Y { get { return _value.Y; } set { _value.Y = value; _oldValue = _value; } }

        Vector2 _oldValue;
        public override void Update()
        {
            base.Update();
            if (_controller != null) { _controller.Update(); _oldValue = _value; }
            else { Value = _oldValue; }
        }
        public AnimFloat2()
        { _value = new Vector2(); }
        public AnimFloat2(Vector2 v)
        { Value = v; }
        public static implicit operator AnimFloat2(Vector2 v)
        { return new AnimFloat2(v); }
    }
    public class AnimFloat3 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat3>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat3>).AnimParam = this;
            }
            return _controller as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        private Vector3 _value;
        public Vector3 Value
        {
            get { return _value; }
            set { _value = value; _oldValue = value; }
        }

        Vector3 _oldValue;
        public override void Update()
        {
            base.Update();
            if (_controller != null) { _controller.Update(); _oldValue = _value; }
            else { Value = _oldValue; }
        }
        public AnimFloat3()
        { _value = new Vector3(); }
        public AnimFloat3(Vector3 v)
        { Value = v; }
        public static implicit operator AnimFloat3(Vector3 v)
        { return new AnimFloat3(v); }
    }
    public class AnimFloat4 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat4>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat4>).AnimParam = this;
            }
            return _controller as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        private Vector4 _value;
        public Vector4 Value
        {
            get { return _value; }
            set { _value = value; _oldValue = value; }
        }

        Vector4 _oldValue;
        public override void Update()
        {
            base.Update();
            if (_controller != null) { _controller.Update(); _oldValue = _value; }
            else { Value = _oldValue; }
        }
        public AnimFloat4()
        { _value = new Vector4(); }
        public AnimFloat4(Vector4 v)
        { Value = v; }
        public static implicit operator AnimFloat4(Vector4 v)
        { return new AnimFloat4(v); }
    }
}
