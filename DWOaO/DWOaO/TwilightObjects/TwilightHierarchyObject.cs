﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DWOaO.Anim;

namespace DWOaO
{
    public class TwilightHierarchyObject : TwilightObject
    {
        TwilightObject parentObject;
        protected List<TwilightHierarchyObject> childObjects = new List<TwilightHierarchyObject>();
        public void SetParentObject(TwilightObject obj)
        {
            parentObject = obj;
            this.Position.Ctrl<V2HierarchyPosCtrl>().This = this;
            this.Position.Ctrl<V2HierarchyPosCtrl>().Parent = obj;
            this.Position.Ctrl<V2HierarchyPosCtrl>().Cfg(Position.Value);
            this.Rotation.Ctrl<FloatHierarchyAngleCtrl>().Parent = obj;
            this.Rotation.Ctrl<FloatHierarchyAngleCtrl>().Cfg(Rotation.Value);
        }

        public class V2HierarchyPosCtrl : Controller<AnimFloat2> // must be assigned to Location parameter!!!
        {
            public AnimFloat2 To { get; private set; }
            public AnimFloat2 From { get; private set; }
            public AnimFloat RotationRef { get; private set; }
            public float Time { get; private set; }
            public Misc.Anim AnimType { get; private set; }

            public TwilightObject Parent { get; set; }
            public TwilightObject This { get; set; }

            public override void Update()
            {
                if (IsRunning)
                {
                    base.Update();
                    //Parent.Rotation.Update();
                    //Parent.Origin.Update();
                    //Parent.Position.Update();

                    elapsedTime += GetTime();
                    if (elapsedTime > Time && AnimType != Misc.Anim.None)
                    {
                        IsRunning = false; AnimParam.Value = To.Value; return;
                        //Scriptable.UnlockScripting!
                    }
                    switch (AnimType)
                    {
                        case DWOaO.Misc.Anim.Linear:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Linear(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Linear(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case DWOaO.Misc.Anim.Bezier3:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Bezier3(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Bezier3(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case Misc.Anim.None:
                            AnimParam.Value = To.Value;
                            break;
                        default:
                            throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
                    }
                    float x1, x2, y1, y2, distance, alpha, beta;
                    Vector2 line = (This.Origin.Value - Parent.Origin.Value);
                    distance = line.Length();
                    x1 = line.X; y1 = line.Y;
                    alpha = (float)Math.Acos(x1 / distance) * Math.Sign(y1);
                    beta = alpha + MathHelper.ToRadians(Parent.Rotation.Value);
                    x2 = distance * (float)Math.Cos(beta);
                    y2 = distance * (float)Math.Sin(beta);
                    AnimParam.X -= x1 - x2;
                    AnimParam.Y -= (y1 - y2);
                    AnimParam.Value += Parent.Position.Value;
                }

            }
            public void Cfg(AnimFloat2 val)
            {
                AnimParam.Value = val.Value;
                From = AnimParam.Value; To = AnimParam.Value; Time = 0; AnimType = Misc.Anim.None;
                animParams.Clear();
                Start();
            }
            public V2HierarchyPosCtrl Cfg(Misc.Anim at, AnimFloat2 to, float time)
            {
                From = AnimParam.Value; To = to; Time = time; AnimType = at;
                animParams.Clear();
                animParams.AddRange(new AnimParam[] { From, To });
                Start(); return this;
            }
            public V2HierarchyPosCtrl()
            {
            }
        }

        public class FloatHierarchyAngleCtrl : Controller<AnimFloat> // must be assigned to Rotation parameter!!!
        {
            public AnimFloat To { get; private set; }
            public AnimFloat From { get; private set; }
            public float Time { get; private set; }
            public Misc.Anim AnimType { get; private set; }

            public TwilightObject Parent { get; set; }

            public void Cfg(AnimFloat to)
            {
                AnimParam.Value = to.Value;
                From = AnimParam.Value; To = AnimParam.Value; Time = 0; AnimType = Misc.Anim.None;
                animParams.Clear();
                Start();
            }
            public FloatHierarchyAngleCtrl Cfg(AnimFloat to, float time, Misc.Anim at = Misc.Anim.Linear, bool isAdditive = true)
            {
                From = AnimParam.Value - Parent.Rotation.Value; To = isAdditive ? To.Value + to.Value : to.Value; Time = time; AnimType = at;
                animParams.Clear();
                animParams.AddRange(new AnimParam[] { From, To });
                Start();
                return this;
            }
            public override void Update()
            {
                if (!IsRunning)
                    return;
                base.Update();
                //Parent.Rotation.Update();
                elapsedTime += GetTime();
                if (elapsedTime > Time && AnimType != Misc.Anim.None)
                {
                    Cfg(To.Value); UnlockScript(); IsRunning = true; return;
                    //return;
                    //Scriptable.UnlockScripting!
                }
                switch (AnimType)
                {
                    case DWOaO.Misc.Anim.Linear:
                        AnimParam.Value = AnimCalc.Linear(From.Value, To.Value, elapsedTime / Time) + Parent.Rotation.Value;
                        break;
                    case DWOaO.Misc.Anim.Bezier3:
                        AnimParam.Value = AnimCalc.Bezier3(From.Value, To.Value, elapsedTime / Time) + Parent.Rotation.Value;
                        break;
                    case Misc.Anim.None:
                        AnimParam.Value = To.Value + Parent.Rotation.Value;
                        break;
                    default:
                        throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
                }
            }

            public FloatHierarchyAngleCtrl()
            {
            }
        }

        public TwilightHierarchyObject(AnimTexture tex)
            : base(tex, new Vector2(tex.Width, tex.Heigth)) 
        { }
        public TwilightHierarchyObject(AnimTexture tex, Vector2 size)
            : base(tex, size)
        { }
        public TwilightHierarchyObject(AnimTexture tex, Vector2 size, Color rgba)
            : base(tex, size, rgba)
        { }

        public TwilightHierarchyObject.FloatHierarchyAngleCtrl RC()
        { return this.Rotation.Ctrl<TwilightHierarchyObject.FloatHierarchyAngleCtrl>(); }
    }
}
