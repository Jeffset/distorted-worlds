﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    public class TwilightObject : Animatable
    {
        public AnimTexture Texture { get; protected set; }
        public AnimFloat2 Position { get; private set; }
        public AnimFloat3 RGBMulti { get; private set; }
        public AnimFloat2 Size { get; private set; }
        public AnimFloat Rotation { get; private set; }
        public AnimFloat2 Origin { get; private set; }
        public AnimFloat Opacity { get; private set; }
        public SpriteEffects Flip { get; set; }

        public bool RestoreLocationOrigin { get; set; }

        private float _level = 0.0f;
        public float Level
        {
            get { return _level; }
            set { _level = value; Global.Twilight.ChangeLevel(this); }
        }

        public int Layer { get; set; }
        internal LinkedListNode<TwilightObject> _engineEntry;

        private void _Init()
        {
            Position = new AnimFloat2();
            Size = new AnimFloat2();
            RGBMulti = new AnimFloat3(new Vector3(1.0f));
            Rotation = new AnimFloat();
            Origin = new AnimFloat2(new Vector2(0.5f));
            Opacity = new AnimFloat(1.0f);
            Flip = SpriteEffects.None;
            RestoreLocationOrigin = true;
        }

        protected TwilightObject()
        {
            _Init();
            Layer = 1;
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Size, Rotation, Position });
        }

        public TwilightObject(AnimTexture tex, Vector2 size)
        {
            _Init();
            Layer = 1;
            Texture = tex;
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Rotation, Size, Position, Texture });
            Size.Value = size;
        }

        public TwilightObject(AnimTexture tex, Vector2 size, Color rgba)
        {
            _Init();
            Layer = 1;
            Texture = tex;
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Rotation, Size, Position, Texture });
            Size.Value = size;
            RGBMulti.Value = rgba.ToVector3();
        }

        private Rectangle _GetDestRect()
        {
            Rectangle r = Bounds;
            r.X += (RestoreLocationOrigin ? (int)Origin.X : 0) - Global.Twilight.Camera.X;
            r.Y += (RestoreLocationOrigin ? (int)Origin.Y : 0) - Global.Twilight.Camera.Y;
            return r;
        }
        public virtual void Render()
        {
            Texture2D texture; Rectangle? sourceRect; Shader finalShader;
            Texture.GetTextureForFinalRender(out texture, out sourceRect, out finalShader);
            if (finalShader.ShaderEffect != null)
                TwilightEngine.Sprite.Begin(SpriteSortMode.Immediate, finalShader.BlendingOptions, null, null, null, finalShader.ShaderEffect);
            else
                TwilightEngine.Sprite.Begin(SpriteSortMode.Immediate, finalShader.BlendingOptions);
            TwilightEngine.Sprite.Draw(texture, _GetDestRect(), sourceRect, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, Flip, 1.0f);
            TwilightEngine.Sprite.End();
        }
        public virtual void RenderAsSubobject()
        {
            TwilightEngine.Sprite.Draw(Texture.Texture, _GetDestRect(), null, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, Flip, Level);
        }

        public int X
        {
            get { return (int)Position.Value.X; }
            set { Position.Value = new Vector2(value, Position.Value.Y); }
        }
        public int Y
        {
            get { return (int)Position.Value.Y; }
            set { Position.Value = new Vector2(Position.Value.X, value); }
        }
        public int Width
        {
            get { return (int)Size.Value.X; }
            set { Size.Value = new Vector2(value, Size.Value.Y); }
        }
        public int Height
        {
            get { return (int)Size.Value.Y; }
            set { Size.Value = new Vector2(Size.Value.X, value); }
        }
        public Rectangle Bounds { get { return new Rectangle(X, Y, Width, Height); } }
        public int Left { get { return X; } }
        public int Right { get { return X + Width; } }
        public int Top { get { return Y; } }
        public int Bottom { get { return Y + Height; } }
    }
}
