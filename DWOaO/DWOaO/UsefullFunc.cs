﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.Misc
{
    public static class UF
    {
        public static void PutOrigin(TwilightObject o)
        {
            o.Origin.Value = o.Size.Value * 0.5f;
        }
        public static void PutOrigin(TwilightObject o, Vector2 relation)
        {
            o.Origin.Value = o.Size.Value * relation;
        }
    }
}
