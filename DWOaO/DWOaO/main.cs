using System;

namespace DWOaO
{
    static class main
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
#if !DEBUG || INTEGRATED_TEST
            using (XNAPlatform game = new XNAPlatform(Misc.GraphicMode.Fullscreen))
            {
                // ����� ������ ���� �������� TwilightEngine3 ����, � ��� ������� � game.
                // ����� � Game.Initialize() ����� ��������� ������������� ������ (engine.Construct()).
                // ����� ���� ����� ��������� ������� ������ (����� ��� ���������.) � ������ � game.
                // �� ���������� ���� ������� ������ ����� ���������� �� ������ ����
                // � ��� �� �����, ���� ������������ �� ������ �� ������ ���� (����� game.Exit()).
                DWOaO.Life.Storyline.Episode1.Glava_I_Intro begining = new Life.Storyline.Episode1.Glava_I_Intro();
                game.GameEngine = begining;
                game.Run();
            }
#else 
            using (XNAPlatform game = new XNAPlatform(Misc.GraphicMode.Windowed))
            {
                // ����� ������ ���� �������� TwilightEngine3 ����, � ��� ������� � game.
                // ����� � Game.Initialize() ����� ��������� ������������� ������ (engine.Construct()).
                // ����� ���� ����� ��������� ������� ������ (����� ��� ���������.) � ������ � game.
                // �� ���������� ���� ������� ������ ����� ���������� �� ������ ����
                // � ��� �� �����, ���� ������������ �� ������ �� ������ ���� (����� game.Exit()).
                DWOaO.Life.Storyline.Episode1.Glava_I_Intro begining = new Life.Storyline.Episode1.Glava_I_Intro();
                game.GameEngine = begining;
                game.Run();
            }
#endif
        }
    }
}

