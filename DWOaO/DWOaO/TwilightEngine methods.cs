﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    public partial class TwilightEngine : Game
    {
        public void RegisterObject(TwilightObject obj)
        {
            if (obj._engineEntry != null) return;
            LinkedList<TwilightObject> layer = objects[obj.Layer];
            LinkedListNode<TwilightObject> node = layer.First;
            while (node != null && node.Value.Level <= obj.Level)
                node = node.Next;
            if (node == null)
            {
                layer.AddLast(obj);
                obj._engineEntry = layer.Last;
            }
            else
            {
                layer.AddBefore(node, obj);
                obj._engineEntry = node.Previous;
            }
            
        }
        public void UnregisterObject(TwilightObject obj)
        {
            if (obj._engineEntry == null) return;
            objects[obj.Layer].Remove(obj._engineEntry);
            obj._engineEntry = null;
        }
        public void ChangeLevel(TwilightObject obj)
        {
            if (obj._engineEntry == null) return;
            LinkedList<TwilightObject> layer = objects[obj.Layer];
            layer.Remove(obj._engineEntry);
            LinkedListNode<TwilightObject> node = layer.First;
            while (node != null && node.Value.Level >= obj.Level)
                node = node.Next;
            if (node == null)
            {
                layer.AddLast(obj);
                obj._engineEntry = layer.Last;
            }
            else
            {
                layer.AddBefore(node, obj);
                obj._engineEntry = node.Previous;
            }
        }
    }
}
