﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.Misc
{
    public enum GraphicMode
    {
        Fullscreen = 1,
        Windowed = 0
    }
    public enum Anim
    {
        None = -1,
        Linear = 0, 
        Bezier2 = 1,
        Bezier3 = 2,
        bezierPath2 = 10,
        bezierPath3 = 20,
        bezierPath4 = 30
    }
}

namespace DWOaO.Anim
{
    public static class AnimCalc
    {
        public static float Linear(float From, float To, float t)
        {
            return (From + (float)(To - From) * t);
        }
        public static float Bezier3(float From, float To, float t)
        {
            return (((1.0f - t) * (1.0f - t) * (1.0f - t) * From) + (3.0f * (1.0f - t) * (1.0f- t) * t * From) + (3.0f * (1.0f - t) * t * t * To) + (t * t * t * To));
        }
        public static float BezierPath2(float From, float Direction, float To, float t)
        {
            return ((1.0f - t) * (1.0f - t) * From + 2.0f * (1.0f - t) * t * Direction + t * t * To);
        }
        public static float BezierPath3(float From, float Direction1, float Direction2, float To, float t)
        {
            return (((1.0f - t) * (1.0f - t) * (1.0f - t) * From) + (3.0f * (1.0f - t) * (1.0f - t) * t * Direction1) + (3.0f * (1.0f - t) * t * t * Direction2) + (t * t * t * To));
        }
    }
}

namespace DWOaO.ActorModels
{
    public enum ActorOrient
    {
        //Orientless = -1,
        ToUs = 0,
        ToRight = 1,
        ToForward = 2,
        ToLeft = 3
    }
    public enum ActorDefaultState
    {
        Standing = 0,
        Striking = 1,
        Runing = 2
    }

    public class ActorModelTextureSet
    {
        public string Body = "body";
        public string Head = "head";
        public string LeftArm = "left_arm";
        public string RightArm = "right_arm";
        public string LeftHand = "left_hand";
        public string RightHand = "right_hand";
        public string LeftLeg = "left_leg";
        public string RightLeg = "right_leg";
        public string LeftFoot = "left_foot";
        public string RightFoot = "right_foot";
        public string Weapon = "sword";
    }
}
