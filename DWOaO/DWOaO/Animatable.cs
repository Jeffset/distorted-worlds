﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    public abstract class Animatable
    {
        protected List<AnimParam> animParams = new List<AnimParam>();

        public virtual void Update()
        {
            foreach (AnimParam param in animParams)
            {
                if (!param.IsUpdated)
                    param.Update();
            }
        }

        internal void MakeNeedUpdate()
        {
            foreach (AnimParam param in animParams)
            {
                param.MakeNeedUpdate();
            }
        }
    }
}
