﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DWOaO
{
    public class Script<Deleg>
    {
        private class _ScriptStart { public object[] args; public Deleg scriptFct; }
        public static Script<Deleg> Start(Deleg script, params object[] parameters)
        {
            return new Script<Deleg>(script, parameters);
        }

        Thread scriptThread;

        private Script(Deleg script, object[] parameters)
        {
            scriptThread = new Thread(o => { ((o as _ScriptStart).scriptFct as Delegate).Method.Invoke(null, (o as _ScriptStart).args); });
            scriptThread.IsBackground = true;
            scriptThread.Priority = ThreadPriority.Highest;
            scriptThread.Start(new _ScriptStart { args = parameters, scriptFct = script });
        }
        
        public void Terminate()
        {
            Monitor.Enter(Global.Twilight);
            scriptThread.Abort();
            Monitor.Exit(Global.Twilight);
        }
    }
}
