﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    public abstract class TwilightObject : Animatable
    {
        public AnimTexture Texture { get; protected set; }
        public AnimFloat2 Position { get; private set; }
        public AnimFloat3 RGBMulti { get; private set; }
        public AnimFloat2 Size { get; private set; }
        public AnimFloat Rotation { get; private set; }
        public AnimFloat2 Origin { get; private set; }
        public AnimFloat Opacity { get; private set; }
        public SpriteEffects Flip { get; set; }

        private float _level = 0.0f;
        public float Level
        {
            get { return _level; }
            set { _level = value; Global.Twilight.ChangeLevel(this); }
        }

        public int Layer { get; protected set; }
        internal LinkedListNode<TwilightObject> _engineEntry;

        protected TwilightObject()
        {
            Position = new AnimFloat2();
            Size = new AnimFloat2();
            RGBMulti = new AnimFloat3(new Vector3(1.0f));
            Rotation = new AnimFloat();
            Origin = new AnimFloat2(new Vector2(0.5f));
            Opacity = new AnimFloat(1.0f);
            Flip = SpriteEffects.None;

            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Rotation, Size, Position });
        }

        private Rectangle _GetDestRect()
        {
            Rectangle r = Bounds;
            r.X += (int)Origin.Value.X - Global.Twilight.Camera.X;
            r.Y += (int)Origin.Value.Y - Global.Twilight.Camera.Y;
            return r;
        }
        public virtual void Render()
        {
            Texture2D texture; Rectangle? sourceRect; Shader finalShader;
            Texture.GetTextureForFinalRender(out texture, out sourceRect, out finalShader);
            if (finalShader.ShaderEffect != null)
                TwilightEngine.Sprite.Begin(SpriteSortMode.Immediate, finalShader.BlendingOptions, null, null, null, finalShader.ShaderEffect);
            else
                TwilightEngine.Sprite.Begin(SpriteSortMode.Immediate, finalShader.BlendingOptions);
            TwilightEngine.Sprite.Draw(texture, _GetDestRect(), sourceRect, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, Flip, 1.0f);
            TwilightEngine.Sprite.End();
        }

        public int X
        {
            get { return (int)Position.Value.X; }
            set { Position.Value = new Vector2(value, Position.Value.Y); }
        }
        public int Y
        {
            get { return (int)Position.Value.Y; }
            set { Position.Value = new Vector2(Position.Value.X, value); }
        }
        public int Width
        {
            get { return (int)Size.Value.X; }
            set { Size.Value = new Vector2(value, Size.Value.Y); }
        }
        public int Height
        {
            get { return (int)Size.Value.Y; }
            set { Size.Value = new Vector2(Size.Value.X, value); }
        }
        public Rectangle Bounds { get { return new Rectangle(X, Y, Width, Height); } }
        public int Left { get { return X; } }
        public int Right { get { return X + Width; } }
        public int Top { get { return Y; } }
        public int Bottom { get { return Y + Height; } }
    }
}
