﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    public class TwilightCamera : TwilightObject
    {
        public TwilightCamera(int width = -1, int height = -1)
        {
            if (width == -1) Width = TwilightEngine.GraphicsManager.PreferredBackBufferWidth;
            else
                Width = width;
            if (height == -1) Height = TwilightEngine.GraphicsManager.PreferredBackBufferHeight;
            else
                Height = height;
        }
    }
}
