﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.ActorModels
{
    public class ExtrIncMdl1 : ActorModel
    {
        public TwilightHierarchyObject Body { get { return bones[0]; } }
        public TwilightHierarchyObject Head { get { return bones[1]; } }
        public TwilightHierarchyObject LeftArm { get { return bones[2]; } }
        public TwilightHierarchyObject RightArm { get { return bones[3]; } }
        public TwilightHierarchyObject LeftHand { get { return bones[4]; } }
        public TwilightHierarchyObject RightHand { get { return bones[5]; } }
        public TwilightHierarchyObject LeftLeg { get { return bones[6]; } }
        public TwilightHierarchyObject RightLeg { get { return bones[7]; } }
        public TwilightHierarchyObject LeftFoot { get { return bones[8]; } }
        public TwilightHierarchyObject RightFoot { get { return bones[9]; } }
        public TwilightHierarchyObject Sword { get { return bones[10]; } }

        public ExtrIncMdl1(ActorModelTextureSet texSet)
        {
            string mdlPath;
            Size.Value = new Vector2(150, 150);

            orientsModels[ActorOrient.ToUs] = new TwilightHierarchyObject[11];
            orientsModels[ActorOrient.ToRight] = new TwilightHierarchyObject[11];
            orientsModels[ActorOrient.ToLeft] = new TwilightHierarchyObject[11];
            orientsModels[ActorOrient.ToForward] = new TwilightHierarchyObject[11];

            mdlPath = "mdl\\" + typeof(ExtrIncMdl1).Name + "\\" + Orientation.ToString() + "_";
            Orientation = ActorOrient.ToUs;
            {
                bones[0] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.Body));
                Body.Origin.Value = new Vector2(73, 83);Body.SetParentObject(this); Body.Level = 0.80f;

                bones[1] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.Head));
                Head.Origin.Value = new Vector2(73, 42); Head.SetParentObject(Body); Head.Level = 0.75f;

                bones[2] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.LeftArm));
                LeftArm.Origin.Value = new Vector2(54, 48); LeftArm.SetParentObject(Body); LeftArm.Level = 0.50f;

                bones[3] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.RightArm));
                RightArm.Origin.Value = new Vector2(100, 48); RightArm.SetParentObject(Body); RightArm.Level = 0.70f;

                bones[4] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.LeftHand));
                LeftHand.Origin.Value = new Vector2(45, 79); LeftHand.SetParentObject(LeftArm); LeftHand.Level = 0.45f;

                bones[5] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.RightHand));
                RightHand.Origin.Value = new Vector2(105, 78); RightHand.SetParentObject(RightArm); RightHand.Level = 0.65f;

                bones[6] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.LeftLeg));
                LeftLeg.Origin.Value = new Vector2(68, 84); LeftLeg.SetParentObject(Body); LeftLeg.Level = 0.90f;

                bones[7] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.RightLeg));
                RightLeg.Origin.Value = new Vector2(84, 84); RightLeg.SetParentObject(Body); RightLeg.Level = 0.85f;

                bones[8] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.LeftFoot));
                LeftFoot.Origin.Value = new Vector2(66, 111); LeftFoot.SetParentObject(LeftLeg); LeftFoot.Level = 1.00f;

                bones[9] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.RightFoot));
                RightFoot.Origin.Value = new Vector2(84, 111); RightFoot.SetParentObject(RightLeg); RightFoot.Level = 0.95f;

                bones[10] = new TwilightHierarchyObject(new AnimTexture(mdlPath + texSet.Weapon));
                Sword.Origin.Value = new Vector2(51, 96); Sword.SetParentObject(LeftHand); Sword.Level = 0.30f;
            }
            mdlPath = "mdl\\" + typeof(ExtrIncMdl1).Name + "\\" + Orientation.ToString() + "_";
            Orientation = ActorOrient.ToRight;
            {
            }
            mdlPath = "mdl\\" + typeof(ExtrIncMdl1).Name + "\\" + Orientation.ToString() + "_";
            Orientation = ActorOrient.ToLeft;
            {
            }
            mdlPath = "mdl\\" + typeof(ExtrIncMdl1).Name + "\\" + Orientation.ToString() + "_";
            Orientation = ActorOrient.ToForward;
            {
            }
        }
    }
}
