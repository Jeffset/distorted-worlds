﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.ActorModels
{
    public abstract class ActorModel : TwilightObject
    {
        public Script<Action<ActorModel>> currentModelScript { get; set; }

        private ActorOrient _orient;
        public ActorOrient Orientation
        {
            get { return _orient; }
            set
            {
                if(currentModelScript!=null)
                    currentModelScript.Terminate();
                _orient = value;
                bones = orientsModels[value];
            }
        }

        protected Dictionary<ActorOrient, TwilightHierarchyObject[]> orientsModels = new Dictionary<ActorOrient, TwilightHierarchyObject[]>(4);
        protected TwilightHierarchyObject[] bones;

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < bones.Length; i++)
            {
                bones[i].Update();
            }
        }
        internal override void MakeNeedUpdate()
        {
            base.MakeNeedUpdate();
            for (int i = 0; i < bones.Length; i++)
            {
                bones[i].MakeNeedUpdate();
            }
        }
        public override void Render()
        {
            TwilightEngine.Sprite.Begin(SpriteSortMode.BackToFront, BlendState.NonPremultiplied);
            for (int i = 0; i < bones.Length; i++)
            {
                bones[i].RenderAsSubobject();
            }
            TwilightEngine.Sprite.End();
        }

        public void ResetModelState()
        {
            for (int i = 0; i < bones.Length; i++)
                bones[i].RC().Cfg(0);
        }
        public TwilightHierarchyObject.FloatHierarchyAngleCtrl ResetState(float time)
        {
            for (int i = 0; i < bones.Length - 1; i++)
                bones[i].RC().Cfg(0.0f, time, isAdditive: false);
            return bones[bones.Length - 1].RC().Cfg(0.0f, time, isAdditive: false);
        }

        protected Dictionary<ActorOrient, Action<ActorModel>> defaultRunScripts = new Dictionary<ActorOrient,Action<ActorModel>>(4);
        public void StartDefaultRunScript() { Script<Action<ActorModel>>.Start(defaultRunScripts[Orientation]); }
    }
}
