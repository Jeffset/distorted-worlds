using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

namespace DWOaO
{
    public partial class TwilightEngine : Microsoft.Xna.Framework.Game
    {
        public static GraphicsDeviceManager GraphicsManager { get; private set; }
        public static SpriteBatch Sprite { get; private set; }
        public static ContentManager ContentManager { get; private set; }

        private LinkedList<TwilightObject>[] objects;// = new LinkedList<TwilightObject>();
        private List<TwilightObject> objectsToRender = new List<TwilightObject>();

        public TwilightCamera Camera { get; private set; }

        public TwilightEngine(Misc.GraphicMode graphicsMode)
        {
            Global.Twilight = this;
            GraphicsManager = new GraphicsDeviceManager(this);
            IsMouseVisible = true;
            //IsFixedTimeStep = false;
            Content.RootDirectory = "Content";
            GraphicsManager.PreferredBackBufferFormat = SurfaceFormat.Bgr565;
            GraphicsManager.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            GraphicsManager.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            switch (graphicsMode)
            {
                case DWOaO.Misc.GraphicMode.Fullscreen:
                    {
                        GraphicsManager.IsFullScreen = true;
                        GraphicsManager.SynchronizeWithVerticalRetrace = true;
                    }
                    break;
                case DWOaO.Misc.GraphicMode.Windowed:
                    {
                        GraphicsManager.IsFullScreen = false;
                    }
                    break;
            }
        }
        protected override void Initialize()
        {
            Sprite = new SpriteBatch(GraphicsDevice);
            ContentManager = Content;
            //
            objects = new LinkedList<TwilightObject>[3];
            for (int i = 0; i < objects.Length; i++)
                objects[i] = new LinkedList<TwilightObject>();
            Camera = new TwilightCamera();
            Camera.Position.Ctrl<Anim.V2InputCtrl>().CfgKeyboard(2000f);
            Camera.Position.Ctrl<Anim.V2InputCtrl>().CfgMouseEdge(1500f);
            Camera.Position.Cnstr<Anim.V2RegionCnstr>().Cfg(new Vector2(0), new Vector2(20 * 150), Camera.Size);
            //
            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    ActorModels.ExtrIncMdl1 mdl = new ActorModels.ExtrIncMdl1(new ActorModels.ActorModelTextureSet());
                    mdl.Position.Value = new Vector2(x * 150, y * 150);
                    mdl.Orientation = ActorModels.ActorOrient.ToUs;
                    Script<Action<ActorModels.ExtrIncMdl1, float>>.Start(WarriorStrikeToUs, mdl, 100);
                    RegisterObject(mdl);
                }
            }
            base.Initialize();
        }
        private static void WarriorStrikeToUs(ActorModels.ExtrIncMdl1 actor, float speed)
        {
            Monitor.Enter(Global.Twilight);
            try
            {
                float fi = speed;
                float time;
                while (true)
                {
                    time = 6;// zamah1
                    actor.ResetModelState();
                    actor.Body.RC().Cfg(+14, fi * time);
                    actor.Head.RC().Cfg(+14, fi * time);
                    actor.RightLeg.RC().Cfg(-14, fi * time);
                    actor.LeftLeg.RC().Cfg(+20, fi * time);
                    actor.LeftFoot.RC().Cfg(-20, fi * time);
                    actor.RightArm.RC().Cfg(20, fi * time);
                    actor.RightHand.RC().Cfg(45, fi * time);
                    actor.LeftHand.RC().Cfg(-30, fi * time);
                    actor.Sword.RC().Cfg(+30, fi * time);
                    actor.LeftArm.RC().Cfg(-50, fi * time)
                        .WaitForFinish(fi * time);

                    time = 4;//zamah2
                    actor.Body.RC().Cfg(+10, fi * time);
                    actor.Head.RC().Cfg(-10, fi * time);
                    actor.RightLeg.RC().Cfg(-5, fi * time);
                    actor.LeftLeg.RC().Cfg(+5, fi * time);
                    actor.LeftFoot.RC().Cfg(-30, fi * time);
                    actor.RightArm.RC().Cfg(5, fi * time);
                    actor.RightHand.RC().Cfg(0, fi * time);
                    actor.LeftHand.RC().Cfg(-30, fi * time);
                    actor.LeftArm.RC().Cfg(-5, fi * time)
                        .WaitForFinish(fi * time);

                    time = 3;//udar
                    actor.ResetState(fi * time);
                    actor.Head.RC().Cfg(-15, fi * time);
                    actor.Body.RC().Cfg(-20, fi * time);
                    actor.Sword.RC().Cfg(+80, fi * time);
                    actor.RightArm.RC().Cfg(-40, fi * time);
                    actor.LeftArm.RC().Cfg(+40, fi * time)
                        .WaitForFinish(fi * time);

                    time = 7;//reset
                    actor.ResetState(fi * time).WaitForFinish(fi * time);
                }
            }
            catch (ThreadAbortException) { }
            finally { actor.ResetModelState(); actor.currentModelScript = null; Monitor.Exit(Global.Twilight); }
        }

        protected override void LoadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            Monitor.Enter(this);
            Camera.MakeNeedUpdate();
            for (int g = 0; g < objects.Length; g++)
            {
                LinkedList<TwilightObject> layer = objects[g];
                int count = layer.Count;
                for (LinkedListNode<TwilightObject> i = layer.First; i != null; i = i.Next)
                {
                    TwilightObject o = i.Value;
                    o.MakeNeedUpdate();
                }
            }
            objectsToRender.Clear();
            Camera.Update();
            Global.RegisterInput();
            for (int g = 0; g < objects.Length; g++)
            {
                LinkedList<TwilightObject> layer = objects[g];
                int count = layer.Count;
                for (LinkedListNode<TwilightObject> i = layer.First; i != null; i = i.Next)
                {
                    TwilightObject o = i.Value;
                    o.Update();
                    if (o.Bounds.Intersects(Camera.Bounds))
                        objectsToRender.Add(o);
                }
            }
            Monitor.Exit(this);
        }

        protected override void Draw(GameTime gameTime)
        {
            Monitor.Enter(this);
            GraphicsDevice.SetRenderTarget(null);
            GraphicsDevice.Clear(Color.Gray);
            foreach (var obj in objectsToRender)
                obj.Render();
            Sprite.Begin();
            Sprite.DrawString(Content.Load<SpriteFont>("Fonts\\BuxtonSketch"), "������!", new Vector2(50, 50), Color.Red);
            Sprite.End();
            Monitor.Exit(this);
        }
    }
}
