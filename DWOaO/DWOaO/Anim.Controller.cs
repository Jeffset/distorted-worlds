﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.Anim
{
    public abstract class Controller : Animatable
    {
        public bool IsRunning { get; protected set; }
        private int _lastTime;
        protected float elapsedTime;
        protected int GetTime()
        {
            int ms = Environment.TickCount - _lastTime;
            _lastTime = Environment.TickCount;
            return ms;
        }

        protected void Start()
        {
            _lastTime = Environment.TickCount;
            IsRunning = true;
        }
        protected Controller()
        {
            elapsedTime = 0.0f;
        }
    }
    public abstract class Controller<AP> : Controller where AP : AnimParam
    {
        public AP AnimParam { get; set; }
    }

    public class Constraint<AP> : Animatable where AP : AnimParam
    {
        public AP AnimParam { get; set; }
        private int _lastTime;
        protected int GetTime()
        {
            int ms = Environment.TickCount - _lastTime;
            _lastTime = Environment.TickCount;
            return ms;
        }
        protected void Start()
        {
            _lastTime = Environment.TickCount;
        }
    }

    public class FrameController : Controller<AnimTexture>
    {
        public FrameController()
        {

        }
        public void Stop()
        {
            IsRunning = false;
        }
    }
}
