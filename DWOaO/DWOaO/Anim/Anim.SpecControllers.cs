﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.Anim
{
    public class V2MouseCtrl : Controller<AnimFloat2>
    {
        public AnimFloat2 Offset { get; private set; }
        public override void Update()
        {
            base.Update();
            AnimParam.Value = Global.MousePos + Offset.Value;
        }
        public void Cfg(bool useCurrentOffset = false)
        {
            Offset.Value = useCurrentOffset ? (AnimParam.Value - Global.MousePos) : new Vector2();
            Start();
        }
        public V2MouseCtrl()
        {
            Offset = new AnimFloat2();
            animParams.Add(Offset);
        }
    }

    public class V2SmplCtrl : Controller<AnimFloat2>
    {
        public AnimFloat2 To { get; private set; }
        public AnimFloat2 From { get; private set; }
        public float Time { get; private set; }
        public Misc.Anim AnimType { get; private set; }

        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();

            elapsedTime += GetTime();
            if (elapsedTime > Time)
            {
                IsRunning = false; AnimParam.Value = To.Value; return;
                //Scriptable.UnlockScripting!
            }
            switch (AnimType)
            {
                case DWOaO.Misc.Anim.Linear:
                    AnimParam.Value =
                        new Vector2(AnimCalc.Linear(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Linear(From.Value.Y, To.Value.Y, elapsedTime / Time));
                    break;
                case DWOaO.Misc.Anim.Bezier3:
                    AnimParam.Value =
                        new Vector2(AnimCalc.Bezier3(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Bezier3(From.Value.Y, To.Value.Y, elapsedTime / Time));
                    break;
                default:
                    throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
            }
        }
        public V2SmplCtrl Cfg(Misc.Anim at, AnimFloat2 from, AnimFloat2 to, float time)
        {
            From = from; To = to; Time = time; AnimType = at;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start(); return this;
        }
        public V2SmplCtrl Cfg(Misc.Anim at, AnimFloat2 to, float time)
        {
            From = AnimParam.Value; To = to; Time = time; AnimType = at;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start(); return this;
        }
        public V2SmplCtrl()
        {
        }
    }

    public class V2InputCtrl : Controller<AnimFloat2>
    {
        public Keys UpKey { get; private set; }
        public Keys DownKey { get; private set; }
        public Keys RightKey { get; private set; }
        public Keys LeftKey { get; private set; }
        public float PpsKey { get; private set; }
        public float PpsMouse { get; private set; }

        public bool UseKeyboard { get; private set; }
        public bool UseMouseEdge { get; private set; }

        public void CfgKeyboard(float pixelPerSecond, Keys upkey = Keys.Up, Keys downKey = Keys.Down, Keys rightKey = Keys.Right, Keys leftKey = Keys.Left)
        {
            PpsKey = pixelPerSecond; UpKey = upkey; DownKey = downKey; RightKey = rightKey; LeftKey = leftKey; UseKeyboard = true; Start();
        }
        public void CfgMouseEdge(float pixelPerSecond)
        {
            PpsMouse = pixelPerSecond; UseMouseEdge = true; Start();
        }
        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();
            float ms = GetTime();
            if (UseKeyboard)
            {
                if (Global.Keyboard.IsKeyDown(UpKey))
                    AnimParam.Y -= PpsKey * ms / 1000.0f;
                if (Global.Keyboard.IsKeyDown(DownKey))
                    AnimParam.Y += PpsKey * ms / 1000.0f;
                if (Global.Keyboard.IsKeyDown(RightKey))
                    AnimParam.X += PpsKey * ms / 1000.0f;
                if (Global.Keyboard.IsKeyDown(LeftKey))
                    AnimParam.X -= PpsKey * ms / 1000.0f;
            }
            if (UseMouseEdge)
            {
                if (Global.Mouse.X == 0)
                    AnimParam.X -= PpsMouse * ms / 1000.0f;
                else if (Global.Mouse.X == TwilightEngine.GraphicsManager.PreferredBackBufferWidth - 1)
                    AnimParam.X += PpsMouse * ms / 1000.0f;
                if (Global.Mouse.Y == 0)
                    AnimParam.Y -= PpsMouse * ms / 1000.0f;
                else if (Global.Mouse.Y == TwilightEngine.GraphicsManager.PreferredBackBufferHeight - 1)
                    AnimParam.Y += PpsKey * ms / 1000.0f;
            }
        }
        public V2InputCtrl()
        {
        }
    }

    public class FCtrl : Controller<AnimFloat>
    {
        public AnimFloat To { get; private set; }
        public AnimFloat From { get; private set; }
        public float Time { get; private set; }
        public Misc.Anim AnimType { get; private set; }

        public FCtrl Cfg(Misc.Anim at, AnimFloat from, AnimFloat to, float time)
        {
            From = from; To = to; Time = time; AnimType = at;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start();
            return this;
        }
        public FCtrl Cfg(Misc.Anim at, AnimFloat to, float time)
        {
            From = AnimParam.Value; To = to; Time = time; AnimType = at;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start();
            return this;
        }
        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();
            elapsedTime += GetTime();
            if (elapsedTime > Time)
            {
                AnimParam.Value = To.Value; Finish(); return;

                //Scriptable.UnlockScripting!
            }
            switch (AnimType)
            {
                case DWOaO.Misc.Anim.Linear:
                    AnimParam.Value = AnimCalc.Linear(From.Value, To.Value, elapsedTime / Time);
                    break;
                case DWOaO.Misc.Anim.Bezier3:
                    AnimParam.Value = AnimCalc.Bezier3(From.Value, To.Value, elapsedTime / Time);
                    break;
                default:
                    throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
            }
        }

        public FCtrl()
        {
        }
    }

    public class V2BezierPathCtrl : Controller<AnimFloat2>
    {
        public AnimFloat2 From { get; private set; }
        public AnimFloat2 D1 { get; private set; }
        public AnimFloat2 D2 { get; private set; }
        public AnimFloat2 To { get; private set; }
        public float Time { get; private set; }
        public Misc.Anim AnimType { get; private set; }

        public V2BezierPathCtrl Cfg2(AnimFloat2 from, AnimFloat2 d1, AnimFloat2 to, float time)
        {
            From = from; To = to; Time = time; D1 = d1; AnimType = Misc.Anim.bezierPath2;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, D1, To });
            Start(); return this;
        }
        public V2BezierPathCtrl Cfg2(AnimFloat2 d1, AnimFloat2 to, float time)
        {
            From = AnimParam.Value; To = to; Time = time; D1 = d1; AnimType = Misc.Anim.bezierPath2;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, D1, To });
            Start(); return this;
        }
        public V2BezierPathCtrl Cfg3(AnimFloat2 from, AnimFloat2 d1, AnimFloat2 d2, AnimFloat2 to, float time)
        {
            From = from; To = to; Time = time; D1 = d1; D2 = d2; AnimType = Misc.Anim.bezierPath3;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, D1, D2, To });
            Start(); return this;
        }
        public V2BezierPathCtrl Cfg3(AnimFloat2 d1, AnimFloat2 d2, AnimFloat2 to, float time)
        {
            From = AnimParam.Value; To = to; Time = time; D1 = d1; D2 = d2; AnimType = Misc.Anim.bezierPath3;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, D1, D2, To });
            Start(); return this;
        }

        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();

            elapsedTime += GetTime();
            if (elapsedTime > Time)
            {
                IsRunning = false; AnimParam.Value = To.Value; return;
                //Scriptable.UnlockScripting!
            }
            switch (AnimType)
            {
                case DWOaO.Misc.Anim.bezierPath2:
                    AnimParam.Value =
                        new Vector2(AnimCalc.BezierPath2(From.X, D1.X, To.X, elapsedTime / Time),
                            AnimCalc.BezierPath2(From.Y, D1.Y, To.Y, elapsedTime / Time));
                    break;
                case DWOaO.Misc.Anim.bezierPath3:
                    AnimParam.Value =
                        new Vector2(AnimCalc.BezierPath3(From.X, D1.X, D2.X, To.X, elapsedTime / Time),
                            AnimCalc.BezierPath3(From.Y, D1.Y, D2.Y, To.Y, elapsedTime / Time));
                    break;
                default:
                    throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
            }
        }

        public V2BezierPathCtrl()
        {

        }
    }

    public class V2RegionCnstr : Constraint<AnimFloat2>
    {
        public AnimFloat2 RegionPos { get; private set; }
        public AnimFloat2 RegionSize { get; private set; }
        public AnimFloat2 ObjectSize { get; private set; }

        public void Cfg(AnimFloat2 regPos, AnimFloat2 regSize, AnimFloat2 objSize)
        {
            RegionPos = regPos; RegionSize = regSize; ObjectSize = objSize;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { RegionPos, RegionSize, ObjectSize });
        }

        public override void Update()
        {
            base.Update();
            if (AnimParam.X < RegionPos.X)
                AnimParam.X = RegionPos.X;
            else if (AnimParam.X + ObjectSize.X > RegionPos.X + RegionSize.X)
                AnimParam.X = RegionPos.X + RegionSize.X - ObjectSize.X;
            if (AnimParam.Y < RegionPos.Y)
                AnimParam.Y = RegionPos.Y;
            else if (AnimParam.Y + ObjectSize.Y > RegionPos.Y + RegionSize.Y)
                AnimParam.Y = RegionPos.Y + RegionSize.Y - ObjectSize.Y;
        }

        public V2RegionCnstr()
        {

        }
    }

    public class V3SmplCtrl : Controller<AnimFloat3>
    {
        public AnimFloat3 To { get; private set; }
        public AnimFloat3 From { get; private set; }
        public float Time { get; private set; }
        public Misc.Anim AnimType { get; private set; }

        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();

            elapsedTime += GetTime();
            if (elapsedTime > Time)
            {
                IsRunning = false; AnimParam.Value = To.Value; return;
                //Scriptable.UnlockScripting!
            }
            switch (AnimType)
            {
                case DWOaO.Misc.Anim.Linear:
                    AnimParam.Value =
                        new Vector3(AnimCalc.Linear(From.Value.X, To.Value.X, elapsedTime / Time),
                            AnimCalc.Linear(From.Value.Y, To.Value.Y, elapsedTime / Time),
                            AnimCalc.Linear(From.Value.Z, To.Value.Z, elapsedTime / Time));
                    break;
                case DWOaO.Misc.Anim.Bezier3:
                    AnimParam.Value =
                        new Vector3(AnimCalc.Bezier3(From.Value.X, To.Value.X, elapsedTime / Time),
                            AnimCalc.Bezier3(From.Value.Y, To.Value.Y, elapsedTime / Time),
                            AnimCalc.Bezier3(From.Value.Z, To.Value.Z, elapsedTime / Time));
                    break;
                default:
                    throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
            }
        }
        public V3SmplCtrl Cfg(Misc.Anim at, AnimFloat3 from, AnimFloat3 to, float time)
        {
            From = from; To = to; Time = time; AnimType = at;
            AnimParam.Value = From.Value;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start(); return this;
        }
        public V3SmplCtrl Cfg(Misc.Anim at, AnimFloat3 to, float time)
        {
            From = AnimParam.Value; To = to; Time = time; AnimType = at;
            animParams.Clear();
            animParams.AddRange(new AnimParam[] { From, To });
            Start(); return this;
        }
        public V3SmplCtrl()
        {
        }
    };
}
