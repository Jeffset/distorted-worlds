﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DWOaO.Anim
{
    public abstract class Controller : Animatable
    {
        public bool IsRunning { get; protected set; }
        private int _lastTime;
        private bool _isScriptingLocked = false;
        protected float elapsedTime;
        protected int GetTime()
        {
            int ms = Environment.TickCount - _lastTime;
            _lastTime = Environment.TickCount;
            return ms;
        }
        public override void Update()
        {
            base.Update();
            //if (!_isScriptingLocked)
            //{
            //    Monitor.Enter(this);
            //    _isScriptingLocked = true;
            //}
        }

        protected void Start()
        {
            //UnlockScript();
            elapsedTime = 0.0f;
            _lastTime = Environment.TickCount;
            IsRunning = true;
        }
        protected void UnlockScript()
        {
            //if (_isScriptingLocked)
            //{
            //    _isScriptingLocked = false;
            //    Monitor.Exit(this);
            //}
        }
        protected void Finish()
        {
            IsRunning = false;
            //UnlockScript();
        }
        public void WaitForFinish(float time)
        {
            Monitor.Exit(Global.Twilight);
            Thread.Sleep((int)Math.Round(time)+10);
            Monitor.Enter(Global.Twilight);
        }
        protected Controller()
        {
            elapsedTime = 0.0f;
        }
    }
    public abstract class Controller<AP> : Controller where AP : AnimParam
    {
        public AP AnimParam { get; set; }
    }

    public abstract class Constraint : Animatable
    {
        private int _lastTime;
        protected int GetTime()
        {
            int ms = Environment.TickCount - _lastTime;
            _lastTime = Environment.TickCount;
            return ms;
        }
        protected void Start()
        {
            _lastTime = Environment.TickCount;
        }
    }

    public class Constraint<AP> : Constraint where AP : AnimParam
    {
        public AP AnimParam { get; set; }
    }

    public class FrameController : Controller<AnimTexture>
    {
        public AnimFloat FrameInterval { get; private set; }
        public void Cfg(AnimFloat frameInterval)
        {
            FrameInterval = frameInterval;
            animParams.Clear();
            animParams.Add(FrameInterval);
            Start();
        }
        public override void Update()
        {
            if (!IsRunning) return;
            base.Update();

            elapsedTime += GetTime();
            if (elapsedTime > FrameInterval.Value)
            {
                AnimParam.CurrentFrame = AnimParam.CurrentFrame + 1 >= AnimParam.FrameCount ? 0 : AnimParam.CurrentFrame + 1;
                elapsedTime = 0;
            }
        }
        public FrameController()
        {

        }
        public void Stop()
        {
            IsRunning = false;
        }
    }
}
