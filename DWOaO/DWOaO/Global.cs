﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    public static class Global
    {
        public static TwilightEngine Twilight;
        public static KeyboardState Keyboard;
        public static MouseState Mouse;
        public static Vector2 MousePos;

        public static void RegisterInput()
        {
            Mouse = Microsoft.Xna.Framework.Input.Mouse.GetState();
            MousePos = new Vector2(Mouse.X, Mouse.Y);
            MousePos += Global.Twilight.Camera.Position.Value;
            Keyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();
        }
    }
}
