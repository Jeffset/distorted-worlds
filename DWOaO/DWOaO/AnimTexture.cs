﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    public class Shader
    {
        public Effect ShaderEffect { get; set; }
        public BlendState BlendingOptions { get; set; }
    }

    public class AnimTexture : AnimParam
    {
        private Anim.FrameController _frameController = new Anim.FrameController();

        public Anim.FrameController CtrlFrames()
        { return _frameController; }
        public C Ctrl<C>() where C : Anim.Controller<AnimTexture>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimTexture>).AnimParam = this;
            }
            return _controller as C;
        }
        public override void TerminateAnimation()
        { _controller = null; }

        public Texture2D Texture { get; private set; }
        public RenderTarget2D RenderTarget { get; set; }

        public int Width { get; private set; }
        public int Heigth { get; private set; }
        private int _fullTextureWidth;
        private int _fullTExtureHeight;
        private int _wf, _hf;

        public int FrameCount { get; private set; }
        public int CurrentFrame { get; set; }
        public Shader Shader { get; set; }

        public void GetTextureForFinalRender(out Texture2D texture, out Rectangle? sourceRect, out Shader shader)
        {
            //if (RenderTarget != null) texture = RenderTarget;
            texture = Texture;
            if (!Animated) sourceRect = null;
            else
            {
                int x = CurrentFrame % _wf;
                int y = CurrentFrame / _wf;
                sourceRect = new Rectangle(x * Width, y * Heigth, Width, Heigth);
            }
            shader = this.Shader;
        }

        public Texture2D GetTextureForShader()// Only for non-animated textures!!!
        {
            if (Animated) throw new System.Exception("Can not use animated textures as shader parameters!");
            if (Shader == null)
                return Texture;
            else
            {
                if (RenderTarget == null)
                    RenderTarget = new RenderTarget2D(TwilightEngine.GraphicsManager.GraphicsDevice, Width, Heigth, false, SurfaceFormat.Color, DepthFormat.None);
                TwilightEngine.GraphicsManager.GraphicsDevice.SetRenderTarget(RenderTarget);
                TwilightEngine.Sprite.Begin(SpriteSortMode.Immediate, Shader.BlendingOptions, null, null, null, Shader.ShaderEffect);
                if (!Animated)
                    TwilightEngine.Sprite.Draw(Texture, new Vector2(0.0f), null, Color.White);
                else
                {
                    int x = CurrentFrame % _wf; int y = CurrentFrame / _wf;
                    TwilightEngine.Sprite.Draw(Texture, new Vector2(0.0f), new Rectangle(x * Width, y * Heigth, Width, Heigth), Color.White);
                }
                TwilightEngine.Sprite.End();
                return RenderTarget;
            }
        }

        public bool Animated { get; private set; }

        public AnimTexture(string textureName, bool animation = false, int width = -1, int height = -1)
        {
            Texture = TwilightEngine.ContentManager.Load<Texture2D>(textureName);
            Animated = animation;
            if (!Animated)
            {
                Width = Texture.Width;
                Heigth = Texture.Height;
            }
            else
            {
                if (width == -1 || height == -1)
                    throw new System.Exception("Heigth or width is not specified for animated texture!");
                if (_fullTextureWidth % Width != 0 || _fullTExtureHeight % Heigth != 0)
                    throw new System.Exception("Wrong width or heigth is specified for animated texture!");
                Width = width;
                Heigth = height;
                _fullTextureWidth = Texture.Width;
                _fullTExtureHeight = Texture.Height;
                _wf = _fullTextureWidth / Width;
                _hf = _fullTExtureHeight / Heigth;
                FrameCount = _wf * _hf;
            }
            Shader = new Shader();
            Shader.BlendingOptions = BlendState.NonPremultiplied;
        }

        public override void Update()
        {
            if (_controller != null) { _controller.Update(); }
            _frameController.Update();
        }
    }
}
