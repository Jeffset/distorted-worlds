#define MAX 20

float3 objPos;
texture normalMap;

const float dist = 0.005f;

float3 lightDir[MAX];
float4 lightColor[MAX];
float lightDistance[MAX];

float factor;
float strength;// [0.0; 1.0]
float scrollX;// [0.0; 1.0]
texture displMap;
texture maskMap;

sampler NormalMap  : samplerState{
	Texture = normalMap;
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler DisplSampler : register (s0);

sampler Mask : samplerState{
	Texture = maskMap;
	MinFilter = Linear;
	MagFilter = Linear;
	MipFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4x4 MatrixTransform;

void vs(inout float4 color    : COLOR0,
	inout float2 texCoord : TEXCOORD0,
	inout float4 position : SV_Position)
{
	position = mul(position, MatrixTransform);
}

float4 main_Displacement(float2 texCoord)
{
	float maskVal = tex2D(Mask, texCoord);
	float2 originalCoord = texCoord;
	texCoord.x += scrollX;

	float4 displ = tex2D(DisplSampler, texCoord);
	texCoord = originalCoord;
	displ.b = max(displ.r, displ.g);
	displ.r -= displ.b; displ.g -= displ.b;
	texCoord.x -= displ.r * factor * strength*(maskVal);
	texCoord.y -= displ.g * factor * strength*(maskVal);
	float4 retVal = tex2D(NormalMap, texCoord);
	return retVal;
}

float4 lght(uniform int c, float2 texCoord : TEXCOORD0) : COLOR0
{
	float3 normal = main_Displacement(texCoord);//tex2D(NormalMap, texCoord);
	normal -= float3(0.5f, 0.5f, 0.5f);
	normal *= 2;
	float3 local = float3(texCoord.x, texCoord.y, 0.0f);
	float4 base = float4(0, 0, 0, 1);
	for (int i = 0; i < c; i++)
	{
		float3 vl = lightDir[i] + local;
		base = float4(base.rgb + saturate(lightColor[i].rgb * lightColor[i].a*(1.0f - length(vl) / lightDistance[i])* saturate(dot(normal, normalize(vl)))), 1.0f);
	}
	return base;
}

technique L1
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(1);
	}
}

technique L2
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(2);
	}
}
technique L3
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(3);
	}
}
technique L4
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(4);
	}
}
technique L5
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(5);
	}
}
technique L6
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(6);
	}
}
technique L7
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(7);
	}
}
technique L8
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(8);
	}
}
technique L9
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(9);
	}
}
technique L10
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(10);
	}
}
technique L11
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(11);
	}
}
technique L12
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(12);
	}
}
technique L13
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(13);
	}
}
technique L14
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(14);
	}
}
technique L15
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(15);
	}
}
technique L16
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(16);
	}
}
technique L17
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(17);
	}
}
technique L18
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(18);
	}
}
technique L19
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(19);
	}
}
technique L20
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(20);
	}
}