#define MAX 20

float3 objPos;
texture2D normalMap;

const float dist = 0.005f;

float3 lightDir[MAX];
float4 lightColor[MAX];
float lightDistance[MAX];

sampler Texture : register(s0);

//sampler NormalMap : samplerState
//{
//	Texture = normalMap;
//	MinFilter = Linear;
//	MagFilter = Linear;
//	MipFilter = Linear;
//	AddressU = Clamp;
//	AddressV = Clamp;
//};

float4x4 MatrixTransform;

void vs(inout float4 color    : COLOR0,
	inout float2 texCoord : TEXCOORD0,
	inout float4 position : SV_Position)
{
	position = mul(position, MatrixTransform);
}

float4 lght(uniform int c, float2 texCoord : TEXCOORD0) : COLOR0
{
	float3 normal = normalMap.Sample(Texture,texCoord);//tex2D(NormalMap, texCoord);
	normal -= float3(0.5f, 0.5f, 0.5f);
	normal *= 2;
	float4 base = tex2D(Texture, texCoord);
	float3 local = float3(texCoord.x, texCoord.y, 0.0f);

	for (int i = 0; i < c; i++)
	{
		float3 vl = lightDir[i] + local;
		base = float4(base.rgb + saturate(lightColor[i].rgb * lightColor[i].a*(1.0f - length(vl) / lightDistance[i])* saturate(dot(normal, normalize(vl)))), base.a);
	}
	return base;
}

technique L1
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(1);
	}
}

technique L2
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(2);
	}
}
technique L3
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(3);
	}
}
technique L4
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(4);
	}
}
technique L5
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(5);
	}
}
technique L6
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(6);
	}
}
technique L7
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(7);
	}
}
technique L8
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(8);
	}
}
technique L9
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(9);
	}
}
technique L10
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(10);
	}
}
technique L11
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(11);
	}
}
technique L12
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(12);
	}
}
technique L13
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(13);
	}
}
technique L14
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(14);
	}
}
technique L15
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(15);
	}
}
technique L16
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(16);
	}
}
technique L17
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(17);
	}
}
technique L18
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(18);
	}
}
technique L19
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(19);
	}
}
technique L20
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 vs();
		PixelShader = compile ps_3_0 lght(20);
	}
}