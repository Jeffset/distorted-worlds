texture maskMap;

sampler TextureSampler : register (s0);

sampler MaskSampler : samplerState
{
	Texture = maskMap;
	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4 main_Masking(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
	float4 texel = tex2D(TextureSampler,texCoord);
	float4 masking = tex2D(MaskSampler,texCoord);
	return texel * masking.r * color;
}

technique Masking
{
	pass Pass0
	{
		PixelShader = compile ps_2_0 main_Masking();
	}
}