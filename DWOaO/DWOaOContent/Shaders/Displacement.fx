float factor;
float strength;// [0.0; 1.0]
float scrollX;// [0.0; 1.0]
texture displMap;
texture maskMap;

sampler Sampler : register (s0);

sampler DisplSampler : samplerState{
	Texture = displMap;
	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler Mask : samplerState{
	Texture = maskMap;
	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

float4 main_Displacement (float4 color : COLOR0, float2 texCoord: TEXCOORD0) : COLOR0
{
	float maskVal = tex2D(Mask,texCoord);
	float2 originalCoord = texCoord;
		texCoord.x += scrollX;
	/*float n = 1 - scrollX + texCoord.x;
	if(n>=1)n-=1;
	texCoord.x = n;*/
	float4 displ = tex2D(DisplSampler,texCoord);
	texCoord = originalCoord;
	displ.b = max(displ.r,displ.g);
	displ.r-=displ.b; displ.g-=displ.b;
	texCoord.x -= displ.r * factor * strength*(maskVal);
	texCoord.y -= displ.g * factor * strength*(maskVal);
	float4 retVal = tex2D(Sampler,texCoord);
	return retVal*color;
}

technique Displacement
{
	pass Pass0
	{
		PixelShader = compile ps_2_0 main_Displacement();
	}
}