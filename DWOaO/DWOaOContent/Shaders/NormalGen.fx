//float3 lightPos;
//float3 objPos;
//float4 lightColor;
//float lightDistance;

sampler NormalMap : register(s0);

const float dist = 0.0001f;

float4 normalMapping_main(float4 color : COLOR0, float2 texCoord : TEXCOORD0) : COLOR0
{
	//texCoord.y *= 0.8;
	//float3 vl = objPos - lightPos + float3(texCoord.x, texCoord.y*0.8, 0.0f); // ����������� �����.
	//vl.y /= 0.8f;
	//vl.z *= -1.0f;

	float4 dP1 = tex2D(NormalMap, texCoord);// ������� ������ � �������
	float4 dP2 = tex2D(NormalMap, texCoord + float2(dist, -dist));// ������� ������ � �������.
	float4 dP3 = tex2D(NormalMap, texCoord + float2(dist, +dist));// ������� ������ � �������.

	float3 v1 = float3(dist, -dist, -dP2.r + dP1.r);
	float3 v2 = float3(dist, +dist, -dP3.r + dP1.r);// ���������� ��������, �������� ��������� ��������������.

	float3 n = normalize(cross(v1, v2));
	n /= 2; n += float3(0.5f, 0.5f, 0.5f);
	return float4(n.xyz,1);
	//float xK = dist*(v2.z+v1.z); // ���������� �-���������� ������� � ��������� ��������������.
	//float yK = dist*(v2.z-v1.z); // ���������� �-���������� ������� � ��������� ��������������.
	//float zK = dist*dist*2;// ���������� �-���������� ������� � ��������� ��������������.*/

	//float CosN = (vl.x*xK+vl.y*yK+vl.z*zK) / sqrt((vl.x*vl.x+vl.y*vl.y+vl.z*vl.z)*(xK*xK+yK*yK+zK*zK));// ���������� �������� ���� ������� ��������� ���� � �������
	//float3 ld = normalize(vl); float3 normal = normalize(raw_normal/*float3(xK,yK,zK)*/);
	//float3 normal = tex2D(NormalMap, texCoord);
	//float CosN = saturate(pow(dot(normal, ld),1));
	//CosN = pow(CosN,3);// �������������������
	//return float4(lightColor.rgb,lightColor.a*(1.0f - length(vl) / lightDistance)*CosN * 1.0f * dP1.a);
	//return float4(texCoord, texCoord);
}

technique NormalMapping
{
	pass Pass1
	{
		PixelShader = compile ps_2_0 normalMapping_main();
	}
}
