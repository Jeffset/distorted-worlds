﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;
//using DWOaO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TwilightProcessors
{
    [Serializable]
    public class BoneDescription
    {
        public Point Origin;
        public Point BaseLocation;
        public List<Point> ExtraMarkers = new List<Point>();
    }

    [ContentProcessor]
    class TwilightActorMdlProcessor : TextureProcessor
    {
        static readonly Color ORIGIN = new Color(1f, 0f, 1f, 1f);
        static readonly Color EXTRAMARKER = new Color(0f, 0f, 1f, 1f);
        static readonly Point NPOINT = new Point(0, 0);
        public override TextureContent Process(TextureContent input, ContentProcessorContext context)
        {
            PixelBitmapContent<Color> bmp = input.Faces[0][0] as PixelBitmapContent<Color>;

            //Point bd.BaseLocation = new Point(0, bmp.Height), bd.Origin = NPOINT, corner = new Point(bmp.Width, 0);
            //List<Point> extraMarkers = new List<Point>();
            BoneDescription bd = new BoneDescription();
            bd.BaseLocation = new Point(0, bmp.Height);
            Point corner = new Point(bmp.Width, 0);

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    Color pix = bmp.GetPixel(x, y);
                    if (pix.A != 0)
                    {
                        if (bd.BaseLocation.X == 0) bd.BaseLocation.X = x;
                        if (y < bd.BaseLocation.Y) bd.BaseLocation.Y = y;
                    }
                    if (pix == ORIGIN)
                    {
                        bd.Origin = new Point(x, y);
                        bmp.SetPixel(x, y, bmp.GetPixel(x - 1, y - 1));
                    }
                    if (pix == EXTRAMARKER)
                    {
                        bd.ExtraMarkers.Add(new Point(x, y));
                        bmp.SetPixel(x, y, bmp.GetPixel(x - 1, y - 1));
                    }
                }
            }
            for (int x = bmp.Width - 1; x >= 0; x--)
            {
                for (int y = bmp.Height - 1; y >= 0; y--)
                {
                    Color pix = bmp.GetPixel(x, y);
                    if (pix.A != 0)
                    {
                        if (corner.X == bmp.Width) corner.X = x;
                        if (y > corner.Y) corner.Y = y;
                        continue;
                    }
                }
            }
            PixelBitmapContent<Color> clr = new PixelBitmapContent<Color>(corner.X - bd.BaseLocation.X, corner.Y - bd.BaseLocation.Y);
            for (int x = 0; x < clr.Width; x++)
            {
                for (int y = 0; y < clr.Height; y++)
                {
                    Color pix = bmp.GetPixel(x + bd.BaseLocation.X, y + bd.BaseLocation.Y);
                    clr.SetPixel(x, y, pix);
                }
            }
            string path = Path.ChangeExtension(context.OutputFilename, ".bone");
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            using (BinaryWriter br = new BinaryWriter(File.OpenWrite(path)))
            {
                br.Write(bd.BaseLocation.X); br.Write(bd.BaseLocation.Y);
                br.Write(bd.Origin.X); br.Write(bd.Origin.Y);
                br.Write(bd.ExtraMarkers.Count);
                foreach (var m in bd.ExtraMarkers)
                { br.Write(m.X); br.Write(m.Y); }
            }
            input.Faces[0][0] = clr;
            return base.Process(input, context);
        }
    }
}
