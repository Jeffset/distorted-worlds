﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline.Processors;

namespace TwilightProcessors
{
    [ContentProcessor]
    public class TwilightAnimationProcessor : TextureProcessor
    {
        public override TextureContent Process(TextureContent input, ContentProcessorContext context)
        {
            string assetName = Path.GetFileNameWithoutExtension(context.OutputFilename);
            string[] frames = Directory.GetFiles(assetName);
            PixelBitmapContent<Color> bmp = input.Faces[0][0] as PixelBitmapContent<Color>;
            if (frames.Length != 20) throw new System.Exception("frame count should be equal to 20!");
            PixelBitmapContent<Color> result = new PixelBitmapContent<Color>(bmp.Width * 4, bmp.Height * 5);
            int x = 0, y = 0;
            for (int c = 0; c < 20; c++)
            {
                bmp = context.BuildAndLoadAsset<TextureContent, TextureContent>(new ExternalReference<TextureContent>(frames[c]), "TextureProcessor")
                    .Faces[0][0] as PixelBitmapContent<Color>;
                y = c / 4;
                x = c % 4;
                for (int xx = x * bmp.Width; xx < (x + 1) * bmp.Width; xx++)
                    for (int yy = y * bmp.Height; yy < (y + 1) * bmp.Height; yy++)
                    {
                        Color pixel = bmp.GetPixel(xx - x * bmp.Width, yy - bmp.Height * y);
                        result.SetPixel(xx, yy, pixel);
                    }
            }
            input.Faces[0][0] = result;
            return base.Process(input, context);
        }
    }
}
