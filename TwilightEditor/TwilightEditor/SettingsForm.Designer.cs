﻿namespace DWOaO.TwilightEditor
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.lbStyles = new System.Windows.Forms.ListBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.gbCreate = new System.Windows.Forms.GroupBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.nudHeight = new System.Windows.Forms.NumericUpDown();
            this.nudWidth = new System.Windows.Forms.NumericUpDown();
            this.lbFields = new System.Windows.Forms.ListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbOpen = new System.Windows.Forms.GroupBox();
            this.btnDuplicate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lbMaps = new System.Windows.Forms.ListBox();
            this.tbMapFolder = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            this.gbCreate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).BeginInit();
            this.gbOpen.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(6, 99);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(96, 13);
            label1.TabIndex = 4;
            label1.Text = "Ширина (в полях):";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(6, 143);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(95, 13);
            label2.TabIndex = 4;
            label2.Text = "Высота (в полях):";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(6, 55);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(60, 13);
            label3.TabIndex = 4;
            label3.Text = "Название:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(151, 97);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(112, 13);
            label4.TabIndex = 4;
            label4.Text = "Стиль (набор полей):";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(6, 62);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(126, 13);
            label5.TabIndex = 4;
            label5.Text = "Папка с файлами карт:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(7, 189);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(136, 13);
            label6.TabIndex = 4;
            label6.Text = "Список доступных полей:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new System.Drawing.Point(6, 115);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(154, 13);
            label7.TabIndex = 4;
            label7.Text = "Сохранённые карты в папке:";
            // 
            // lbStyles
            // 
            this.lbStyles.FormattingEnabled = true;
            this.lbStyles.Location = new System.Drawing.Point(154, 116);
            this.lbStyles.Name = "lbStyles";
            this.lbStyles.Size = new System.Drawing.Size(134, 290);
            this.lbStyles.TabIndex = 0;
            this.lbStyles.SelectedIndexChanged += new System.EventHandler(this.lbStyles_SelectedIndexChanged);
            // 
            // btnOpen
            // 
            this.btnOpen.Enabled = false;
            this.btnOpen.Location = new System.Drawing.Point(6, 18);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(87, 30);
            this.btnOpen.TabIndex = 1;
            this.btnOpen.Text = "Открыть";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(6, 19);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(282, 30);
            this.btnCreate.TabIndex = 1;
            this.btnCreate.Text = "Создать";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // gbCreate
            // 
            this.gbCreate.Controls.Add(label4);
            this.gbCreate.Controls.Add(label3);
            this.gbCreate.Controls.Add(label6);
            this.gbCreate.Controls.Add(label2);
            this.gbCreate.Controls.Add(label1);
            this.gbCreate.Controls.Add(this.tbName);
            this.gbCreate.Controls.Add(this.nudHeight);
            this.gbCreate.Controls.Add(this.nudWidth);
            this.gbCreate.Controls.Add(this.btnCreate);
            this.gbCreate.Controls.Add(this.lbFields);
            this.gbCreate.Controls.Add(this.lbStyles);
            this.gbCreate.Location = new System.Drawing.Point(12, 12);
            this.gbCreate.Name = "gbCreate";
            this.gbCreate.Size = new System.Drawing.Size(294, 416);
            this.gbCreate.TabIndex = 2;
            this.gbCreate.TabStop = false;
            this.gbCreate.Text = "Создать новую карту";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(9, 73);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(279, 20);
            this.tbName.TabIndex = 3;
            // 
            // nudHeight
            // 
            this.nudHeight.Location = new System.Drawing.Point(9, 161);
            this.nudHeight.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudHeight.Name = "nudHeight";
            this.nudHeight.Size = new System.Drawing.Size(138, 20);
            this.nudHeight.TabIndex = 2;
            this.nudHeight.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nudHeight.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // nudWidth
            // 
            this.nudWidth.Location = new System.Drawing.Point(9, 117);
            this.nudWidth.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudWidth.Name = "nudWidth";
            this.nudWidth.Size = new System.Drawing.Size(139, 20);
            this.nudWidth.TabIndex = 2;
            this.nudWidth.UpDownAlign = System.Windows.Forms.LeftRightAlignment.Left;
            this.nudWidth.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lbFields
            // 
            this.lbFields.Enabled = false;
            this.lbFields.FormattingEnabled = true;
            this.lbFields.Location = new System.Drawing.Point(9, 207);
            this.lbFields.Name = "lbFields";
            this.lbFields.Size = new System.Drawing.Size(134, 199);
            this.lbFields.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(6, 434);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(585, 30);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Выйти";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gbOpen
            // 
            this.gbOpen.Controls.Add(this.btnDuplicate);
            this.gbOpen.Controls.Add(this.btnDelete);
            this.gbOpen.Controls.Add(this.btnOpen);
            this.gbOpen.Controls.Add(this.lbMaps);
            this.gbOpen.Controls.Add(label7);
            this.gbOpen.Controls.Add(label5);
            this.gbOpen.Controls.Add(this.tbMapFolder);
            this.gbOpen.Location = new System.Drawing.Point(312, 13);
            this.gbOpen.Name = "gbOpen";
            this.gbOpen.Size = new System.Drawing.Size(279, 415);
            this.gbOpen.TabIndex = 3;
            this.gbOpen.TabStop = false;
            this.gbOpen.Text = "Открыть карту";
            // 
            // btnDuplicate
            // 
            this.btnDuplicate.Enabled = false;
            this.btnDuplicate.Location = new System.Drawing.Point(179, 19);
            this.btnDuplicate.Name = "btnDuplicate";
            this.btnDuplicate.Size = new System.Drawing.Size(94, 29);
            this.btnDuplicate.TabIndex = 6;
            this.btnDuplicate.Text = "Дублировать";
            this.btnDuplicate.UseVisualStyleBackColor = true;
            this.btnDuplicate.Click += new System.EventHandler(this.btnDuplicate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(99, 18);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(74, 30);
            this.btnDelete.TabIndex = 5;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lbMaps
            // 
            this.lbMaps.FormattingEnabled = true;
            this.lbMaps.HorizontalScrollbar = true;
            this.lbMaps.Location = new System.Drawing.Point(6, 132);
            this.lbMaps.MultiColumn = true;
            this.lbMaps.Name = "lbMaps";
            this.lbMaps.Size = new System.Drawing.Size(267, 277);
            this.lbMaps.TabIndex = 0;
            this.lbMaps.SelectedIndexChanged += new System.EventHandler(this.lbMaps_SelectedIndexChanged);
            // 
            // tbMapFolder
            // 
            this.tbMapFolder.Location = new System.Drawing.Point(6, 80);
            this.tbMapFolder.Name = "tbMapFolder";
            this.tbMapFolder.Size = new System.Drawing.Size(267, 20);
            this.tbMapFolder.TabIndex = 3;
            this.tbMapFolder.Text = "C:\\TwilightMaps\\";
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(603, 476);
            this.Controls.Add(this.gbOpen);
            this.Controls.Add(this.gbCreate);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Twilight Editor Startup";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.gbCreate.ResumeLayout(false);
            this.gbCreate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).EndInit();
            this.gbOpen.ResumeLayout(false);
            this.gbOpen.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbStyles;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox gbCreate;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.NumericUpDown nudHeight;
        private System.Windows.Forms.NumericUpDown nudWidth;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.GroupBox gbOpen;
        private System.Windows.Forms.ListBox lbMaps;
        private System.Windows.Forms.TextBox tbMapFolder;
        private System.Windows.Forms.ListBox lbFields;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnDuplicate;

    }
}