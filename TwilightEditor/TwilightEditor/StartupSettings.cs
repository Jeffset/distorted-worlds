﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.TwilightEditor
{
    [Serializable]
    public sealed class StartupSettings
    {
        public Type[] FieldSet { get; set; }
        public Type[] MapElementSet { get; set; }
        public Type BaseField { get; set; }
        public string Style { get; set; }
        public Vector2 Size { get; set; }
        public string FileNameToOpen { get; set; }
        public enum StartupType { CreateNew, OpenExisting };
        public StartupType Type { get; set; }

        public TwilightField CreateBaseField()
        {
            return FieldSet.First(f => { return (f.GetCustomAttributes(true)[0] as MapSystem.FieldClassAttribute).IsDefault; })
                            .GetConstructor(System.Type.EmptyTypes).Invoke(new object[0]) as TwilightField;
        }
    }
}
