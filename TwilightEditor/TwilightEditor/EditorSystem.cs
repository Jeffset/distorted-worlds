﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DWOaO.TwilightEditor
{
    [Serializable]
    public sealed class StartupSettings
    {
        public Type[] FieldSet { get; set; }
        public Type[] MapElementSet { get; set; }
        public Type BaseField { get; set; }
        public string Style { get; set; }
        public Vector2 Size { get; set; }
        public string FileName { get; set; }
        public enum StartupType { CreateNew, OpenExisting };
        public StartupType Type { get; set; }

        public ConstructorInfo CreateBaseField()
        {
            return FieldSet.First(f => { return (f.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true)[0] as MapSystem.EditorFieldInfoAttribute).IsDefault; })
                            .GetConstructor(System.Type.EmptyTypes);
        }
        public TwilightField CreateField(int index)
        {
            return FieldSet[index].GetConstructor(System.Type.EmptyTypes).Invoke(new object[0]) as TwilightField;
        }
        public TwilightMapObject CreateMapObject(int index)
        {
            return MapElementSet[index].GetConstructor(System.Type.EmptyTypes).Invoke(new object[0]) as TwilightMapObject; 
        }
    }

    public enum ToolType {Field, MapObject }
    
    [Serializable]
    public sealed class EditorControl
    {
        public StartupSettings settings { get; set; }
        public ToolType ToolType { get; set; }
        public int CurrentToolIndex { get; set; }

        public bool ExitRequest { get; set; }
        public bool ExitResponse { get; set; }
        public bool SaveRequest { get; set; }
        public bool Saved { get; set; }
    }
}
