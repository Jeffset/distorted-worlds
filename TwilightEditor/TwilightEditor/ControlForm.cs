﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DWOaO.TwilightEditor
{
    public partial class ControlForm : Form
    {
        private EditorControl editorControl;

        public ControlForm(EditorControl ec)
        {
            editorControl = ec;
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            editorControl.SaveRequest = true;
            Visible = false;
        }

        private void ToolTypeChanged(object sender, EventArgs e)
        {
            lbFieldTools.Enabled = rbFields.Checked;
            lbMapObjects.Enabled = rbMapObjects.Checked;
            if (rbFields.Checked) editorControl.ToolType = ToolType.Field;
            else if (rbMapObjects.Checked) editorControl.ToolType = ToolType.MapObject;
            SelectedToolChanged(null, null);
        }

        private void SelectedToolChanged(object sender, EventArgs e)
        {
            if (lbFieldTools.Enabled)
            {
                txtCurrentTool.Text = "Текущий инструмент: " + lbFieldTools.SelectedItem.ToString();
                editorControl.CurrentToolIndex = lbFieldTools.SelectedIndex;
            }
            else if (lbMapObjects.Enabled)
            {
                txtCurrentTool.Text = "Текущий инструмент: " + lbMapObjects.SelectedItem.ToString();
                editorControl.CurrentToolIndex = lbMapObjects.SelectedIndex;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            editorControl.ExitRequest = true;
        }

        private void ControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!editorControl.ExitRequest)
                e.Cancel = true;
            if (editorControl.ExitResponse)
                e.Cancel = false;
            Visible = false;
        }

        private void ControlForm_Load(object sender, EventArgs e)
        {
            lbFieldTools.Items.AddRange(editorControl.settings.FieldSet.Select
                (f => { return (f.GetCustomAttributes(typeof(MapSystem.EditorFieldInfoAttribute), true)[0] as MapSystem.EditorFieldInfoAttribute).FriendlyName; }).ToArray());
            lbMapObjects.Items.AddRange(editorControl.settings.MapElementSet.Select
                (f => { return (f.GetCustomAttributes(typeof(MapSystem.EditorMapObjectInfoAttribute), true)[0] as MapSystem.EditorMapObjectInfoAttribute).FriendlyName; }).ToArray());
            lbFieldTools.SelectedIndex = 1;
            lbMapObjects.SelectedIndex = 0;
            editorControl.ToolType = ToolType.Field;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Visible = false;
        }

        private void ControlForm_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible) return;
            btnSave.Enabled = !editorControl.Saved;
        }
    }
}
