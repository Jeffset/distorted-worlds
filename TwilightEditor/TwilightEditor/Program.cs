using System;
using System.Reflection;
using System.Windows.Forms;
using System.Linq;

namespace DWOaO.TwilightEditor
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();

            StartupSettings ss = new StartupSettings();
            SettingsForm sf = new SettingsForm();
            sf.Settings = ss;
            if (args.Length == 0)
            {
                DialogResult dr = sf.ShowDialog();
                if (dr == DialogResult.Cancel) return;
            }
            else
            {
                var twas = Assembly.GetAssembly(typeof(DWOaO.XNAPlatform));
                Type[] types = twas.GetTypes();
                ss.FieldSet = types.Where(t =>
                {
                    var atts = t.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true) as DWOaO.MapSystem.EditorFieldInfoAttribute[];
                    if (atts.Length != 0)
                        return true;
                    else
                        return false;
                }).ToArray();
                ss.MapElementSet = types.Where(t =>
                {
                    return (t.GetCustomAttributes(typeof(MapSystem.EditorMapObjectInfoAttribute), true).Length != 0);
                }).ToArray();
                ss.FileName = args[0];
                ss.Type = StartupSettings.StartupType.OpenExisting;
            }

            XNAPlatform core = new XNAPlatform(Misc.GraphicMode.Windowed);
            //TwilightEditor engine = new TwilightEditor(ss);
            { core.ShouldBeLoaded = false; core.GameEngine = new TwilightEditor(ss); }
            core.Run();
        }
    }
#endif
}

