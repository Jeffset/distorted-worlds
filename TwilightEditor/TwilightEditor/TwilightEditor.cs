using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using DWOaO;
using WinDraw = System.Drawing;
using WinForms = System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.IO;
using System.Runtime.Serialization;

namespace DWOaO.TwilightEditor
{
    [Serializable]
    public partial class TwilightEditor : TwilightGameStructEngine3
    {
        [NonSerialized]
        WinForms.Form form;

        private StartupSettings settings;

        private EditorControl editorControl = new EditorControl();

        [NonSerialized]
        private ControlForm controlForm;

        [NonSerialized]
        TwilightObject pointer;
        TwilightMapObject currentMapObject;

        public TwilightEditor(StartupSettings ss)
            : base(ss.Size)
        {
            _Init(ss);
        }

        private void _Init(StartupSettings ss)
        {
            XNAPlatform.Instance.IsMouseVisible = false;
            settings = ss;
            editorControl.settings = ss;
            controlForm = new ControlForm(editorControl);
            controlForm.btnSave.Click += btnSave_Click;
            //WinForms.Application.EnableVisualStyles();
            form = WinForms.Control.FromHandle(XNAPlatform.Instance.Window.Handle) as WinForms.Form;
            form.FormBorderStyle = WinForms.FormBorderStyle.None;
            form.SetDesktopLocation(0, 0);
            form.KeyPreview = true;
            form.PreviewKeyDown += form_PreviewKeyDown;
            //
        }

        public override void Render()
        {
            base.Render();
            GameMap.RenderEditorMap();
        }

        private void form_PreviewKeyDown(object sender, WinForms.PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == WinForms.Keys.Space)
                controlForm.Visible = true;

            if (currentMapObject == null) return;

            if (e.KeyCode == WinForms.Keys.A)
            {
                currentMapObject.Width -= 10;
            }
            else if (e.KeyCode == WinForms.Keys.D)
            {
                currentMapObject.Width += 10;
            }
            else if (e.KeyCode == WinForms.Keys.W)
            {
                currentMapObject.Height += 10;
            }
            else if (e.KeyCode == WinForms.Keys.S)
            {
                currentMapObject.Height -= 10;
            }
            else if (e.KeyCode == WinForms.Keys.Z)
            {
                currentMapObject.Flip = currentMapObject.Flip == SpriteEffects.None ? SpriteEffects.FlipHorizontally : SpriteEffects.None;
            }
            else if (e.KeyCode == WinForms.Keys.Delete)
            {
                GameMap.UnregisterFreeMapObject(currentMapObject);
                currentMapObject = null;
                RegisterObject(pointer);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            GameMap.SaveGameMap(editorControl.settings.FileName);
            WinForms.MessageBox.Show("����� ������� ��������� =)", "=)", WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Information);
        }

        public override void Construct()
        {
            base.Construct();
            InitPointer();
            if (settings.Type == StartupSettings.StartupType.OpenExisting)
            { GameMap = TwilightGameMap3.LoadGameMap(settings.FileName); GameMap.ConstrainCameraForThisMap(Camera); }
            else
            { GameMap = TwilightGameMap3.CreateGameMap((int)settings.Size.X, (int)settings.Size.Y, settings.CreateBaseField()); }

            Thread t = new Thread(() => { WinForms.Application.Run(controlForm); });
            t.IsBackground = true;
            t.Start();
        }

        private void InitPointer()
        {
            if (pointer != null) return;
            pointer = new TwilightObject(new AnimTexture(@"ShaderMaps\solid"), new Vector2(50.0f));
            pointer.RGBMulti.Value = Color.DarkSlateGray.ToVector3();
            pointer.Position.Ctrl<Anim.V2MouseCtrl>().Cfg();
            RegisterObject(pointer);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (editorControl.ExitRequest)
            {
                editorControl.ExitResponse = true;
                controlForm.Close();
                XNAPlatform.Instance.Exit();
            }
            switch (editorControl.ToolType)
            {
                case ToolType.Field:
                    ProcessFieldTool();
                    break;
                case ToolType.MapObject:
                    ProcessMapObjectTool();
                    break;
            }
        }

        private void ProcessMapObjectTool()
        {
            if (form.Focused && Global.Mouse.LeftButton == ButtonState.Pressed && Global.Mouse.RightButton == ButtonState.Released)// ���� ��� ������.
            {
                // �����, ����� ������������ ������ ��� ����� ���, ����� ������ currentMapObject, �������� ��� �� ����, ������ ������.
                // � ���� ����� ������������ �������� � ��������.
                if (currentMapObject == null)
                {
                    currentMapObject = settings.CreateMapObject(editorControl.CurrentToolIndex);
                    UnregisterObject(pointer);
                    GameMap.RegisterFreeMapObject(currentMapObject);
                    currentMapObject.Position.Ctrl<Anim.V2MouseCtrl>().Cfg();
                    ProcessObjectTransform();
                }
                else { }//����� ������ ������ ���������� ������ �� ����, ���� ������������ �� �������� ���.
            }
            else if (form.Focused && Global.Mouse.LeftButton == ButtonState.Released && Global.Mouse.RightButton == ButtonState.Released)// ���� ��� ��������.
            {
                // �����, ����� ������������ ������ ��� �������� ���, ����� ����� currentMapObject � ����, �������� ��� ������, �������� ������.
                if (currentMapObject != null)
                {
                    currentMapObject.Position.TerminateAnimation();
                    //currentMapObject.Level = Misc.TwilightLayer.ActorLayer;
                    currentMapObject = null;
                    RegisterObject(pointer);
                }
            }
            if (form.Focused && Global.Mouse.RightButton == ButtonState.Pressed && Global.Mouse.LeftButton == ButtonState.Released)// ���� ��� ������.
            {
                // �����, ����� ������������ ������ ��� ����� ���, ����� ����� ������ ��� ��������(���� ����),
                // �������� ��� � currentMapObject, �������� �� ����, �������� ������.
                // � ���� ����� ������������ �������� � ��������.
                if (currentMapObject == null)
                {
                    TwilightObject[] obj = GameMap.QueryFreeMapObject(Global.MousePos).ToArray();
                    if (obj.Length != 0)
                    {
                        currentMapObject = obj[0] as TwilightMapObject;
                        UnregisterObject(pointer);
                        //GameMap.RegisterFreeMapObject(currentMapObject);
                        currentMapObject.Position.Ctrl<Anim.V2MouseCtrl>().Cfg(true);
                    }
                    ProcessObjectTransform();
                }
                if (currentMapObject as TwilightEnergySource != null)
                {
                    var tes = currentMapObject as TwilightEnergySource;
                    Point fp = tes.FieldPos;
                    tes.EnergySource = new EnergySource(tes);
                    var f = GameMap.FieldMatrix[fp.X, fp.Y];
                    //f.Settler = tes.EnergySource;
                    tes.EnergySource.EnergySourceBuilding.Light.LightColor = f.GetAvColor().ToVector3();
                }
            }
            else if (form.Focused && Global.Mouse.RightButton == ButtonState.Released && Global.Mouse.LeftButton == ButtonState.Released)// ���� ��� ��������.
            {
                // �����, ����� ������������ ������ ��� �������� ���, ������� ������ � ����, �������� ������, ���������� ������
                if (currentMapObject != null)
                {
                    currentMapObject.Position.TerminateAnimation();
                    //currentMapObject.Level = Misc.TwilightLayer.ActorLayer;
                    currentMapObject = null;
                    RegisterObject(pointer);
                }
            }
        }

        private void ProcessObjectTransform()
        {
        }

        private void ProcessFieldTool()
        {
            if (form.Focused && Global.Keyboard.IsKeyDown(Keys.LeftAlt) && Global.Mouse.LeftButton == ButtonState.Pressed)
            {
                Vector2 mp = Global.MousePos;
                Point fp = new Point((int)mp.X / (int)Global.Cell_H, (int)mp.Y / (int)Global.Cell_V);
                TwilightField f = GameMap.FieldMatrix[fp.X, fp.Y];
                Type t = f.GetType();
                int c = 0; while (settings.FieldSet[c] != t) c++;
                editorControl.CurrentToolIndex = c;
            }
            else if (form.Focused && Global.Mouse.LeftButton == ButtonState.Pressed || Global.Mouse.RightButton == ButtonState.Pressed)
            {
                Vector2 mp = Global.MousePos;
                Point fp = new Point((int)mp.X / (int)Global.Cell_H, (int)mp.Y / (int)Global.Cell_V);
                int oldTool = editorControl.CurrentToolIndex;
                if (Global.Mouse.RightButton == ButtonState.Pressed) editorControl.CurrentToolIndex = 0;
                Rectangle tool = new Rectangle((int)mp.X, (int)mp.Y, 50, 50);
                TwilightField f = GameMap.FieldMatrix[fp.X, fp.Y];
                string toolName = editorControl.settings.FieldSet[editorControl.CurrentToolIndex].Name;
                int x = fp.X, y = fp.Y;
                if (f.Bounds.Contains(tool))// ������: �� ������
                {
                    #region CaseCenter
                    // ���� ���������, �� ��� ��, ���� �� ������ =)
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, � ��������� ���������
                    {
                        //var oldf = f; string bf =oldf.Attribute.IsDefault ? "Default" : oldf.GetName();
                        f = ReplaceField(x, y);// f.fieldMaskConfig.BackField = bf; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y - 1]; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y - 1]; f.fieldMaskConfig.c4 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y + 1]; f.fieldMaskConfig.c2 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y + 1]; f.fieldMaskConfig.c1 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    #endregion
                }
                else if (f.Right < tool.Right && f.Bottom < tool.Bottom)// ������: �������
                {
                    #region CaseCorner
                    // 1) ��������� ������ �������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� right, corner3, bottom � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.r = true; f.fieldMaskConfig.c3 = true; f.fieldMaskConfig.b = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y - 1]; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� right, corner3, bottom
                    {
                        f.fieldMaskConfig.r = true; f.fieldMaskConfig.c3 = true; f.fieldMaskConfig.b = true; f.CalculateTexture();
                    }
                    // 2) ��������� ������� �������� ����
                    x++; f = GameMap.FieldMatrix[x, y];// �������� ��������� �� 1 �� ����� (������), ��������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� left, corner4, bottom � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.l = true; f.fieldMaskConfig.c4 = true; f.fieldMaskConfig.b = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y - 1]; f.fieldMaskConfig.c4 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� left, corner4, bottom
                    {
                        f.fieldMaskConfig.l = true; f.fieldMaskConfig.c4 = true; f.fieldMaskConfig.b = true; f.CalculateTexture();
                    }
                    // 3) ��������� ������� ������� ����
                    y++; f = GameMap.FieldMatrix[x, y];// �������� ��������� �� 1 �� ������� (����), ��������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� left, corner1, top � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.l = true; f.fieldMaskConfig.c1 = true; f.fieldMaskConfig.t = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y + 1]; f.fieldMaskConfig.c1 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� left, corner4, bottom
                    {
                        f.fieldMaskConfig.l = true; f.fieldMaskConfig.c1 = true; f.fieldMaskConfig.t = true; f.CalculateTexture();
                    }
                    // 4) ��������� ������ ������� ����
                    x--; f = GameMap.FieldMatrix[x, y];// �������� ��������� �� -1 �� ����� (�����), ��������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� right, corner2, top � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.r = true; f.fieldMaskConfig.c2 = true; f.fieldMaskConfig.t = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y + 1]; f.fieldMaskConfig.c2 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� right, corner2, top
                    {
                        f.fieldMaskConfig.r = true; f.fieldMaskConfig.c2 = true; f.fieldMaskConfig.t = true; f.CalculateTexture();
                    }
                    #endregion
                }
                else if (f.Right < tool.Right)// ������: ��� ���� �������������
                {
                    #region CaseTwoHorizontal
                    // 1) ��������� ������ ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� right � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.r = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y - 1]; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y + 1]; f.fieldMaskConfig.c2 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� right
                    {
                        f.fieldMaskConfig.r = true; f.CalculateTexture();
                    }
                    // 2) ��������� ������� ����
                    x++; f = GameMap.FieldMatrix[x, y];// �������� ��������� �� 1 �� ����� (������), ��������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� left � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.l = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y - 1]; f.fieldMaskConfig.c4 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); f = GameMap.FieldMatrix[x - 1, y + 1]; f.fieldMaskConfig.c2 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y + 1]; f.fieldMaskConfig.c1 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� left
                    {
                        f.fieldMaskConfig.l = true; f.CalculateTexture();
                    }
                    #endregion
                }
                else if (f.Bottom < tool.Bottom)// ������: ��� ���� �����������
                {
                    #region CaseTwoVertical
                    // 1) ��������� �������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� bottom � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.b = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y - 1]; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y - 1]; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.b = false; f.fieldMaskConfig.c3 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y - 1]; f.fieldMaskConfig.c4 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� bottom
                    {
                        f.fieldMaskConfig.b = true; f.CalculateTexture();
                    }
                    // 2) ��������� ������� ����
                    y++; f = GameMap.FieldMatrix[x, y];// �������� ��������� �� 1 �� ������� (����), ��������� ����
                    if (f.Name != toolName)// ���� �� ���������, �������� ����, ��������� ��� top � ��������� ���������
                    {
                        f = ReplaceField(x, y);
                        f.fieldMaskConfig.t = true; f.CalculateTexture();
                        try { f = GameMap.FieldMatrix[x - 1, y]; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.c3 = false; f.fieldMaskConfig.r = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c4 = false; f.fieldMaskConfig.l = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x - 1, y + 1]; f.fieldMaskConfig.c2 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x, y + 1]; f.fieldMaskConfig.c1 = false; f.fieldMaskConfig.c2 = false; f.fieldMaskConfig.t = false; f.CalculateTexture(); }
                        catch (Exception) { }
                        try { f = GameMap.FieldMatrix[x + 1, y + 1]; f.fieldMaskConfig.c1 = false; f.CalculateTexture(); }
                        catch (Exception) { }
                    }
                    else// ���� ���������, �� ������ ��������� top
                    {
                        f.fieldMaskConfig.t = true; f.CalculateTexture();
                    }
                    #endregion
                }
                editorControl.CurrentToolIndex = oldTool;
                editorControl.Saved = false;
            }
        }

        private TwilightField ReplaceField(int x, int y)
        {
            TwilightField f = editorControl.settings.CreateField(editorControl.CurrentToolIndex);
            f.Position.Value = new Vector2(x * Global.Cell_H, y * Global.Cell_V); GameMap.FieldMatrix[x, y] = f;
            return f;
        }

        public override void SaveEngine(string fileNameToLoadFrom)
        {
            //UnregisterObject(pointer);
            //base.SaveEngine(fileNameToLoadFrom);
            //editorControl.SaveRequest = false;
            //editorControl.Saved = true;
            //RegisterObject(pointer);
        }
    }
}
