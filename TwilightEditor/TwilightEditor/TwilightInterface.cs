﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using DWOaO;
using WinDraw = System.Drawing;
using WinForms = System.Windows.Forms;
using System.IO;

namespace DWOaO.TwilightEditor
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public partial class TwilightEditor : TwilightEngine
    {
        const string MapFolder = @"C:\DWMaps\";

        WinForms.Form form;
        WinForms.FlowLayoutPanel Menu = new WinForms.FlowLayoutPanel();
        WinForms.Button Open = new WinForms.Button();
        WinForms.ListView Maps = new WinForms.ListView();

        protected override void Initialize()
        {
            base.Initialize();

            //
            //Menu.Size = new WinDraw.Size(50, 50);
            //Menu.Dock = WinForms.DockStyle.Right;
            //Menu.AutoSize = true;
            //Menu.FlowDirection = WinForms.FlowDirection.TopDown;
            //form.Controls.Add(Menu);
            ////
            //Open.AutoSize = true;
            //Open.Text = "Open/Create";
            //Open.Click += Open_Click;
            //Menu.Controls.Add(Open);
            ////
            //Maps.Items.AddRange(Directory.GetFiles(MapFolder, "*.twilight").Select(o=>{return new WinForms.ListViewItem(o);}).ToArray());
            //Menu.Controls.Add(Maps);
        }

        void InterfaceControl()
        {
            WinForms.Form w = new WinForms.Form();
            WinForms.Application.Run(w);
        }

        void Open_Click(object sender, EventArgs e)
        {
            //
        }
    }
}
