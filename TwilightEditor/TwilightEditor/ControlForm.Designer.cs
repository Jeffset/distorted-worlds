﻿namespace DWOaO.TwilightEditor
{
    partial class ControlForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFieldTools = new System.Windows.Forms.ListBox();
            this.lbMapObjects = new System.Windows.Forms.ListBox();
            this.rbFields = new System.Windows.Forms.RadioButton();
            this.rbMapObjects = new System.Windows.Forms.RadioButton();
            this.gbTool = new System.Windows.Forms.GroupBox();
            this.txtCurrentTool = new System.Windows.Forms.Label();
            this.gbMenu = new System.Windows.Forms.GroupBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbTool.SuspendLayout();
            this.gbMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbFieldTools
            // 
            this.lbFieldTools.FormattingEnabled = true;
            this.lbFieldTools.Location = new System.Drawing.Point(6, 43);
            this.lbFieldTools.Name = "lbFieldTools";
            this.lbFieldTools.Size = new System.Drawing.Size(190, 225);
            this.lbFieldTools.TabIndex = 0;
            this.lbFieldTools.SelectedIndexChanged += new System.EventHandler(this.SelectedToolChanged);
            // 
            // lbMapObjects
            // 
            this.lbMapObjects.Enabled = false;
            this.lbMapObjects.FormattingEnabled = true;
            this.lbMapObjects.Location = new System.Drawing.Point(202, 43);
            this.lbMapObjects.Name = "lbMapObjects";
            this.lbMapObjects.Size = new System.Drawing.Size(195, 225);
            this.lbMapObjects.TabIndex = 0;
            this.lbMapObjects.SelectedIndexChanged += new System.EventHandler(this.SelectedToolChanged);
            // 
            // rbFields
            // 
            this.rbFields.AutoSize = true;
            this.rbFields.Checked = true;
            this.rbFields.Location = new System.Drawing.Point(66, 20);
            this.rbFields.Name = "rbFields";
            this.rbFields.Size = new System.Drawing.Size(54, 17);
            this.rbFields.TabIndex = 1;
            this.rbFields.TabStop = true;
            this.rbFields.Text = "Поля:";
            this.rbFields.UseVisualStyleBackColor = true;
            this.rbFields.CheckedChanged += new System.EventHandler(this.ToolTypeChanged);
            // 
            // rbMapObjects
            // 
            this.rbMapObjects.AutoSize = true;
            this.rbMapObjects.Location = new System.Drawing.Point(222, 20);
            this.rbMapObjects.Name = "rbMapObjects";
            this.rbMapObjects.Size = new System.Drawing.Size(134, 17);
            this.rbMapObjects.TabIndex = 1;
            this.rbMapObjects.Text = "Объекты ландшафта:";
            this.rbMapObjects.UseVisualStyleBackColor = true;
            this.rbMapObjects.CheckedChanged += new System.EventHandler(this.ToolTypeChanged);
            // 
            // gbTool
            // 
            this.gbTool.Controls.Add(this.txtCurrentTool);
            this.gbTool.Controls.Add(this.lbMapObjects);
            this.gbTool.Controls.Add(this.rbMapObjects);
            this.gbTool.Controls.Add(this.lbFieldTools);
            this.gbTool.Controls.Add(this.rbFields);
            this.gbTool.Location = new System.Drawing.Point(12, 12);
            this.gbTool.Name = "gbTool";
            this.gbTool.Size = new System.Drawing.Size(403, 307);
            this.gbTool.TabIndex = 2;
            this.gbTool.TabStop = false;
            this.gbTool.Text = "Инструмент ландшафта";
            // 
            // txtCurrentTool
            // 
            this.txtCurrentTool.AutoSize = true;
            this.txtCurrentTool.Location = new System.Drawing.Point(78, 273);
            this.txtCurrentTool.Name = "txtCurrentTool";
            this.txtCurrentTool.Size = new System.Drawing.Size(117, 13);
            this.txtCurrentTool.TabIndex = 2;
            this.txtCurrentTool.Text = "Текущий инструмент:";
            // 
            // gbMenu
            // 
            this.gbMenu.Controls.Add(this.btnExit);
            this.gbMenu.Controls.Add(this.btnOK);
            this.gbMenu.Controls.Add(this.btnSave);
            this.gbMenu.Location = new System.Drawing.Point(421, 12);
            this.gbMenu.Name = "gbMenu";
            this.gbMenu.Size = new System.Drawing.Size(155, 307);
            this.gbMenu.TabIndex = 3;
            this.gbMenu.TabStop = false;
            this.gbMenu.Text = "Меню";
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(6, 245);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(143, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Выйти";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(6, 141);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(143, 69);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(6, 216);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(143, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Сохранить";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ControlForm
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(588, 334);
            this.ControlBox = false;
            this.Controls.Add(this.gbMenu);
            this.Controls.Add(this.gbTool);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ControlForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Меню Twilight Editor";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ControlForm_FormClosing);
            this.Load += new System.EventHandler(this.ControlForm_Load);
            this.VisibleChanged += new System.EventHandler(this.ControlForm_VisibleChanged);
            this.gbTool.ResumeLayout(false);
            this.gbTool.PerformLayout();
            this.gbMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbFieldTools;
        private System.Windows.Forms.ListBox lbMapObjects;
        private System.Windows.Forms.RadioButton rbFields;
        private System.Windows.Forms.RadioButton rbMapObjects;
        private System.Windows.Forms.GroupBox gbTool;
        private System.Windows.Forms.GroupBox gbMenu;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label txtCurrentTool;
        private System.Windows.Forms.Button btnOK;
        public System.Windows.Forms.Button btnSave;
    }
}