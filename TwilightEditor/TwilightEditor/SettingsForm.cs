﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using WinDraw = System.Drawing;
using WinForms = System.Windows.Forms;

namespace DWOaO.TwilightEditor
{
    public partial class SettingsForm : WinForms.Form
    {
        public StartupSettings Settings { get; set; }
        List<string> styles = new List<string>();

        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            if (File.Exists("settings.conf")) tbMapFolder.Text = File.ReadAllText("settings.conf");
            var twas = Assembly.GetAssembly(typeof(DWOaO.XNAPlatform));
            Type[] types = twas.GetTypes();
            Settings.FieldSet = types.Where(t =>
            {
                var atts = t.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true) as DWOaO.MapSystem.EditorFieldInfoAttribute[];
                if (atts.Length != 0)
                {
                    DWOaO.MapSystem.EditorFieldInfoAttribute att = atts[0];
                    if (!lbStyles.Items.Contains(att.Style)) lbStyles.Items.Add(att.Style);
                    return true;
                }
                else
                    return false;
            }).ToArray();
            Settings.MapElementSet = types.Where(t =>
            {
                return (t.GetCustomAttributes(typeof(MapSystem.EditorMapObjectInfoAttribute), true).Length != 0);
            }).ToArray();
            if (Directory.Exists(tbMapFolder.Text))
                RefreshSavedMaps();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            Settings.Type = StartupSettings.StartupType.OpenExisting;
            Settings.FileName = tbMapFolder.Text + lbMaps.SelectedItem.ToString() + ".twilight";
            DialogResult = WinForms.DialogResult.OK; Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try { ValidateUserInputOnCreate(); }
            catch (System.Exception err)
            { WinForms.MessageBox.Show(err.Message, "Невозможно создать карту!", WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation); return; }

            Settings.FieldSet = Settings.FieldSet.Where(t =>
                {
                    var atts = t.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true) as DWOaO.MapSystem.EditorFieldInfoAttribute[];
                    DWOaO.MapSystem.EditorFieldInfoAttribute att = atts[0];
                    if (att.Style == lbStyles.SelectedItem.ToString())
                        return true;
                    else
                        return false;
                }).ToArray();
            Settings.MapElementSet = Settings.MapElementSet.Where(t =>
            {
                var atts = t.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorMapObjectInfoAttribute), true) as DWOaO.MapSystem.EditorMapObjectInfoAttribute[];
                DWOaO.MapSystem.EditorMapObjectInfoAttribute att = atts[0];
                if (att.Style == lbStyles.SelectedItem.ToString())
                    return true;
                else
                    return false;
            }).ToArray();
            Settings.FileName = tbMapFolder.Text + tbName.Text + ".twilight";
            Settings.Type = StartupSettings.StartupType.CreateNew;
            Settings.Size = new Vector2((int)nudWidth.Value, (int)nudHeight.Value);
            Settings.Style = lbStyles.SelectedItem.ToString();
            Settings.BaseField = Settings.FieldSet.First(f => { return (f.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true)[0] as MapSystem.EditorFieldInfoAttribute).IsDefault; });
            DialogResult = WinForms.DialogResult.OK; Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = WinForms.DialogResult.Cancel;
            Close();
        }

        private void ValidateUserInputOnCreate()
        {
            if (tbName.Text == "")
                throw new System.Exception("Введите название карты!");
            else if (tbName.Text.IndexOfAny(Path.GetInvalidFileNameChars()) != -1)
                throw new System.Exception("Название карты содержит недопустимые символы!");
            if (lbStyles.SelectedItem == null)
                throw new System.Exception("Обозначте стиль карты!");
            try { if (!Directory.Exists(tbMapFolder.Text)) Directory.CreateDirectory(tbMapFolder.Text); }
            catch (System.Exception e) { throw new System.Exception("Невозможно создать папку для карт! \n\r" + e.Message); }
            if (File.Exists(tbMapFolder.Text + tbName.Text + ".twilight"))
                throw new System.Exception("Карта с таким именем уже существует!");
            File.WriteAllText("settings.conf", tbMapFolder.Text);
        }

        private void lbStyles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbStyles.SelectedItem == null) { lbFields.Items.Clear(); return; }
            lbFields.Items.Clear();
            lbFields.Items.AddRange(Settings.FieldSet.Where(t =>
            {
                var atts = t.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true) as DWOaO.MapSystem.EditorFieldInfoAttribute[];
                DWOaO.MapSystem.EditorFieldInfoAttribute att = atts[0];
                if (att.Style == lbStyles.SelectedItem.ToString())
                    return true;
                else
                    return false;
            }).Select(t => { return t.Name; }).ToArray());
        }

        private void lbMaps_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbMaps.SelectedItem != null)
            {
                btnOpen.Enabled = true;
                btnDelete.Enabled = true;
                btnDuplicate.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var dr = WinForms.MessageBox.Show("Вы действиетльно хотите удалить карту " + lbMaps.SelectedItem.ToString() + "?", "Удаление карты",
                WinForms.MessageBoxButtons.YesNo, WinForms.MessageBoxIcon.Question);
            if (dr == WinForms.DialogResult.Yes)
            {
                File.Delete(tbMapFolder.Text + lbMaps.SelectedItem.ToString() + ".twilight");
                RefreshSavedMaps();
            }
        }

        private void RefreshSavedMaps()
        {
            lbMaps.Items.Clear();
            lbMaps.Items.AddRange(Directory.GetFiles(tbMapFolder.Text, "*.twilight").Select(f => { return Path.GetFileNameWithoutExtension(f); }).ToArray());
            btnOpen.Enabled = false;
            btnDelete.Enabled = false;
            btnDuplicate.Enabled = false;
        }

        private void btnDuplicate_Click(object sender, EventArgs e)
        {
            if (tbName.Text == "")
            {
                WinForms.MessageBox.Show("Введите имя карты-дубликата в поле \"Название\"!", "Ошибка дублирования карты",
                   WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
                return;
            }
            if (File.Exists(tbMapFolder.Text + tbName.Text + ".twilight"))
            {
                WinForms.MessageBox.Show("Карта с таком именем уже существует!", "Ошибка дублирования карты",
                   WinForms.MessageBoxButtons.OK, WinForms.MessageBoxIcon.Exclamation);
                return;
            }
            File.Copy(tbMapFolder.Text + lbMaps.SelectedItem.ToString() + ".twilight",
                tbMapFolder.Text + tbName.Text + ".twilight");
            RefreshSavedMaps();
        }
    }
}
