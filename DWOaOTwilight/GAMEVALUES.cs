﻿namespace DWOaO
{

    static class GRAPHIC_SETTINGS
    {
        public const int MAX_LIGHT_SOURCES_PER_LIGHTABLE = 20;
    }

    static class GVAL
    {
        public const int MANEUVER_DIST_DEFAULT = 7;

        #region ОПЫТ. УРОВЕНЬ.
        public const int OMNIT_MAX_LEVEL = 10;

        public static int OmnitReqEXPLevel(int level)
        {
            switch (level)
            {
                case 2:
                    return OMNIT_REQ_EXP_LEVEL_2;
                case 3:
                    return OMNIT_REQ_EXP_LEVEL_3;
                case 4:
                    return OMNIT_REQ_EXP_LEVEL_4;
                case 5:
                    return OMNIT_REQ_EXP_LEVEL_5;
                case 6:
                    return OMNIT_REQ_EXP_LEVEL_6;
                case 7:
                    return OMNIT_REQ_EXP_LEVEL_7;
                case 8:
                    return OMNIT_REQ_EXP_LEVEL_8;
                case 9:
                    return OMNIT_REQ_EXP_LEVEL_9;
                case 10:
                    return OMNIT_REQ_EXP_LEVEL_10;
                default:
                    return 0;
            }
        }
        private const int OMNIT_REQ_EXP_LEVEL_2 = 50;
        private const int OMNIT_REQ_EXP_LEVEL_3 = 150;
        private const int OMNIT_REQ_EXP_LEVEL_4 = 300;
        private const int OMNIT_REQ_EXP_LEVEL_5 = 400;
        private const int OMNIT_REQ_EXP_LEVEL_6 = 500;
        private const int OMNIT_REQ_EXP_LEVEL_7 = 600;
        private const int OMNIT_REQ_EXP_LEVEL_8 = 700;
        private const int OMNIT_REQ_EXP_LEVEL_9 = 1000;
        private const int OMNIT_REQ_EXP_LEVEL_10 = 1500;
        #endregion

        #region РЕГЕНЕРАЦИЯ. РЕЗЕРВУАРЫ
        public const int EXTRAINCARNATOR_SINTES_REGEN_RATE = 20;
        public const int EXTRAL_STASIS_REGEN_RANGE = 2;
        public const int EXTRAL_STASIS_REGEN_RATE_BASE = 65;

        public const int EXTRAINC_STASIS_REGEN_RATE_BASE = 50;
        public const int EXTRAINC_STASIS_REGEN_RANGE = 2;

        public const int INTRIN_STASIS_REGEN_RATE_BASE = 25;
        public const int INTRIN_SINTES_REGEN_RATE_BASE = 10;

        public const int INTRAINC_STASIS_REGEN_RATE_BASE = 5;
        #endregion
    }

}