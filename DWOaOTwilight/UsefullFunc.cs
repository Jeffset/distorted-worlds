﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.Misc
{
    public static class UF
    {
        public static Point V2tP(Vector2 v)
        { return new Point((int)v.X, (int)v.Y); }

        public static Point AbsToField(Vector2 v)
        {
            Point p = V2tP(v); p.X /= Global.Cell_H; p.Y /= Global.Cell_V;
            return p;
        }

        public static Vector2 FieldToAbs(int x, int y)
        {
            return new Vector2(x * Global.Cell_H, y * Global.Cell_V);
        }

        public static void PutOrigin(TwilightObject o)
        {
            o.Origin.Value = o.Size.Value * 0.5f;
        }
        public static void PutOrigin(TwilightObject o, Vector2 relation)
        {
            o.Origin.Value = o.Size.Value * relation;
        }

        public static Color RGBA(AnimFloat3 rgbmulti, AnimFloat opacity)
        {
            return Color.FromNonPremultiplied(new Vector4(rgbmulti.Value, opacity.Value));
        }

        public static void PutOn(TwilightObject operand, TwilightObject target)
        {
            operand.Position.Value = target.Position.Value + (target.Size.Value - operand.Size.Value) / 2.0f;
        }

        public static Vector2 PutOnA(TwilightObject operand, TwilightObject target)
        {
            return target.Position.Value + (target.Size.Value - operand.Size.Value) / 2.0f;
        }

        public static ActorModels.Orient GetOrient(Vector2 actPos, Vector2 watchPos)
        {
            Vector2 signVec = watchPos - actPos;

                if (signVec.Y >= 0 && signVec.X < 0)
                    if (Math.Abs(signVec.X) < Math.Abs(signVec.Y))
                        return ActorModels.Orient.ToUs;
                    else return ActorModels.Orient.ToLeft;
                else if (signVec.Y < 0 && signVec.X <= 0)
                    if (Math.Abs(signVec.X) < Math.Abs(signVec.Y))
                        return ActorModels.Orient.ToForward;
                    else return ActorModels.Orient.ToLeft;
                else if (signVec.Y >= 0 && signVec.X > 0)
                    if (Math.Abs(signVec.X) < Math.Abs(signVec.Y))
                        return ActorModels.Orient.ToUs;
                    else return ActorModels.Orient.ToRight;
                else if (signVec.Y < 0 && signVec.X >= 0)
                    if (Math.Abs(signVec.X) < Math.Abs(signVec.Y))
                        return ActorModels.Orient.ToForward;
                    else return ActorModels.Orient.ToRight;
                return ActorModels.Orient.ToUs;
        }

        public static bool IsInRange(TwilightField f1, TwilightField f2, int range)
        {
            return new Vector2(f1.FieldPos.X - f2.FieldPos.X, f1.FieldPos.Y - f2.FieldPos.Y).Length() <= range;
        }
    }
}
