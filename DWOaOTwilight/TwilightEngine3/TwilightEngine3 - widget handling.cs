﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DWOaO
{
    public abstract partial class TWE3
    {
        public LinkedList<TwilightObject> GraphWidgets { get; private set; }
        protected List<TwilightWidget> widgets = new List<TwilightWidget>();
        private TwilightWidget _enteredWidget;
        private bool _gameControlTemporaryDenied;
        public bool GameControlFullyDenied;
        public bool GameControlDenied { get { return GameControlFullyDenied || _gameControlTemporaryDenied; } }

        protected virtual void _UnregisterEventHandlers()
        {
            MouseDown -= TwilightEngine3_MouseDown;
            MouseUp -= TwilightEngine3_MouseUp;
            MouseMove -= TwilightEngine3_MouseMove;
        }

        protected virtual void _RegisterEventHandlers()
        {
            MouseDown += TwilightEngine3_MouseDown;
            MouseUp += TwilightEngine3_MouseUp;
            MouseMove += TwilightEngine3_MouseMove;
        }

        public void RegisterWidget(TwilightObject obj)
        {
            if (obj.IsRegistered) return;
            LinkedList<TwilightObject> layer = GraphWidgets;
            LinkedListNode<TwilightObject> node = layer.First;
            while (node != null && node.Value.Level <= obj.Level)
                node = node.Next;
            if (node == null)
            {
                layer.AddLast(obj);
            }
            else
            {
                layer.AddBefore(node, obj);
            }
            obj.IsRegistered = true;
            if (obj is TwilightWidget) widgets.Add(obj as TwilightWidget);
            obj.OnObjectRegistered();
        }
        public void UnregisterWidget(TwilightObject obj)
        {
            if (!obj.IsRegistered) return;
            GraphWidgets.Remove(obj);
            if (obj is TwilightWidget) widgets.Remove(obj as TwilightWidget);
            obj.IsRegistered = false;
            obj.OnObjectUnregistered();
        }

        void TwilightEngine3_MouseMove(MouseButton mb, Vector2 pos)
        {
            TwilightWidget newWidget = null;
            foreach (var widget in widgets)
                if (widget.Bounds.Contains(Misc.UF.V2tP(pos)))
                {
                    newWidget = widget; break;
                }
            if (_enteredWidget != newWidget)
            {
                if (_enteredWidget != null) _enteredWidget._MouseLeave();
                _enteredWidget = newWidget;
                if (_enteredWidget != null) _enteredWidget._MouseEnter();
            }
        }

        void TwilightEngine3_MouseUp(MouseButton mb, Vector2 pos)
        {
            for (int i = 0; i < widgets.Count; i++)
            {
                var widget = widgets[i];
                if (widget.Bounds.Contains(Misc.UF.V2tP(pos - Camera.Position.Value)))
                {
                    _gameControlTemporaryDenied = true;
                    widget._MouseUp(mb);
                }
            }
        }

        void TwilightEngine3_MouseDown(MouseButton mb, Vector2 pos)
        {
            foreach (var widget in widgets)
                if (widget.Bounds.Contains(Misc.UF.V2tP(pos - Camera.Position.Value)))
                {
                    _gameControlTemporaryDenied = true;
                    widget._MouseDown(mb);
                }
        }
    }
}
