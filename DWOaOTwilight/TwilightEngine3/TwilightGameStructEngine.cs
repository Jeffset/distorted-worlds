﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace DWOaO
{
    // Класс движка специально для реализации слоированной структуры, наличия КАРТЫ, 
    [Serializable]
    public class TwilightGameStructEngine3 : TWE3
    {
        public TwilightGameMap3 GameMap;

        public TwilightGameStructEngine3(Vector2 sizeInFields)
            : base(new Vector2(sizeInFields.X * Global.Cell_H, sizeInFields.Y * Global.Cell_V))
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            GameMap.UpdateGameMap();
        }
        public override void Render()
        {
            GameMap.RenderGameMap();
            base.Render();
        }
    }

    [Serializable]
    public class TwilightGameMap3
    {
        public TwilightField[,] FieldMatrix;
        public Point SizeInFields { get; private set; }
        public LinkedList<TwilightObject> FreeMapObjects = new LinkedList<TwilightObject>();
        // еще тут типо должен быть массив со зданиями
        //public string Style { get; private set; }

        public void SaveGameMap(string filename)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.OpenWrite(filename);
            bf.Serialize(fs, this);
            fs.Close();
            using (FileStream sfs = File.OpenWrite(filename + "2"))
            {
                BinaryWriter br = new BinaryWriter(sfs);
                //
                // Size in fields
                br.Write(this.SizeInFields.X);
                br.Write(this.SizeInFields.Y);
                // Matrix fields
                for (int i = 0; i < SizeInFields.X; i++)
                {
                    for (int j = 0; j < SizeInFields.Y; j++)
                    {
                        var f = FieldMatrix[i, j];
                        // Field typename for reflection
                        br.Write(f.GetType().FullName);
                        // Presence of settler (EnergySource)
                        br.Write(f.Settler != null);
                    }
                }
                // FreeMO count
                br.Write(FreeMapObjects.Count);
                // FreeMOs:
                foreach (var fmo in FreeMapObjects)
                {
                    // FMO type
                    br.Write(fmo.GetType().FullName);
                    // Position
                    br.Write(fmo.X); br.Write(fmo.Y);
                    // Size
                    br.Write(fmo.Width); br.Write(fmo.Height);
                }
            }

        }

        public static TwilightGameMap3 LoadGameMap(string filename)
        {
            /*BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.OpenRead(filename);
            TwilightGameMap3 map = bf.Deserialize(fs) as TwilightGameMap3;
            fs.Close();*/
            var lib = Assembly.GetExecutingAssembly();
            TwilightGameMap3 map = null;
            using (FileStream fs = File.OpenRead(filename + "2"))
            {
                BinaryReader br = new BinaryReader(fs);
                int w = br.ReadInt32(), h = br.ReadInt32();
                map = CreateGameMap(w, h);
                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < h; j++)
                    {
                        string s = br.ReadString();
                        Type type = Type.GetType(s);
                        map.FieldMatrix[i, j] = type.GetConstructor(Type.EmptyTypes).Invoke(new object[0]) as TwilightField;
                        map.FieldMatrix[i, j].Position.Value = new Vector2(i * Global.Cell_H, j * Global.Cell_V);
                        bool hasSettler = br.ReadBoolean();
                        if (hasSettler)
                        {
                            //map.FieldMatrix[i, j].Settler = new EnergySource(new MapSystem.AnticRuinsEnergySource());
                        }
                    }
                }
                int fmoCount = br.ReadInt32();
                for (int k = 0; k < fmoCount; k++)
                {
                    var fmo = Type.GetType(br.ReadString())
                        .GetConstructor(Type.EmptyTypes).Invoke(new object[0]) as TwilightMapObject;
                    fmo.X = br.ReadInt32(); fmo.Y = br.ReadInt32();
                    fmo.Width = br.ReadInt32(); fmo.Height = br.ReadInt32();
                    map.RegisterFreeMapObject(fmo);
                    //map.FreeMapObjects.AddLast(fmo);
                }
            }
            return map;
        }

        public void ConstrainCameraForThisMap(TwilightCamera camera)
        {
            camera.Position.Cnstr<Anim.V2RegionCnstr>().Cfg(new Vector2(), new Vector2(SizeInFields.X * Global.Cell_H, SizeInFields.Y * Global.Cell_V), camera.Size);
        }

        public static TwilightGameMap3 CreateGameMap(int w, int h, ConstructorInfo baseField = null)
        {
            TwilightGameMap3 map = new TwilightGameMap3();
            map.SizeInFields = new Point(w, h);
            map.FieldMatrix = new TwilightField[w, h];
            for (int x = 0; x < w; x++)
                for (int y = 0; y < h; y++)
                {
                    if (baseField != null)
                    {
                        map.FieldMatrix[x, y] = baseField.Invoke(new object[0]) as TwilightField;
                        map.FieldMatrix[x, y].Position.Value = new Vector2(x * Global.Cell_H, y * Global.Cell_V);// *Global.FLD;
                    }
                }
            return map;
        }

        public void RenderGameMap()
        {
            Rectangle cam = TWE3.twi.Camera.FieldBounds;
            for (int x = cam.Left; x < cam.Right; x++)
                for (int y = cam.Top; y < cam.Bottom; y++)
                    FieldMatrix[x, y].Render();
            cam = TWE3.twi.Camera.Bounds;
            //foreach (var o in FreeMapObjects)
            //{
            //    if (o.Bounds.Intersects(cam))
            //        o.Render();
            //}
        }
        public void RenderEditorMap()
        {
            //Rectangle cam = TWE3.twi.Camera.FieldBounds;
            //for (int x = cam.Left; x < cam.Right; x++)
            //    for (int y = cam.Top; y < cam.Bottom; y++)
            //        FieldMatrix[x, y].Render();
            Rectangle cam = TWE3.twi.Camera.Bounds;
            foreach (var o in FreeMapObjects)
            {
                if (o.Bounds.Intersects(cam))
                    o.Render();
            }
        }

        public void RegisterFreeMapObject(TwilightMapObject obj)
        {
            LinkedList<TwilightObject> layer = FreeMapObjects;
            LinkedListNode<TwilightObject> node = layer.First;
            while (node != null && node.Value.Level <= obj.Level)
                node = node.Next;
            if (node == null)
            {
                layer.AddLast(obj);
                obj.IsRegistered = true;
            }
            else
            {
                layer.AddBefore(node, obj);
                obj.IsRegistered = true;
            }
            obj.OnObjectRegistered();
        }
        public void UnregisterFreeMapObject(TwilightMapObject obj)
        {
            FreeMapObjects.Remove(obj);
            obj.IsRegistered = false;
            obj.OnObjectUnregistered();
        }

        public IEnumerable<TwilightObject> QueryFreeMapObject(Type type, Point pos)
        {
            return FreeMapObjects.Where(o => { return o.Bounds.Contains(pos) && o.GetType() == type; });
        }
        public IEnumerable<TwilightObject> QueryFreeMapObject(Point pos)
        {
            return FreeMapObjects.Where(o => { return o.Bounds.Contains(pos); });
        }
        public IEnumerable<TwilightObject> QueryFreeMapObject(Type type, Vector2 pos)
        {
            return QueryFreeMapObject(type, new Point((int)pos.X, (int)pos.Y));
        }
        public IEnumerable<TwilightObject> QueryFreeMapObject(Vector2 pos)
        {
            return QueryFreeMapObject(new Point((int)pos.X, (int)pos.Y));
        }

        public void UpdateGameMap() // Функция оптимизированного обновления карты.
        {
            //for (int x = 0; x < SizeInFields.X; x++)
            //{
            //    for (int y = 0; y < SizeInFields.Y; y++)
            //    {
            //        FieldMatrix[x, y].Update();
            //    }
            //}
            Rectangle cam = TWE3.twi.Camera.FieldBounds;
            for (int x = cam.Left; x < cam.Right; x++)
                for (int y = cam.Top; y < cam.Bottom; y++)
                    FieldMatrix[x, y].Update();
            foreach (var obj in FreeMapObjects)
            {
                obj.Update();
            }
        }

        private TwilightGameMap3()
        { }

        internal void ProcessGameMapObjects(TwilightGameStructEngine3 e)
        {
            foreach (var mo in FreeMapObjects)
                if ((mo as TwilightEnergySource) != null)
                {
                    var tes = mo as TwilightEnergySource;
                    Point fp = tes.FieldPos;
                    tes.EnergySource = new EnergySource(tes);
                    var f = FieldMatrix[fp.X, fp.Y];
                    f.Settler = tes.EnergySource;
                    //tes.EnergySource.EnergySourceBuilding.Light.LightColor = f.GetAvColor().ToVector3();
                }
            foreach (var mo in FreeMapObjects)
            {
                mo.IsRegistered = false;
                e.RegisterObject(mo);
            }
        }
    }
}
