﻿using DWOaO.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace DWOaO
{
    public delegate void MouseActorHandler(object oldObj, Actor newObj);
    public delegate void MouseFieldHandler(object oldObj, TwilightField newObj);
    public delegate void MouseEnergySourceHandler(object oldObj, EnergySource newObj);


    public interface IGameInteractive
    {
        void DisplayBasicProperties(bool isForFriend);
        void DisplayAdvancedProperties(bool isForFriend);
        void ShowPossibleActions(Actor forWhom);
    }

    [Serializable]
    public abstract partial class TWGE3 : TwilightGameStructEngine3
    {
        public GamePanel GUI;

        internal Omnit Omnit1;
        internal Omnit Omnit2;

        public Omnit ActiveOmnit;

        internal Actor ActiveActor;

        internal Skill ActiveSkill;
        internal void ActivateSkill(Skill skill) { ActiveSkill = skill; }

        internal void NewTurn()
        {
            if (ActiveOmnit == null) { ActiveOmnit = Omnit1; ActiveActor = ActiveOmnit; }
            else
            {
                //ActiveOmnit.GEOmniPhaseEnd();
                ActiveOmnit = ActiveOmnit == Omnit1 ? Omnit2 : Omnit1;
                ActiveActor = ActiveOmnit;
            }
            ActiveOmnit.OmniPhaseStart();
        }

        internal object EnteredObject;

        public string MapName { get; protected set; }

        public TWGE3()
            : base(new Vector2())
        {
            MapName = @"Content\TwilightMaps\" + this.GetType().Name + ".twilight";
        }

        public override void Construct()
        {
            base.Construct();

            GameMap = TwilightGameMap3.LoadGameMap(MapName);
            GameMap.ProcessGameMapObjects(this);
            GameMap.ConstrainCameraForThisMap(Camera);
            foreach (var l in LightSources)
                l.NeedReRenderLightMap = true;
            GUI = new GamePanel();

            TwilightObject bl = new TwilightObject(new AnimTexture("ShaderMaps\\solid"), TWE3.twi.Camera.Size.Value * 1.5f, Color.Black);
            bl.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1, 0, 2000);
            //bl.RestoreLocationOrigin = true;
            bl.Position = TWE3.twi.Camera.Position;
            bl.Level = TwilightLayer.WidgetLayer - 10f;
            TWE3.twi.RegisterObject(bl);
            //TWGE3.Gtwi.GUI.HidePanel();
            //ScriptControl.Wait(4000);
        }

        protected override void _RegisterEventHandlers()
        {
            base._RegisterEventHandlers();
            this.MouseDown += TwilightGameEngine3_MouseDown;
            this.MouseMove += TwilightGameEngine3_MouseMove;
            this.MouseUp += TwilightGameEngine3_MouseUp;
            this.KeyDown += TwilightGameEngine3_KeyDown;
            this.KeyUp += TwilightGameEngine3_KeyUp;
            this.DragBegin += TwilightGameEngine3_DragBegin;
            this.DragMove += TwilightGameEngine3_DragMove;
            this.DragEnd += TwilightGameEngine3_DragEnd;
        }
        protected override void _UnregisterEventHandlers()
        {
            base._UnregisterEventHandlers();
            this.MouseDown -= TwilightGameEngine3_MouseDown;
            this.MouseMove -= TwilightGameEngine3_MouseMove;
            this.MouseUp -= TwilightGameEngine3_MouseUp;
            this.KeyDown -= TwilightGameEngine3_KeyDown;
            this.KeyUp -= TwilightGameEngine3_KeyUp;
            this.DragBegin -= TwilightGameEngine3_DragBegin;
            this.DragMove -= TwilightGameEngine3_DragMove;
            this.DragEnd -= TwilightGameEngine3_DragEnd;
        }

        public event MouseActorHandler MouseEnterActor;
        public event MouseFieldHandler MouseEnterField;
        public event MouseEnergySourceHandler MouseEnterSource;

        public TwilightField QueryField(Vector2 pos)
        {
            Point p = Misc.UF.AbsToField(pos);
            try
            {
                return GameMap.FieldMatrix[p.X, p.Y];
            }
            catch (IndexOutOfRangeException)
            {
                return GameMap.FieldMatrix[0, 0];
            }
        }

        public DistortedGameComponent[] QueryAnyGameElemsAround(TwilightField f, int range, Type what)
        {
            List<DistortedGameComponent> Q = new List<DistortedGameComponent>();
            int X = f.FieldPos.X, Y = f.FieldPos.Y;
            for (int x = X - range; x < X + range + 1; x++)
            {
                for (int y = Y - range; y < Y + range + 1; y++)
                {
                    var ff = GameMap.FieldMatrix[x, y];
                    if (UF.IsInRange(ff, GameMap.FieldMatrix[X, Y], range))
                        if (ff.Settler != null && ff.Settler.GetType() == what)
                            Q.Add(ff.Settler);
                }
            }
            return Q.ToArray();
        }

        void TwilightGameEngine3_DragEnd(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
        }

        void TwilightGameEngine3_DragMove(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
        }

        void TwilightGameEngine3_DragBegin(MouseButton mb, Vector2 pos)
        {
        }

        void TwilightGameEngine3_KeyUp(Microsoft.Xna.Framework.Input.Keys key)
        {
        }

        void TwilightGameEngine3_KeyDown(Microsoft.Xna.Framework.Input.Keys key)
        {
            if (GameControlDenied) return;
            switch (key)
            {
                case Keys.Space:
                    if (ActiveSkill != null) { ActiveSkill.Deactivated(); }
                    else if (ActiveActor == ActiveOmnit)
                    { ActiveOmnit.OmniPhaseEnd(); NewTurn(); }
                    else
                    { ActiveActor.IncoPhaseEnd(); }
                    break;
            }
        }

        void TwilightGameEngine3_MouseUp(MouseButton mb, Vector2 pos)
        {
        }

        void TwilightGameEngine3_MouseMove(MouseButton mb, Vector2 pos)
        {
            #region MouseEnterObject Handle
            TwilightField f = QueryField(pos);
            if (f.Settler != null) // Обработка актора
            {
                object old = EnteredObject;
                DistortedGameComponent a = f.Settler;
                if (a as Actor != null)
                {
                    if (old != a)
                    {
                        EnteredObject = a;
                        if (MouseEnterActor != null)
                            MouseEnterActor(old, a as Actor);
                    }
                }
                else if (a as EnergySource != null)
                {
                    if (old != a) { EnteredObject = a; if (MouseEnterSource != null) MouseEnterSource(old, a as EnergySource); }
                }
            }
            else // Обработка самого поля
            {
                object old = EnteredObject;
                if (old != f) { EnteredObject = f; if (MouseEnterField != null) MouseEnterField(old, f); }
            }
            #endregion
        }

        void TwilightGameEngine3_MouseDown(MouseButton mb, Vector2 pos)
        {
            if (GameControlDenied) return;
        }
    }
}