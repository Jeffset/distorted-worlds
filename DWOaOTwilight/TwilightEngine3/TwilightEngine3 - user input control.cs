﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework.Input;

namespace DWOaO
{
    public enum Event
    {
        MouseDown, MouseUp, MouseMove,
        KeyDown, KeyUp,
        DragBegin, DragMove, DragEnd
    }
    public enum MouseButton { LMB, RMB, none }
    public delegate void MouseEventHandler(MouseButton mb, Vector2 pos);
    public delegate void MouseDragEventHandler(MouseButton mb, Vector2 beginPos, Vector2 endPos);
    public delegate void KeyEventHandler(Keys key);

    public abstract partial class TWE3
    {
        // Базовые события для использования производными движками.
        public event MouseEventHandler MouseMove; // если КМ не нажаты
        public event MouseEventHandler MouseDown; // нажатие КМ
        public event MouseEventHandler MouseUp; // релиз КМ
        public event KeyEventHandler KeyDown; // нажатие клавиши
        public event KeyEventHandler KeyUp; // релиз клавиши
        public event MouseEventHandler DragBegin; // если КМ нажата и мышь сдвинулась на опр. расстояние (DragThreshold).
        public event MouseDragEventHandler DragMove; // если КМ нажата и мышь 
        public event MouseDragEventHandler DragEnd; // если КМ была нажата и двигалась и теперь отпущена

        private bool _wasLMBpressed = false;
        private bool _wasRMBpressed = false;
        private Vector2 _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
        private Keys[] _pressedKeys = new Keys[0];

        private float dragDistanceL = 0; private Vector2 lClick; private bool _wasDragL = false;
        private float dragDistanceR = 0; private Vector2 rClick; private bool _wasDragR = false;


        private static float dragThreshold = 30;

        // Система обработки ввода: функция, отвечающая за реализацию модели событий действий пользователя.
        private void ProcessUserInput()
        {
            // получение и первичная обработка данных о состоянии устройств ввода.
            Global.Mouse = Microsoft.Xna.Framework.Input.Mouse.GetState();
            Global.MousePos = new Vector2(Global.Mouse.X, Global.Mouse.Y);
            Global.MousePos += TWE3.twi.Camera.Position.Value;
            Global.Keyboard = Microsoft.Xna.Framework.Input.Keyboard.GetState();

            bool leftDown = false, rightDown = false, leftUp = false, rightUp = false, move = false;

            if (!_wasLMBpressed && Global.Mouse.LeftButton == ButtonState.Pressed) // если ЛКМ была отпущена, а теперь нажата
            {
                leftDown = true; dragDistanceL = 0.0f; lClick = Global.MousePos;
                if (MouseDown != null) MouseDown(MouseButton.LMB, Global.MousePos);// событие НАЖАТИЕ, отдельно для левой кнопки
            }
            if (!_wasRMBpressed && Global.Mouse.RightButton == ButtonState.Pressed) // если ПКМ была отпущена, а теперь нажата
            {
                rightDown = true; dragDistanceR = 0.0f; rClick = Global.MousePos;
                if (MouseDown != null) MouseDown(MouseButton.RMB, Global.MousePos);// событие НАЖАТИЕ, отдельно для правой кнопки
            }

            if (_wasLMBpressed && Global.Mouse.LeftButton == ButtonState.Released) // если ЛКМ была нажата, а теперь отпущена
            {
                leftUp = true;
                if (MouseUp != null) MouseUp(MouseButton.LMB, Global.MousePos);// событие РЕЛИЗ, отдельно для левой кнопки
            }
            if (_wasRMBpressed && Global.Mouse.RightButton == ButtonState.Released) // если ПКМ была нажата, а теперь отпущена
            {
                rightUp = true;
                if (MouseUp != null) MouseUp(MouseButton.RMB, Global.MousePos);// событие РЕЛИЗ, отдельно для правой кнопки
            }

            // мышь сдвинулась
            if (Global.MousePos != _lastMousePos)
            { move = true; if (MouseMove != null) MouseMove(MouseButton.none, Global.MousePos); }

            // Перетягивание началось
            if (!_wasDragL && Global.Mouse.LeftButton == ButtonState.Pressed && dragDistanceL > dragThreshold)
            { _wasDragL = true; if (DragBegin != null) DragBegin(MouseButton.LMB, lClick); }
            if (!_wasDragR && Global.Mouse.RightButton == ButtonState.Pressed && dragDistanceR > dragThreshold)
            { _wasDragR = true; if (DragBegin != null) DragBegin(MouseButton.RMB, rClick); }

            // Перетягивание в процессе
            if (move && Global.Mouse.LeftButton == ButtonState.Pressed)
            {
                dragDistanceL = (Global.MousePos - lClick).Length();
                if (_wasDragL && DragMove != null) DragMove(MouseButton.LMB, lClick, Global.MousePos);
            }
            if (move && Global.Mouse.RightButton == ButtonState.Pressed)
            {
                dragDistanceR = (Global.MousePos - rClick).Length();
                if (_wasDragR && DragMove != null) DragMove(MouseButton.RMB, rClick, Global.MousePos);
            }

            // Перетягивание закончено
            if (_wasDragL && leftUp)
            { _wasDragL = false; if (DragEnd != null) DragEnd(MouseButton.LMB, lClick, Global.MousePos); }// DragEnd;
            if (_wasDragR && rightUp)
            { _wasDragR = false; if (DragEnd != null) DragEnd(MouseButton.RMB, rClick, Global.MousePos); }// DragEnd;

            // Обработка клавиатурных нажатий
            for (int c = 0; c < _pressedKeys.Length; c++)
                if (Global.Keyboard.IsKeyUp(_pressedKeys[c]) && KeyUp != null)
                {
                    KeyUp(_pressedKeys[c]);
                }
            Keys[] keys = Global.Keyboard.GetPressedKeys();
            foreach (var key in keys)
                if (!_pressedKeys.Contains(key) && KeyDown != null) KeyDown(key);

            // Сохранения текущих значений как старые
            _pressedKeys = keys;
            _lastMousePos = Global.MousePos;
            _wasLMBpressed = Global.Mouse.LeftButton == ButtonState.Pressed;
            _wasRMBpressed = Global.Mouse.RightButton == ButtonState.Pressed;
            _gameControlTemporaryDenied = false;
        }
    }
}
