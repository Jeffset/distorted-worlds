﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace DWOaO
{
    [Serializable]
    public abstract partial class TWE3
    {
        public static TWE3 twi;
        public static TWGE3 Gtwi { get { return twi as TWGE3; } }

        public List<LightSource> LightSources { get; private set; }
        protected List<TwilightObject> FreeObjects { get; private set; }

        public TwilightCamera Camera { get; private set; }
        public Vector2 EngineSize { get; set; }
        public Vector2 ScreenSize
        {
            get
            {
                return new Vector2(XNAPlatform.GraphicsManager.GraphicsDevice.DisplayMode.Width,
                    XNAPlatform.GraphicsManager.GraphicsDevice.DisplayMode.Height);
            }
        }

        public virtual void Render()
        {
            //XNAPlatform.GraphicsManager.GraphicsDevice.Clear(Microsoft.Xna.Framework.Graphics.ClearOptions.Target, Color.Red, 0, 0);
            Rectangle cam = Camera.Bounds;
            foreach (var o in FreeObjects)
            {
                if (o.Bounds.Intersects(cam))
                    o.Render();
            }
            foreach (var w in GraphWidgets)
                w.Render();
        }
        public virtual void Update(GameTime gameTime)
        {
            Global.GameTime = gameTime;
            Camera.MakeNeedUpdate();
            List<TwilightObject> layer = FreeObjects;
            int count = layer.Count;
            foreach (var o in LightSources)
                o.Update();
            foreach (var o in GraphWidgets)
                o.MakeNeedUpdate();
            foreach (var o in FreeObjects)
                o.MakeNeedUpdate();
            foreach (var o in LightSources)
                o.MakeNeedUpdate();
            Camera.Update();
            ProcessUserInput();
            foreach (var o in GraphWidgets)
                o.Update();
            //foreach (var o in FreeObjects)
            //    o.Update();
            for (int i = 0; i < FreeObjects.Count; i++)
                FreeObjects[i].Update();
            FreeObjects.Sort((o1, o2) =>
                {
                    float a = o1.Level - o2.Level;
                    if (a == 0.0f)
                    { return o1.Bottom - o2.Bottom; }
                    else return (int)(a * 1000.0f);
                });
        }

        // Функция для инициализации движка при создании его заново.
        // Использ. при создании главного меню.
        // Использ. при создании карты в редакторе карт.
        public virtual void Construct()
        {
            SetAsActiveTwilight();
            LightSources = new List<LightSource>();
            //
            FreeObjects = new List<TwilightObject>();
            GraphWidgets = new LinkedList<TwilightObject>();
            Camera = new TwilightCamera();
            Camera.Position.Cnstr<Anim.V2RegionCnstr>().Cfg(new Vector2(), EngineSize, Camera.Size);
            ConfigCameraCtrl();
        }

        // Функция инициализации движка, при загрузке его из файла сохранения.
        // Использ. для загрузки ранее сохраннной игры.
        public static TWE3 Construct(string fileNameToLoadFrom)
        {
            FileStream fs = File.OpenRead(fileNameToLoadFrom);
            BinaryFormatter bf = new BinaryFormatter();
            TWE3 eng = bf.Deserialize(fs) as TWE3;
            fs.Close();
            foreach (var l in eng.LightSources)
                l.NeedReRenderLightMap = true;
            return eng;
        }

        // Единственный конструктор, принимающий размер движка в пикселях.
        public TWE3(Vector2 engineAbsSize)
        {
            EngineSize = engineAbsSize;
        }

        public void ConfigCameraCtrl()
        {
            Camera.Position.Ctrl<Anim.V2InputCtrl>().CfgKeyboard(2000f);
            Camera.Position.Ctrl<Anim.V2InputCtrl>().CfgMouseEdge(2500f);
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext context)
        {
            SetAsActiveTwilight();
        }

        public void SetAsActiveTwilight()
        {
            if (twi != null) twi._UnregisterEventHandlers();
            TWE3.twi = this;
            _RegisterEventHandlers();
        }

        public void RegisterObject(TwilightObject obj)
        {
            if (obj.IsRegistered) return;
            List<TwilightObject> layer = FreeObjects;
            int i = 0;
            for (; i < layer.Count; i++)
                if (layer[i].Level > obj.Level) break;
            layer.Insert(i, obj);
            //LinkedListNode<TwilightObject> node = layer.First;
            //while (node != null && node.Value.Level <= obj.Level)
            //    node = node.Next;
            //if (node == null)
            //{
            //    layer.AddLast(obj);
            //    
            //}
            //else
            //{
            //    layer.AddBefore(node, obj);
            //    obj.IsRegistered = true;
            //}
            obj.IsRegistered = true;
            obj.OnObjectRegistered();
        }
        public void UnregisterObject(TwilightObject obj)
        {
            FreeObjects.Remove(obj);
            obj.IsRegistered = false;
            obj.OnObjectUnregistered();
        }
        public void RegisterLightSource(LightSource light)
        {
            if (LightSources.Contains(light)) return;
            LightSources.Add(light);
        }
        public void UnregisterLightSource(LightSource light)
        {
            if (!LightSources.Contains(light)) return;
            LightSources.Remove(light);
        }
        public void ChangeLevel(TwilightObject obj)
        {
            if (!obj.IsRegistered) return;
            List<TwilightObject> layer = FreeObjects;
            layer.Remove(obj);
            int i = 0;
            for (; i < layer.Count; i++)
                if (layer[i].Level > obj.Level) break;
            layer.Insert(i, obj);
        }

        public virtual void SaveEngine(string fileNameToSave)
        {
            using (FileStream fs = File.OpenWrite(fileNameToSave))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, this);
            }
        }

        public IEnumerable<TwilightObject> QueryFreeObject(Type type, Point pos)
        {
            return FreeObjects.Where(o => { return o.Bounds.Contains(pos) && o.GetType() == type; });
        }
        public IEnumerable<TwilightObject> QueryFreeObject(Point pos)
        {
            return FreeObjects.Where(o => { return o.Bounds.Contains(pos); });
        }
        public IEnumerable<TwilightObject> QueryFreeObject(Type type, Vector2 pos)
        {
            return QueryFreeObject(type, new Point((int)pos.X, (int)pos.Y));
        }
        public IEnumerable<TwilightObject> QueryFreeObject(Vector2 pos)
        {
            return QueryFreeObject(new Point((int)pos.X, (int)pos.Y));
        }
    }
}
