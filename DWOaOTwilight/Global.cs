﻿using DWOaO.ActorModels;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    public static class Global
    {
        public static Random rnd = new Random(Environment.TickCount % 1024);

        static Global()
        {
            GameTime = new GameTime();
        }

        private static int FLD_base = 120;
        public static readonly float PerspectiveFactor = 0.8f;
        public static int Cell_H { get { return FLD_base; } }
        public static Vector2 CellSize
        { get { return new Vector2(Cell_H, Cell_V); } }
        public static int Cell_V { get { return (int)(FLD_base * PerspectiveFactor); } }
        public static KeyboardState Keyboard;
        public static MouseState Mouse;
        public static Vector2 MousePos;
        public static GameTime GameTime;
    }

    public static class TwiSound
    {
        private const int _idleSpeachCount = 3;
        public static void IdleSpeak(string ActorName)
        {
            XNAPlatform.ContentManager.Load<SoundEffect>(@"Sound\" + ActorName + @"\SpeachSys\idle" + Global.rnd.Next(1, _idleSpeachCount + 1).ToString()).Play();
        }

        public static void SysSpeak(string ActorName, string sound)
        { XNAPlatform.ContentManager.Load<SoundEffect>(@"Sound\" + ActorName + @"\SpeachSys\" + sound).Play(); }

        public static void PlaySnd(string sound)
        { XNAPlatform.ContentManager.Load<SoundEffect>(@"Sound\" + sound).Play(); }

        private static SoundEffect _step;
        public static void PlayStep(ActorModel mdl)
        {
            string sndName = @"Sound\go_" + (mdl.IsEtherial ? "Etherial" : mdl.GetField().Attribute.SurfaceType.ToString());
            if (_step != null && _step.Name == sndName)
                _step.Play();
            else
            { _step = XNAPlatform.ContentManager.Load<SoundEffect>(sndName); _step.Play(); }
        }
    }
}
