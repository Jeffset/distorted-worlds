﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DWOaO
{
    // реализация всенаправленного источника света.
    // регистрация источника света должна производиться объектом-плафоном
    [Serializable]
    public class LightSource : Animatable
    {
        [OnDeserialized]
        void OnDeserialized(StreamingContext c)
        {
            NeedReRenderLightMap = true;
        }

        public AnimFloat Altitude { get; private set; }
        public AnimFloat2 Position { get; private set; }
        public AnimFloat3 LightColor { get; set; }
        public AnimFloat LightIntensity { get; set; }
        public AnimFloat LightDistance { get; private set; }
        public Rectangle LightArea
        {
            get
            {
                Rectangle r = new Rectangle(
                    (int)(Position.X - LightDistance.Value),
                    (int)(Position.Y - LightDistance.Value),
                    (int)(LightDistance.Value * 2),
                    (int)(LightDistance.Value * 2));
                return r;
            }
        }

        public LightSource(float altitude = 60.0f, float lightIntensity = 1.0f, float lightDistance = 3.0f * 120.0f)
        {
            Altitude = new AnimFloat(altitude);
            Position = new AnimFloat2();
            LightIntensity = new AnimFloat(lightIntensity);
            LightColor = new AnimFloat3(Color.White.ToVector3());
            LightDistance = new AnimFloat(lightDistance); // пять ячеек]
            animParams.AddRange(new AnimParam[] { LightIntensity, LightColor, LightDistance, Altitude, Position });
            NeedReRenderLightMap = true;
        }

        public bool NeedReRenderLightMap { get; set; }
        public override void Update()
        {
            float a = Altitude.Value, li = LightIntensity.Value, ld = LightDistance.Value;
            Vector2 p = Position.Value; Vector3 c = LightColor.Value;
            base.Update();
            bool test = (p != Position.Value || c != LightColor.Value || ld != LightDistance.Value || li != LightIntensity.Value || a != Altitude.Value);
            NeedReRenderLightMap = (p != Position.Value || c != LightColor.Value || ld != LightDistance.Value || li != LightIntensity.Value || a != Altitude.Value);
        }
    }


    [Serializable]
    public abstract class Lightable : TwilightObject
    {
        //[NonSerialized]
        //public Texture2D LightMap;
        public List<LightSource> Lights = new List<LightSource>();

        [NonSerialized]
        protected Effect LightShader;
        [NonSerialized]
        protected EffectParameter lightDir;
        [NonSerialized]
        protected EffectParameter normalMap;
        [NonSerialized]
        protected EffectParameter lightColor;
        [NonSerialized]
        protected EffectParameter lightDistance;
        [NonSerialized]
        protected EffectParameter matrixTransform;

        protected void _InitShader()
        {
            LightShader = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\Light\Lightable");
            lightDir = LightShader.Parameters["lightDir"];
            lightDistance = LightShader.Parameters["lightDistance"];
            lightColor = LightShader.Parameters["lightColor"];
            normalMap = LightShader.Parameters["normalMap"];
            matrixTransform = LightShader.Parameters["MatrixTransform"];
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Lights = new List<LightSource>();
            _InitShader();
        }

        protected Lightable()
        {
            _InitShader();
        }

        public override void Update()
        {
            base.Update();

            Lights.Clear();
            foreach (var light in TWE3.twi.LightSources)
            {
                if (light.LightArea.Intersects(this.Bounds))
                {
                    Lights.Add(light);
                }
            }
        }
        public override void Render()
        {
            if (Lights.Count == 0)
                base.Render();
            else
            {
                LightShader.CurrentTechnique = LightShader.Techniques
                    ["L" + (Lights.Count > GRAPHIC_SETTINGS.MAX_LIGHT_SOURCES_PER_LIGHTABLE ? GRAPHIC_SETTINGS.MAX_LIGHT_SOURCES_PER_LIGHTABLE : Lights.Count).ToString()];
                //LightShader.Parameters["objPos"].SetValue
                Vector3 objPos = new Vector3(Position.Value, 0.0f) / Size.X;
                normalMap.SetValue(Texture.NormalMap);
                matrixTransform.SetValue(
                        Matrix.CreateTranslation(-0.5f, -0.5f, 0) *
                         Matrix.CreateOrthographicOffCenter
                        (0, XNAPlatform.GraphicsManager.GraphicsDevice.Viewport.Width, XNAPlatform.GraphicsManager.GraphicsDevice.Viewport.Height, 0, 0, 1)
                        );
                lightDir.SetValue(Lights.Select(l =>
                   {
                       var v = objPos - new Vector3(l.Position.Value, -l.Altitude.Value) / Size.X;
                       v.Y /= 0.8f;
                       return v;
                   }).ToArray());
                lightColor.SetValue(Lights.Select(l => new Vector4(l.LightColor.Value, l.LightIntensity.Value)).ToArray());
                lightDistance.SetValue(Lights.Select(l => l.LightDistance.Value / Size.X).ToArray());
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.LinearClamp, null, null, LightShader);
                //XNAPlatform.Sprite.Draw(Texture.Texture, _GetDestRect(), null, Color.White);
                RenderAsSubobject(new Vector4(1));
                //XNAPlatform.Sprite.Draw(Texture.Texture, _GetDestRect(), Color.White);
                XNAPlatform.Sprite.End();
            }
        }
    }
}