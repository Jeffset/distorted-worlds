﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;

namespace DWOaO.ActorModels
{
    public abstract class ActorModel : TwilightObject
    {
        public class ActorMotionBlur
        {
            private ActorModel mdl;
            public int Length { get; set; }
            public int Delay { get; set; }

            List<TwilightObject> traces;
            Queue<RenderTarget2D> textureQueue;

            private bool _enabled;
            public bool Enabled
            {
                get { return _enabled; }
                set
                {
                    if (!_enabled && value)
                    {
                        textureQueue.Clear();
                        for (int i = 0; i < (Length + 1); i++)
                        {
                            textureQueue.Enqueue(new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, (int)mdl.Width, (int)mdl.Height, false, SurfaceFormat.Color, DepthFormat.None));
                        }
                    }
                    _enabled = value;
                }
            }

            public ActorMotionBlur(ActorModel model, int length, int delay)
            {
                Length = length; Delay = delay; mdl = model;
                traces = new List<TwilightObject>();
                textureQueue = new Queue<RenderTarget2D>();
            }

            public void Process()
            {
                if (_enabled)
                {
                    //foreach (var bone in this.Bones) bone.Opacity.Value = 0f;
                    //Bones[10].Opacity.Value = 1f;
                    var rt = textureQueue.Dequeue();
                    var trace = mdl.SnapshotActor(rt); textureQueue.Enqueue(rt);
                    //foreach (var bone in this.Bones) bone.Opacity.Value = 1f;
                    TWE3.twi.RegisterObject(trace);
                    trace.Texture.Shader.BlendingOptions = BlendState.AlphaBlend; trace.Y -= 2;
                    trace.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.4f, 0, 8 * Length * 0.8f);
                    //trace.RGBMulti.Value = Actor.SideColor.ToVector3();
                    //trace.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Linear, Color.White.ToVector3(), Actor.SideColor.ToVector3(), MBDelay * MBLength * 0.8f);
                    //trace.Size.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, new Vector2(0.5f), wait * length);
                    if (traces.Count < Length)
                    {
                        traces.Add(trace);
                    }
                    else
                    {
                        var o = traces[0]; TWE3.twi.UnregisterObject(o); traces.RemoveAt(0); traces.Add(trace);
                    } 
                }
                else if(traces.Count!= 0)
                {
                    var o = traces[0]; TWE3.twi.UnregisterObject(o); traces.RemoveAt(0);
                } 

            }
        }

        public bool IsEtherial { get; set; }

        public ActorMotionBlur Mblur { get; private set; }

        public TwilightObject Shadow;
        public Actor Actor { get; private set; }

        public LightSource selfGlowing;

        public Script CurrentModelScript { get; set; }

        private Orient _orient;
        public Orient Orientation
        {
            get { return _orient; }
            set
            {
                _orient = value;
                Bones = orientsModels[value];
                foreach (var bone in Bones)
                {
                    if (bone != null) bone.Position.Update();
                }
            }
        }
        protected Dictionary<Orient, ActorBone[]> orientsModels = new Dictionary<Orient, ActorBone[]>(4);
        public ActorBone[] Bones;

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < Bones.Length; i++)
            {
                Bones[i].Update();
            }
            //selfGlowing.Update();
            Mblur.Process();
        }

        internal override void MakeNeedUpdate()
        {
            base.MakeNeedUpdate();
            for (int i = 0; i < Bones.Length; i++)
            {
                Bones[i].MakeNeedUpdate();
            }
        }

        public TwilightField GetField()
        {
            Vector2 legPoint = new Vector2(Center.X, Bottom - 2);
            Point p = Misc.UF.AbsToField(legPoint);
            return TWGE3.Gtwi.GameMap.FieldMatrix[p.X, p.Y];
        }

        public void ResetModelState()
        {
            for (int i = 0; i < Bones.Length; i++)
                Bones[i].RC().Cfg(0);
        }
        public ActorBone.FloatHierarchyAngleCtrl ResetState(float time, Misc.Anim type = Misc.Anim.Linear)
        {
            for (int i = 0; i < Bones.Length - 1; i++)
                Bones[i].RC().Cfg(0.0f, time, isAdditive: false, at: type);
            return Bones[Bones.Length - 1].RC().Cfg(0.0f, time, isAdditive: false, at: type);
        }
        public ActorBone.FloatHierarchyAngleCtrl Reverse()
        {
            ActorBone.FloatHierarchyAngleCtrl c = null;
            for (int i = 0; i < Bones.Length - 1; i++)
            {
                c = Bones[i].RC();
                c.Cfg(c.From, c.Time, isAdditive: false);
            }
            return c;
        }

        public void PlaceOnField(int x, int y)
        {
            Position.Value = new Vector2(x * Global.Cell_H, (y + 1) * Global.Cell_V - Height - 40);
            TWE3.twi.RegisterObject(this);
            TWE3.Gtwi.GameMap.FieldMatrix[x, y].Settler = Actor;
        }

        public ActorModel(Actor act, int w, int h)
        {
            Level = Misc.TwilightLayer.ActorLayer;
            Size.Value = new Vector2(w, h);
            Actor = act;
            Shadow = new TwilightObject(new AnimTexture("rg"), new Vector2(w, h * 0.3f));
            Shadow.Opacity.Value = 0.5f;
            Shadow.Position.Value = new Vector2(0, h * 0.79f);
            Shadow.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(Position);
            Shadow.Level = Level - 0.01f;
            Shadow.Texture.Shader.BlendingOptions = BlendState.AlphaBlend;
            Shadow.RGBMulti.Value = Actor.SideColor.ToVector3();
            selfGlowing = new LightSource();
            selfGlowing.LightDistance.Value = Global.Cell_H * 2f;
            selfGlowing.Position.Value = new Vector2(Width / 2, Height);
            selfGlowing.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(Position);
            selfGlowing.LightColor.Value = Actor.SideColor.ToVector3();
            selfGlowing.Altitude.Value = 120f;
            Mblur = new ActorMotionBlur(this, 20, 10);
        }

        public override void OnObjectRegistered()
        {
            base.OnObjectRegistered();
            TWE3.twi.RegisterLightSource(selfGlowing);
            TWE3.twi.RegisterObject(Shadow);

        }
        public override void OnObjectUnregistered()
        {
            base.OnObjectUnregistered();
            TWE3.twi.UnregisterLightSource(selfGlowing);
            TWE3.twi.UnregisterObject(Shadow);

        }

        public virtual Script<Action> RunIdleBreathScript()
        {
            return null;
        }

        public TwilightObject SnapshotActor(RenderTarget2D dest)
        {
            //int W = (int)(Size.X + addSizeX);
            //int H = (int)(Size.Y + addSizeY);
            //RenderTarget2D rt = new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, W, H, false, SurfaceFormat.Color, DepthFormat.None);
            var r = new AnimTexture(dest);
            var snapshot = new TwilightObject(r, null);
            var savePos = Position.Value;
            var saveCam = TWE3.twi.Camera.Position.Value;
            TWE3.twi.Camera.Position.Value = Vector2.Zero;
            Misc.UF.PutOn(this, snapshot);
            foreach (var bone in Bones) bone.Position.Value = bone.Position.Value - savePos + Position.Value;
            XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(dest);
            XNAPlatform.GraphicsManager.GraphicsDevice.Clear(Color.Transparent);
            Render();
            //XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(null);
            foreach (var bone in Bones) bone.Position.Value = bone.Position.Value + savePos - Position.Value;
            Position.Value = savePos;
            TWE3.twi.Camera.Position.Value = saveCam;
            snapshot.Level = Level;
            Misc.UF.PutOn(snapshot, this);
            return snapshot;

        }
    }
}
