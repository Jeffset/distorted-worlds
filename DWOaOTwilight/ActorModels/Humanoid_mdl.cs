﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DWOaO.ActorModels
{
    public class Humanoid_mdl : ActorModel
    {
        public ActorBone Body { get { return Bones[0]; } }
        public ActorBone Head { get { return Bones[1]; } }
        public ActorBone LeftArm { get { return Bones[2]; } }
        public ActorBone RightArm { get { return Bones[3]; } }
        public ActorBone LeftHand { get { return Bones[4]; } }
        public ActorBone RightHand { get { return Bones[5]; } }
        public ActorBone LeftLeg { get { return Bones[6]; } }
        public ActorBone RightLeg { get { return Bones[7]; } }
        public ActorBone LeftFoot { get { return Bones[8]; } }
        public ActorBone RightFoot { get { return Bones[9]; } }
        public ActorBone Weapon { get { return Bones[10]; } }

        private void _RenderOrient()
        {
            #region RenderOrient
            switch (Orientation)
            {
                case Orient.ToUs:
                    {
                        RightFoot.Render();
                        LeftFoot.Render();
                        RightLeg.Render();
                        LeftLeg.Render();
                        Body.Render();
                        RightArm.Render();
                        RightHand.Render();
                        LeftArm.Render();
                        Head.Render();
                        Weapon.Render();
                        LeftHand.Render();
                    }
                    break;
                case Orient.ToRight:
                    {
                        RightArm.Render();
                        RightHand.Render();
                        RightFoot.Render();
                        RightLeg.Render();
                        LeftFoot.Render();
                        LeftLeg.Render();
                        Body.Render();
                        LeftArm.Render();
                        Head.Render();
                        Weapon.Render();
                        LeftHand.Render();
                    }
                    break;
                case Orient.ToForward:
                    {
                        RightHand.Render();
                        Weapon.Render();
                        LeftHand.Render();
                        LeftArm.Render();
                        RightArm.Render();
                        LeftFoot.Render();
                        RightFoot.Render();
                        LeftLeg.Render();
                        RightLeg.Render();
                        Body.Render();
                        Head.Render();
                    }
                    break;
                case Orient.ToLeft:
                    {
                        LeftArm.Render();
                        LeftFoot.Render();
                        LeftLeg.Render();
                        RightFoot.Render();
                        RightLeg.Render();
                        Body.Render();
                        Head.Render();
                        RightArm.Render();
                        RightHand.Render();
                        Weapon.Render();
                        LeftHand.Render();
                    }
                    break;
            }
            #endregion
        }
        private MapSystem.FieldSurfaceType _lastType;
        private TwiParticleSystem _Sprays;
        AnimFloat2 spraysPos;
        AnimFloat2 spraysRad;
        public override void Render()// Используется для отрисовки пост-эффектов поверхности
        {
            _RenderOrient();
        }

        private const float _timeSplash = 100; 
        public override void Update()
        {
            if (IsEtherial)
            { base.Update(); return; }
            TwilightField f = GetField();
            switch (f.Attribute.SurfaceType)
            {
                case DWOaO.MapSystem.FieldSurfaceType.Liquid:

                    if (_lastType != MapSystem.FieldSurfaceType.Liquid)
                    {
                        var tmp = Orientation;

                        for (int i = 0; i < 4; i++)
                        {
                            Orientation = (Orient)i;
                            LeftFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, _timeSplash);
                            RightFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, _timeSplash);
                            foreach (var bone in Bones)
                            {
                                if ((Orient)i == tmp) bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, +40), _timeSplash);
                                else bone.BasePosition.Value += new Vector2(0, +40);
                            }
                        }
                        Orientation = tmp;

                        Script.Start<Action>(() =>
                        {
                            selfGlowing.Altitude.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 20f, _timeSplash);

                            spraysPos.Value = f.Center;
                            _Sprays.Update();
                            _Sprays.PullInsideAllParticles();
                            //spraysRad.Value = new Vector2(100, 500);
                            ScriptControl.Wait(80);
                            TwiSound.PlaySnd("Splash");
                            TWE3.twi.RegisterObject(_Sprays);
                            _Sprays.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.5f, 200).WaitForFinish(200);
                            _Sprays.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, 300).WaitForFinish(300);
                            TWE3.twi.UnregisterObject(_Sprays);
                        });
                    }
                    break;
                default:

                    if (_lastType == MapSystem.FieldSurfaceType.Liquid)
                    {
                        var tmp = Orientation;

                        for (int i = 0; i < 4; i++)
                        {
                            Orientation = (Orient)i;
                            LeftFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f, _timeSplash);
                            RightFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f, _timeSplash);
                            foreach (var bone in Bones)
                            {
                                if ((Orient)i == tmp) bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -40), _timeSplash);
                                else bone.BasePosition.Value += new Vector2(0, -40);
                            }
                        }
                        Orientation = tmp;
                        Script.Start<Action>(() =>
                        {
                            selfGlowing.Altitude.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 60f, _timeSplash);
                            TWE3.twi.RegisterObject(_Sprays);
                            spraysPos.Value = new Vector2(Center.X, Bottom);
                            _Sprays.Update();
                            _Sprays.PullInsideAllParticles();
                            //spraysRad.Value = new Vector2(100, 500);
                            _Sprays.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.5f, 200).WaitForFinish(200);
                            _Sprays.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, 300).WaitForFinish(300);
                            TWE3.twi.UnregisterObject(_Sprays);
                        });
                    }
                    break;
            }
            _lastType = f.Attribute.SurfaceType;
            base.Update();
        }

        public Humanoid_mdl(ActorModelTextureSet texSet, Actor actor)
            : base(actor, 120, actor as Incarnation != null ? 120 : 160)
        {
            spraysPos = new AnimFloat2(Center.X, Bottom);// spraysPos.Ctrl<Anim.V2SnapToCtrl>().Cfg(Position);
            spraysRad = new AnimFloat2(250, 100);
            _Sprays = new TwiParticleSystem(700, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(spraysPos, spraysRad) },
                opacity: 1.0f, parSize: 8, minTime: 500, maxTime: 800);
            _Sprays.RGBMulti.Value = actor.SideColor.ToVector3();
            for (int i = 0; i < 4; i++)
            {
                Orient Orient = (Orient)i;
                string mdlPath = "mdl\\" + texSet.ActorName + "\\" + Orient.ToString() + "\\";
                orientsModels[Orient] = new ActorBone[11];
                Orientation = Orient;

                Bones[0] = new ActorBone(mdlPath + texSet.Body); Body.SetParentObject(this);
                Bones[1] = new ActorBone(mdlPath + texSet.Head); Head.SetParentObject(Body);
                Bones[2] = new ActorBone(mdlPath + texSet.LeftArm); LeftArm.SetParentObject(Body);
                Bones[3] = new ActorBone(mdlPath + texSet.RightArm); RightArm.SetParentObject(Body);
                Bones[4] = new ActorBone(mdlPath + texSet.LeftHand); LeftHand.SetParentObject(LeftArm);
                Bones[5] = new ActorBone(mdlPath + texSet.RightHand); RightHand.SetParentObject(RightArm);
                Bones[6] = new ActorBone(mdlPath + texSet.LeftLeg); LeftLeg.SetParentObject(Body);
                Bones[7] = new ActorBone(mdlPath + texSet.RightLeg); RightLeg.SetParentObject(Body);
                Bones[8] = new ActorBone(mdlPath + texSet.LeftFoot); LeftFoot.SetParentObject(LeftLeg);
                Bones[9] = new ActorBone(mdlPath + texSet.RightFoot); RightFoot.SetParentObject(RightLeg);
                Bones[10] = new ActorBone(mdlPath + texSet.Weapon);
                Weapon.SetParentObject(Orient == Orient.ToForward ? RightHand : LeftHand);
            }
        }

        public override Script<Action> RunIdleBreathScript()
        {
            var scr = Script.Start<Action>(this._IdleScript);
            //if (CurrentModelScript != null) CurrentModelScript.Terminate();
            this.CurrentModelScript = scr;
            return scr;
        }

        private void _IdleScript()
        {
            Humanoid_mdl A = this;
            try
            {
                //Monitor.Enter(actor);
                while (true)
                {
                    A.ResetState(200, Misc.Anim.Bezier3).WaitForFinish(200);
                    float t = Global.rnd.Next(1000, 2000);
                    switch (A.Orientation)
                    {
                        case Orient.ToUs:
                        case Orient.ToForward:
                            {
                                A.LeftHand[+10, t, Misc.Anim.Bezier3].n();
                                A.LeftArm[-20, t, Misc.Anim.Bezier3].n();
                                A.Body[+10, t, Misc.Anim.Bezier3].n();
                                A.LeftLeg[-10, t, Misc.Anim.Bezier3].n();
                                A.RightLeg[-20, t, Misc.Anim.Bezier3].n();
                                A.RightFoot[+10, t, Misc.Anim.Bezier3].n();
                                A.Head[-10, t].WaitForFinish(t);
                                //
                                A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            }
                            break;
                        case Orient.ToRight:
                        case Orient.ToLeft:
                            {
                                A.LeftHand[+10, t, Misc.Anim.Bezier3].n();
                                A.LeftArm[-20, t, Misc.Anim.Bezier3].n();
                                A.Body[+10, t, Misc.Anim.Bezier3].n();
                                A.LeftLeg[-10, t, Misc.Anim.Bezier3].n();
                                A.RightLeg[-20, t, Misc.Anim.Bezier3].n();
                                A.RightFoot[+10, t, Misc.Anim.Bezier3].n();
                                A.Head[-10, t, Misc.Anim.Bezier3].WaitForFinish(t);
                                //
                                A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            }
                            break;
                    }
                }
            }
            catch (ThreadAbortException e)
            {

            }
            finally
            {
            }
        }
    }
}
