﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    public class TwilightElement : TwilightObject
    {
        public TwilightElement(AnimTexture tex, Vector2 size)
        {
            Layer = 1;
            Texture = tex;
            animParams.Add(Texture);
            Size.Value = size;
        }

        public TwilightElement(AnimTexture tex, Vector2 size, Color rgba)
        {
            Layer = 1;
            Texture = tex;
            animParams.Add(Texture);
            Size.Value = size;
            RGBMulti.Value = rgba.ToVector3();
        }
    }
}
