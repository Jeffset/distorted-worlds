﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DWOaO
{
    public class TwiParticleSystem : TwilightObject
    {
        private class Particle : TwilightObject
        {
            public Particle(AnimTexture t, Vector2 size, Vector2 pos)
                : base(t, size)
            {
                Position.Value = pos;
            }
        }

        private List<Particle> particles;

        public List<Tuple<AnimFloat2, AnimFloat2>> SystemCenters = new List<Tuple<AnimFloat2, AnimFloat2>>();

        public override void Update()
        {
            base.Update();
            for (int i = 0; i < particles.Count; i++)
                particles[i].Update();
            _CalculateRect();
        }

        public override void Render()
        {
            XNAPlatform.Sprite.Begin(Microsoft.Xna.Framework.Graphics.SpriteSortMode.Deferred, BlendState.Additive);
            for (int i = 0; i < particles.Count; i++)
                particles[i].RenderAsSubobject(new Vector4(RGBMulti.Value, Opacity.Value));
            XNAPlatform.Sprite.End();
        }

        private void _CalculateRect()
        {
            Rectangle[] rects = SystemCenters.Select
               (o => new Rectangle((int)(o.Item1.X - o.Item2.X / 2), (int)(o.Item1.Y - o.Item2.Y / 2), (int)(o.Item2.X), (int)(o.Item2.Y))).ToArray();
            Rectangle rect = rects[0];
            for (int i = 1; i < rects.Length; i++)
                rect = Rectangle.Union(rect, rects[i]);
            Position.Value = new Vector2(rect.X, rect.Y);
            Size.Value = new Vector2(rect.Width, rect.Height);
        }

        public TwiParticleSystem(int particleCount, Tuple<AnimFloat2, AnimFloat2>[] systemCenters,
            string particleTexName = "rg", float opacity = 1.0f, float parSize = 30, int minTime = 200, int maxTime = 800, Misc.Anim at = Misc.Anim.Bezier3)
        {
            foreach (var center in systemCenters)
            {
                center.Item1.Value -= new Vector2(parSize / 2f);
            }
            RGBMulti.Value = Color.White.ToVector3();
            Level = Misc.TwilightLayer.EffectLayer;
            SystemCenters.AddRange(systemCenters);
            _CalculateRect();
            particles = new List<Particle>(particleCount);
            for (int i = 0; i < particleCount; i++)
            {
                particles.Add(new Particle(new AnimTexture(particleTexName), new Vector2(parSize), systemCenters[0].Item1.Value));
                particles[i].Position.Value = systemCenters[0].Item1.Value;
                particles[i].Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(at, Global.rnd.Next(minTime, maxTime), systemCenters);
                //particles[i].RGBMulti.Value = new Vector3((float)(Global.rnd.NextDouble()), (float)(Global.rnd.NextDouble()), (float)(Global.rnd.NextDouble()));
                //particles[i].Opacity.Value = opacity;
            }
            Opacity.Value = opacity;
        }

        public void PullInsideAllParticles()
        {
            foreach (var p in particles)
            {
                p.Position.Ctrl<Anim.V2RandomdCtrl>().PullInCenter();
            }
        }
    }
}