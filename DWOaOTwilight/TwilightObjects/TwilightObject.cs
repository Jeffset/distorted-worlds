﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace DWOaO
{
    [Serializable]
    public class TwilightObject : Animatable
    {
        public AnimTexture Texture { get; protected set; }
        public AnimFloat2 Position { get;  set; }
        public AnimFloat3 RGBMulti { get; private set; }
        public AnimFloat2 Size { get; private set; }
        public AnimFloat Rotation { get; private set; }
        public AnimFloat2 Origin { get; private set; }
        public AnimFloat Opacity { get; private set; }
        public SpriteEffects Flip { get; set; }

        public bool IsRegistered { get; set; }
        public bool RestoreLocationOrigin { get; set; }
        public AnimFloat2 BasePosition { get; private set; }

        private float _level = 0.0f;
        public float Level
        {
            get { return _level; }
            set { _level = value; TWE3.twi.ChangeLevel(this); }
        }

        //[OnDeserialized]
        //private void OnDeserialized(StreamingContext context)
        //{
        //    //TwilightEngine3.Twilight.FindObjectNode(this);
        //}

        private void _Init()
        {
            BasePosition = new AnimFloat2();
            Position = new AnimFloat2();
            Size = new AnimFloat2();
            RGBMulti = new AnimFloat3(new Vector3(1.0f));
            Rotation = new AnimFloat();
            Origin = new AnimFloat2(new Vector2(0.5f));
            Opacity = new AnimFloat(1.0f);
            Flip = SpriteEffects.None;
            RestoreLocationOrigin = true;
        }

        public virtual void OnObjectRegistered()
        {
        }

        public virtual void OnObjectUnregistered()
        {
        }

        protected TwilightObject()
        {
            _Init();
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Size, Rotation, Position, BasePosition });
        }

        public TwilightObject(AnimTexture tex, Vector2? size)
        {
            _Init();
            Texture = tex;
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Rotation, Size, Position, BasePosition, Texture });
            Size.Value = size ?? new Vector2(tex.Width, tex.Height);
        }

        public TwilightObject(AnimTexture tex, Vector2 size, Color rgba)
        {
            _Init();
            Texture = tex;
            animParams.AddRange(new AnimParam[] { Opacity, RGBMulti, Origin, Rotation, Size, Position, BasePosition, Texture });
            Size.Value = size;
            RGBMulti.Value = rgba.ToVector3();
        }

        protected Rectangle _GetDestRect()
        {
            Rectangle r = Bounds;
            r.X += (RestoreLocationOrigin ? (int)Math.Round(Origin.X) : 0) - TWE3.twi.Camera.X;
            r.Y += (RestoreLocationOrigin ? (int)Math.Round(Origin.Y) : 0) - TWE3.twi.Camera.Y;
            return r;
        }
        public virtual void Render()
        {
            Texture2D texture; Shader finalShader;
            Texture.GetTextureForRender(out texture, out finalShader);
            if (finalShader.ShaderEffect != null)
                XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, finalShader.BlendingOptions, null, null, null, finalShader.ShaderEffect);
            else
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, finalShader.BlendingOptions);
            XNAPlatform.Sprite.Draw(texture, _GetDestRect(), null, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, Flip, 1.0f);
            XNAPlatform.Sprite.End();
        }
        public virtual void RenderAsSubobject(Vector4 masterRGBAmulti)
        {
            XNAPlatform.Sprite.Draw(Texture.Texture, _GetDestRect(), null, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value) * masterRGBAmulti),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, Flip, Level);
        }

        public int X
        {
            get { return (int)Math.Round(Position.Value.X); }
            set { Position.Value = new Vector2(value, Position.Value.Y); }
        }
        public int Y
        {
            get { return (int)Math.Round(Position.Value.Y); }
            set { Position.Value = new Vector2(Position.Value.X, value); }
        }
        public int Width
        {
            get { return (int)Math.Round(Size.Value.X); }
            set { Size.Value = new Vector2(value, Size.Value.Y); }
        }
        public int Height
        {
            get { return (int)Math.Round(Size.Value.Y); }
            set { Size.Value = new Vector2(Size.Value.X, value); }
        }
        public Rectangle Bounds { get { return new Rectangle(X, Y, Width, Height); } }
        public int Left { get { return X; } }
        public int Right { get { return X + Width; } }
        public int Top { get { return Y; } }
        public int Bottom { get { return Y + Height; } }
        public Vector2 Center { get { return Position.Value + Size.Value * 0.5f; } }
    }
}
