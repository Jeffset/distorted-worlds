﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DWOaO.Anim;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization.Formatters.Binary;

namespace DWOaO
{
    [Serializable]
    public class ActorBone : TwilightObject
    {
        public TwilightObject ParentObject { get; private set; }

        public List<AnimFloat2> ExtraMarkers;

        protected List<ActorBone> childObjects = new List<ActorBone>();
        public void SetParentObject(TwilightObject obj)
        {
            ParentObject = obj;
            this.Position.Ctrl<V2HierarchyPosCtrl>().This = this;
            this.Position.Ctrl<V2HierarchyPosCtrl>().Parent = obj;
            this.Position.Ctrl<V2HierarchyPosCtrl>().Cfg(Position.Value);
            this.Rotation.Ctrl<FloatHierarchyAngleCtrl>().Parent = obj;
            this.Rotation.Ctrl<FloatHierarchyAngleCtrl>().Cfg(Rotation.Value);
            foreach (var marker in ExtraMarkers)
            {
                marker.Ctrl<V2HierarchyMarkerCtrl>().This = this;
                marker.Ctrl<V2HierarchyMarkerCtrl>().Parent = obj;
                marker.Ctrl<V2HierarchyMarkerCtrl>().Cfg(marker.Value);
            }
        }

        [Serializable]
        public class BoneDescription
        {
            public Point Origin;
            public Point BaseLocation;
            public List<Point> ExtraMarkers = new List<Point>();
        }

        [Serializable]
        public class V2HierarchyMarkerCtrl : Controller<AnimFloat2> // must be assigned to Location parameter!!!
        {
            public AnimFloat2 To { get; private set; }
            public AnimFloat2 From { get; private set; }
            public AnimFloat RotationRef { get; private set; }
            public float Time { get; private set; }
            public Misc.Anim AnimType { get; private set; }

            public TwilightObject Parent { get; set; }
            public TwilightObject This { get; set; }

            public override void Update()
            {
                if (IsRunning)
                {
                    base.Update();
                    //Parent.Rotation.Update();
                    //Parent.Origin.Update();
                    //Parent.Position.Update();

                    elapsedTime += GetTime();
                    if (elapsedTime > Time && AnimType != Misc.Anim.None)
                    {
                        IsRunning = false; AnimParam.Value = To.Value; return;
                        //Scriptable.UnlockScripting!
                    }
                    switch (AnimType)
                    {
                        case DWOaO.Misc.Anim.Linear:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Linear(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Linear(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case DWOaO.Misc.Anim.Bezier3:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Bezier3(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Bezier3(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case Misc.Anim.None:
                            AnimParam.Value = To.Value;
                            break;
                        default:
                            throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
                    }

                    double x1, x2, y1, y2, distance, alpha, beta;
                    Vector2 line = (AnimParam.Value - This.Origin.Value);
                    distance = line.Length();
                    x1 = line.X; y1 = line.Y;
                    alpha = Math.Acos(x1 / distance) * Math.Sign(y1);
                    beta = alpha + MathHelper.ToRadians(This.Rotation.Value);
                    x2 = distance * Math.Cos(beta);
                    y2 = distance * Math.Sin(beta);
                    AnimParam.X -= (float)(x1 - x2);
                    AnimParam.Y -= (float)(y1 - y2);
                    AnimParam.Value += This.Position.Value;
                }

            }
            public void Cfg(AnimFloat2 val)
            {
                AnimParam.Value = val.Value;
                From = AnimParam.Value; To = AnimParam.Value; Time = 0; AnimType = Misc.Anim.None;
                animParams.Clear();
                Start();
            }
            public V2HierarchyMarkerCtrl Cfg(Misc.Anim at, AnimFloat2 to, float time)
            {
                From = AnimParam.Value; To = to; Time = time; AnimType = at;
                animParams.Clear();
                animParams.AddRange(new AnimParam[] { From, To });
                Start(); return this;
            }
            public V2HierarchyMarkerCtrl()
            {
            }
        }

        [Serializable]
        public class V2HierarchyPosCtrl : Controller<AnimFloat2> // must be assigned to Location parameter!!!
        {
            public AnimFloat2 To { get; private set; }
            public AnimFloat2 From { get; private set; }
            public AnimFloat RotationRef { get; private set; }
            public float Time { get; private set; }
            public Misc.Anim AnimType { get; private set; }

            public TwilightObject Parent { get; set; }
            public TwilightObject This { get; set; }

            public override void Update()
            {
                if (IsRunning)
                {
                    base.Update();
                    //Parent.Rotation.Update();
                    //Parent.Origin.Update();
                    //Parent.Position.Update();

                    elapsedTime += GetTime();
                    if (elapsedTime > Time && AnimType != Misc.Anim.None)
                    {
                        IsRunning = false; AnimParam.Value = To.Value; return;
                        //Scriptable.UnlockScripting!
                    }
                    switch (AnimType)
                    {
                        case DWOaO.Misc.Anim.Linear:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Linear(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Linear(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case DWOaO.Misc.Anim.Bezier3:
                            AnimParam.Value =
                                new Vector2(AnimCalc.Bezier3(From.Value.X, To.Value.X, elapsedTime / Time), AnimCalc.Bezier3(From.Value.Y, To.Value.Y, elapsedTime / Time));
                            break;
                        case Misc.Anim.None:
                            AnimParam.Value = To.Value;
                            break;
                        default:
                            throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
                    }
                    double x1, x2, y1, y2, distance, alpha, beta;
                    Vector2 line = (This.Origin.Value + This.BasePosition.Value - Parent.Origin.Value - Parent.BasePosition.Value);
                    distance = line.Length();
                    x1 = line.X; y1 = line.Y;
                    alpha = Math.Acos(x1 / distance) * Math.Sign(y1);
                    beta = alpha + MathHelper.ToRadians(Parent.Rotation.Value);
                    x2 = distance * Math.Cos(beta);
                    y2 = distance * Math.Sin(beta);
                    AnimParam.X -= (float)(x1 - x2);
                    AnimParam.Y -= (float)(y1 - y2);
                    AnimParam.Value += Parent.Position.Value + This.BasePosition.Value - Parent.BasePosition.Value;
                }

            }
            public void Cfg(AnimFloat2 val)
            {
                AnimParam.Value = val.Value;
                From = AnimParam.Value; To = AnimParam.Value; Time = 0; AnimType = Misc.Anim.None;
                animParams.Clear();
                Start();
            }
            public V2HierarchyPosCtrl Cfg(Misc.Anim at, AnimFloat2 to, float time)
            {
                From = AnimParam.Value; To = to; Time = time; AnimType = at;
                animParams.Clear();
                animParams.AddRange(new AnimParam[] { From, To });
                Start(); return this;
            }
            public V2HierarchyPosCtrl()
            {
            }
        }

        [Serializable]
        public class FloatHierarchyAngleCtrl : Controller<AnimFloat> // must be assigned to Rotation parameter!!!
        {
            public AnimFloat To { get; private set; }
            public AnimFloat From { get; private set; }
            public float Time { get; private set; }
            public Misc.Anim AnimType { get; private set; }

            public TwilightObject Parent { get; set; }

            public void Cfg(AnimFloat to)
            {
                AnimParam.Value = to.Value;
                From = AnimParam.Value; To = AnimParam.Value; Time = 0; AnimType = Misc.Anim.None;
                animParams.Clear();
                Start();
            }
            public FloatHierarchyAngleCtrl Cfg(AnimFloat to, float time, Misc.Anim at = Misc.Anim.Linear, bool isAdditive = true)
            {
                From = AnimParam.Value - Parent.Rotation.Value; To = isAdditive ? To.Value + to.Value : to.Value; Time = time; AnimType = at;
                animParams.Clear();
                animParams.AddRange(new AnimParam[] { From, To });
                Start();
                return this;
            }
            public override void Update()
            {
                if (!IsRunning)
                    return;
                base.Update();
                //Parent.Rotation.Update();
                elapsedTime += GetTime();
                if (elapsedTime > Time && AnimType != Misc.Anim.None)
                {
                    Cfg(To.Value); UnlockScript(); IsRunning = true; return;
                    //return;
                    //Scriptable.UnlockScripting!
                }
                switch (AnimType)
                {
                    case DWOaO.Misc.Anim.Linear:
                        AnimParam.Value = AnimCalc.Linear(From.Value, To.Value, elapsedTime / Time) + Parent.Rotation.Value;
                        break;
                    case DWOaO.Misc.Anim.Bezier3:
                        AnimParam.Value = AnimCalc.Bezier3(From.Value, To.Value, elapsedTime / Time) + Parent.Rotation.Value;
                        break;
                    case Misc.Anim.None:
                        AnimParam.Value = To.Value + Parent.Rotation.Value;
                        break;
                    default:
                        throw new NotSupportedException("This animation type is not supported by PosSimpleCtrl!");
                }
            }

            public FloatHierarchyAngleCtrl()
            {
            }

            public void n() { }
        }

        public ActorBone(string texName)
            : base(new AnimTexture(texName), null)
        {
            using (BinaryReader br = new BinaryReader(File.OpenRead(XNAPlatform.ContentManager.RootDirectory + "\\" + texName + ".bone")))
            {
                BasePosition.Value = new Vector2(br.ReadInt32(), br.ReadInt32());
                Origin.Value = new Vector2(br.ReadInt32(), br.ReadInt32()) - BasePosition.Value;
                Vector2[] markers = new Vector2[br.ReadInt32()];
                for (int i = 0; i < markers.Length; i++)
                    markers[i] = new Vector2(br.ReadInt32(), br.ReadInt32());
                ExtraMarkers = markers.Select(m => new AnimFloat2(new Vector2(m.X, m.Y) - BasePosition.Value)).ToList();
                animParams.AddRange(ExtraMarkers);
            }
        }

        public ActorBone.FloatHierarchyAngleCtrl RC()
        { return this.Rotation.Ctrl<ActorBone.FloatHierarchyAngleCtrl>(); }

        public ActorBone.FloatHierarchyAngleCtrl this[AnimFloat To, float time, Misc.Anim t = Misc.Anim.Linear]
        {
            get { return RC().Cfg(To, time, t); }
        }

        public override void Render()
        {
            Vector2 pos = Position.Value + (RestoreLocationOrigin ? Origin.Value : Vector2.Zero) - TWE3.twi.Camera.Position.Value;
            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, Texture.Shader.BlendingOptions);
            XNAPlatform.Sprite.Draw(Texture.Texture, pos, null, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)),
                MathHelper.ToRadians(Rotation.Value), Origin.Value, 1.0f, Flip, 1.0f);
            XNAPlatform.Sprite.End();
        }
    }
}
