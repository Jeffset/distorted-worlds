﻿using System;
using DWOaO.ActorModels;
using Microsoft.Xna.Framework;
using System.Threading;
using Microsoft.Xna.Framework.Audio;
using System.Linq;
using System.Collections.Generic;

namespace DWOaO
{
    public static class HumanOccupyScript
    {
        public static void HumanOccupy(Actor act, EnergySource source)
        {
            if (act.Model.CurrentModelScript != null) act.Model.CurrentModelScript.Terminate();
            act.Model.CurrentModelScript = Script.Start<Action<Actor, EnergySource>>(_HumanOccupy,null, act, source);
        }
        private static void _HumanOccupy(Actor act, EnergySource source)
        {
            try
            {
                TWE3.Gtwi.GameControlFullyDenied = true;
                Humanoid_mdl A = act.Model as Humanoid_mdl;
                float t = 600;
                switch (A.Orientation)
                {
                    case Orient.ToUs:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, 30), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[-15, t, Misc.Anim.Bezier3].n();
                        A.Head[+20, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[+20, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+45, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[+17, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-30, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[+20, t, Misc.Anim.Bezier3].n();
                        A.LeftHand[+20, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -30), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToRight:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(5, 20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[+55, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[-90, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[+70, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[-115, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+60, t, Misc.Anim.Bezier3].n();
                        A.Head[-40, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[-60, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+60, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToLeft:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, +20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[-55, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[+90, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-70, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[+80, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[-50, t, Misc.Anim.Bezier3].n();
                        A.Head[+40, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[+60, t, Misc.Anim.Bezier3].n();
                        A.LeftHand[-30, t, Misc.Anim.Bezier3].n();
                        A.Weapon[-90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(+5, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToForward:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[+15, t, Misc.Anim.Bezier3].n();
                        A.Head[-20, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[-20, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-45, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[-17, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+30, t, Misc.Anim.Bezier3].n();
                        A.RightArm[-20, t, Misc.Anim.Bezier3].n();
                        A.RightHand[-20, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+20, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, +20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                }
            }
            catch (ThreadAbortException)
            { }
            finally
            {
                act.Model.ResetModelState();
                TWE3.Gtwi.GameControlFullyDenied = false;
                (act.Model as Humanoid_mdl).RunIdleBreathScript();
            }
        }
    }

    public static class ScriptFireBall
    {
        public struct FireBallParams
        {
            public AnimFloat3 FromColor;
            public AnimFloat3 ToColor;

            public AnimFloat2 FromPosition;
            public AnimFloat2 ToPosition;
            public float TossHeight;

            public AnimFloat2 StartSize;
            public AnimFloat2 EndSize;

            public float Time;

            public string Sound;

            public int ParticleCount;
        }
        public static Script<Action<FireBallParams>> Start(FireBallParams ps)
        { return Script.Start<Action<FireBallParams>>(_Script, ps); }

        private static void _Script(FireBallParams p)
        {
            if(p.Sound!=null)TwiSound.PlaySnd(p.Sound);
            AnimFloat2 ballPos = p.FromPosition.Value, ballRad = p.StartSize.Value;
            LightSource ballGlow = new LightSource(altitude: 10f, lightDistance: 300, lightIntensity: 0f);
            ballGlow.Position.Value = p.FromPosition.Value;
            TwiParticleSystem ball = new TwiParticleSystem
                (p.ParticleCount, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(ballPos, ballRad) },
                particleTexName: "rg", minTime: 60, maxTime: 400, at: Misc.Anim.Linear);
            ballGlow.LightColor = ball.RGBMulti;
            TWE3.twi.RegisterObject(ball);
            TWE3.twi.RegisterLightSource(ballGlow);
            ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0, 1f, p.Time).WaitForFinish(150);
            ball.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg
                (Misc.Anim.Linear,p.FromColor, p.ToColor, p.Time);
            ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3,0, 0.5f, p.Time);
            ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.8f, p.Time*0.2f);
            ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear,p.StartSize, p.EndSize, p.Time);
            ballGlow.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(Vector2.Lerp(ballPos.Value, p.ToPosition.Value, 0.5f), p.ToPosition.Value, p.Time);
            ballPos.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(p.FromPosition, Vector2.Lerp(p.FromPosition.Value, p.ToPosition.Value, 0.5f) + new Vector2(0, -p.TossHeight), p.ToPosition.Value, p.Time)
                .WaitForFinish(p.Time);
            float t = 1000;
            ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0, t);
            ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, t);
            ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, p.EndSize.Value*1.5f, t).WaitForFinish(t);
            TWE3.twi.UnregisterObject(ball);
            TWE3.twi.UnregisterLightSource(ballGlow);
            //mdl.Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(Misc.Anim.Bezier3, 50, radius);
            //ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
            //ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
            //TwiSound.PlaySnd("Create1");
            //foreach (var bone in mdl.Bones)
            //{
            //    bone.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, 1f, 200);
            //    bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, UF.PutOnA(bone, fld) - mdl.Position.Value, bone.BasePosition.Value, 300)
            //        .WaitForFinish(100);
            //}
            //radius.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(0), 100).WaitForFinish(110);
            //TWE3.twi.UnregisterObject(ball);
            //TWE3.twi.UnregisterLightSource(ballGlow);
            //mdl.PlaceOnField(fld.FieldPos.X, fld.FieldPos.Y);
        }
    }
}