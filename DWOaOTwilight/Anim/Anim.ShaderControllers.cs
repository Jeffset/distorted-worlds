﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO.Anim
{
    [Serializable]
    public abstract class ShaderController : Controller<AnimTexture>
    {
        public virtual void ApplyShaderParameters() { }
    }

    [Serializable]
    public class DisplacementShader : ShaderController
    {
        private Effect _shader;

        public AnimFloat Strength { get; private set; }
        public AnimFloat ScrollX { get; private set; }
        public AnimFloat Factor { get; private set; }
        public AnimTexture DisplacementMap { get; private set; }
        public AnimTexture MaskMap { get; private set; }

        public void Cfg(AnimTexture displMap, AnimTexture mask)
        {
            animParams.Clear();
            DisplacementMap = displMap;
            MaskMap = mask;
            animParams.AddRange(new AnimParam[] { Strength, ScrollX, Factor, DisplacementMap });
            if (mask != null) animParams.Add(MaskMap);
            Start();
        }

        public override void Update()
        {
            base.Update();
            AnimParam.Shader.ShaderEffect = _shader;
        }
        public override void ApplyShaderParameters()
        {
            _shader.Parameters[0].SetValue(Factor.Value);
            _shader.Parameters[1].SetValue(Strength.Value);
            _shader.Parameters[2].SetValue(ScrollX.Value);
            _shader.Parameters[3].SetValue(DisplacementMap.Texture);
            if (MaskMap != null) _shader.Parameters[4].SetValue(MaskMap.Texture);
        }
        public DisplacementShader()
        {
            _shader = XNAPlatform.ContentManager.Load<Effect>("Shaders\\Displacement");
            ScrollX = 0.0f;
            Factor = 2.0f;
            Strength = 0.2f;
        }
    }
}
