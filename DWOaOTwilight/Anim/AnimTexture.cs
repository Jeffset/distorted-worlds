﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace DWOaO
{
    [Serializable]
    public class DistortedFont
    {
        [NonSerialized]
        public SpriteFont Font;

        public string name;

        public DistortedFont(string font)
        {
            name = font;
            Font = XNAPlatform.ContentManager.Load<SpriteFont>(@"Fonts\" + name);
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Font = XNAPlatform.ContentManager.Load<SpriteFont>(@"Fonts\" + name);
        }
    }

    public class Shader
    {
        public Effect ShaderEffect { get; set; }
        public BlendState BlendingOptions { get; set; }
    }

    [Serializable]
    public class AnimTexture : AnimParam
    {
        //static Dictionary<Texture2D, Texture2D> GeneratedNormalMaps = new Dictionary<Texture2D, Texture2D>();
        public Texture2D GenerateNormalFromBump(Texture2D tex)
        {
            RenderTarget2D res;
            res = new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, tex.Width, tex.Height, false, SurfaceFormat.Bgr565, DepthFormat.None);
            XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(res);
            Effect generator = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\NormalGen");
            XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.AnisotropicClamp, DepthStencilState.None, RasterizerState.CullNone, generator);
            XNAPlatform.Sprite.Draw(tex, Vector2.Zero, Color.White);
            XNAPlatform.Sprite.End();
            //GeneratedNormalMaps[tex] = res;
            return res;
        }


        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Shader = new Shader();
            Shader.BlendingOptions = BlendState.NonPremultiplied;
            if (ResourceName != null && ResourceName != "")
            {
                Texture = XNAPlatform.ContentManager.Load<Texture2D>(ResourceName);
                try { NormalMap = XNAPlatform.ContentManager.Load<Texture2D>(ResourceName + "_bump"); }
                catch (Exception) { }
            }

        }

        public C Ctrl<C>() where C : Anim.ShaderController, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimTexture>).AnimParam = this;
            }
            return _controller as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        [NonSerialized]
        public Texture2D Texture;
        [NonSerialized]
        public Texture2D NormalMap;

        public string ResourceName { get; private set; }
        public int Width { get { return Texture.Width; } }
        public int Height { get { return Texture.Height; } }

        [NonSerialized]
        public Shader Shader;

        public void GetTextureForRender(out Texture2D texture, out Shader shader)
        {
            texture = Texture;
            shader = this.Shader;
            if (_controller != null) (_controller as Anim.ShaderController).ApplyShaderParameters();
        }

        public AnimTexture(string textureName, bool loadNormalMap = false)
        {
            Texture = XNAPlatform.ContentManager.Load<Texture2D>(textureName);
            if (loadNormalMap)
                NormalMap = XNAPlatform.ContentManager.Load<Texture2D>(textureName + "_bump");
            ResourceName = textureName;
            Shader = new Shader();
            Shader.BlendingOptions = BlendState.NonPremultiplied;
        }
        public AnimTexture(int width, int height, bool generateNormalMap = false)
        {
            Texture = new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None);
            if (generateNormalMap)
                NormalMap = new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, width, height, false, SurfaceFormat.Color, DepthFormat.None);
            Shader = new Shader();
            Shader.BlendingOptions = BlendState.NonPremultiplied;
        }

        public AnimTexture(Texture2D texture2D, Texture2D normalMap = null)
        {
            Texture = texture2D;
            NormalMap = normalMap;
            Shader = new Shader();
            Shader.BlendingOptions = BlendState.NonPremultiplied;
        }

        public override void Update()
        {
            if (_controller != null) { _controller.Update(); }
            if (_constraint != null) _constraint.Update();
        }
    }
}
