﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    [Serializable]
    public abstract class Animatable
    {
        protected List<AnimParam> animParams = new List<AnimParam>();

        public virtual void Update()
        {
            for (int i = 0; i < animParams.Count; i++)
            {
                if (!animParams[i].IsUpdated)
                    animParams[i].Update();
            }
        }

        internal virtual void MakeNeedUpdate()
        {
            for (int i = 0; i < animParams.Count; i++)
            {
                if (!animParams[i].IsUpdated)
                    animParams[i].Update();
            }
        }
    }
}
