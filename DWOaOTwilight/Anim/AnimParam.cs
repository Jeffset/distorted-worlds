﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DWOaO
{
    [Serializable]
    public abstract class AnimParam
    {
        protected Anim.Controller _controller;
        protected Anim.Constraint _constraint;

        public bool IsUpdated { get; protected set; }
        public virtual void Update() { }
        public virtual void TerminateAnimation() { }

        internal void MakeNeedUpdate()
        {
            IsUpdated = false;
            if (_controller != null)
                _controller.MakeNeedUpdate();
            if (_constraint != null)
                _constraint.MakeNeedUpdate();
        }
    }

    [Serializable]
    public class AnimFloat : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat>).AnimParam = this;
            }
            return _controller as C;
        }
        public C Cnstr<C>() where C : Anim.Constraint<AnimFloat>, new()
        {
            if (!(_constraint is C))
            {
                _constraint = new C();
                (_constraint as Anim.Constraint<AnimFloat>).AnimParam = this;
            }
            return _constraint as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        public float Value;

        public override void Update()
        {
            base.Update();
            if (_controller != null)
                _controller.Update();
            if (_constraint != null)
                _constraint.Update();
            //IsUpdated = true;
        }
        public AnimFloat()
        { Value = 0.0f; }
        public AnimFloat(float v)
        { Value = v; }
        public static implicit operator AnimFloat(float v)
        { return new AnimFloat(v); }
    }

    [Serializable]
    public class AnimFloat2 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat2>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat2>).AnimParam = this;
            }
            return _controller as C;
        }
        public C Cnstr<C>() where C : Anim.Constraint<AnimFloat2>, new()
        {
            if (!(_constraint is C))
            {
                _constraint = new C();
                (_constraint as Anim.Constraint<AnimFloat2>).AnimParam = this;
            }
            return _constraint as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        public Vector2 Value;

        public float X { get { return Value.X; } set { Value.X = value;} }
        public float Y { get { return Value.Y; } set { Value.Y = value;} }

        public override void Update()
        {
            base.Update();
            if (_controller != null)
                _controller.Update();
            if (_constraint != null)
                _constraint.Update();
            //IsUpdated = true;
        }
        public AnimFloat2()
        { Value = new Vector2(); }
        public AnimFloat2(Vector2 v)
        { Value = v; }
        public AnimFloat2(float x, float y)
        { Value = new Vector2(x, y); }
        public static implicit operator AnimFloat2(Vector2 v)
        { return new AnimFloat2(v); }
    }
    [Serializable]
    public class AnimFloat3 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat3>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat3>).AnimParam = this;
            }
            return _controller as C;
        }
        public C Cnstr<C>() where C : Anim.Constraint<AnimFloat3>, new()
        {
            if (!(_constraint is C))
            {
                _constraint = new C();
                (_constraint as Anim.Constraint<AnimFloat3>).AnimParam = this;
            }
            return _constraint as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        public Vector3 Value;

        public override void Update()
        {
            base.Update();
            if (_controller != null)
                _controller.Update();
            if (_constraint != null)
                _constraint.Update();
            //IsUpdated = true;
        }
        public AnimFloat3()
        { Value = new Vector3(); }
        public AnimFloat3(Vector3 v)
        { Value = v; }
        public static implicit operator AnimFloat3(Vector3 v)
        { return new AnimFloat3(v); }
        public static implicit operator AnimFloat3(Color color)
        { return new AnimFloat3(color.ToVector3()); }
    }
    [Serializable]
    public class AnimFloat4 : AnimParam
    {
        public C Ctrl<C>() where C : Anim.Controller<AnimFloat4>, new()
        {
            if (!(_controller is C))
            {
                _controller = new C();
                (_controller as Anim.Controller<AnimFloat4>).AnimParam = this;
            }
            return _controller as C;
        }
        public C Cnstr<C>() where C : Anim.Constraint<AnimFloat4>, new()
        {
            if (!(_constraint is C))
            {
                _constraint = new C();
                (_constraint as Anim.Constraint<AnimFloat4>).AnimParam = this;
            }
            return _constraint as C;
        }

        public override void TerminateAnimation()
        { _controller = null; }

        public Vector4 Value;

        public override void Update()
        {
            base.Update();
            if (_controller != null)
                _controller.Update();
            if (_constraint != null)
                _constraint.Update();
            //IsUpdated = true;
        }
        public AnimFloat4()
        { Value = new Vector4(); }
        public AnimFloat4(Vector4 v)
        { Value = v; }
        public static implicit operator AnimFloat4(Vector4 v)
        { return new AnimFloat4(v); }
    }
}
