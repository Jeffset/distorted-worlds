﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;

namespace DWOaO.Anim
{
    [Serializable]
    public abstract class Controller : Animatable
    {
        [OnDeserialized]
        private void _OnDeserialized(StreamingContext context)
        {
            _lastTime = (int)Global.GameTime.TotalGameTime.TotalMilliseconds;
        }
        public bool IsRunning { get; protected set; }
        private int _lastTime;
        private bool _isScriptingLocked = false;
        protected float elapsedTime;
        protected int GetTime()
        {
            int ms = (int)Global.GameTime.TotalGameTime.TotalMilliseconds - _lastTime;
            _lastTime = (int)Global.GameTime.TotalGameTime.TotalMilliseconds;
            return ms;
        }
        public override void Update()
        {
            base.Update();
            //if (!_isScriptingLocked)
            //{
            //    Monitor.Enter(this);
            //    _isScriptingLocked = true;
            //}
        }

        protected void Start()
        {
            //UnlockScript();
            elapsedTime = 0.0f;
            _lastTime = (int)Global.GameTime.TotalGameTime.TotalMilliseconds;
            IsRunning = true;
        }
        protected void UnlockScript()
        {
            //if (_isScriptingLocked)
            //{
            //    _isScriptingLocked = false;
            //    Monitor.Exit(this);
            //}
        }
        protected void Finish()
        {
            IsRunning = false;
            //UnlockScript();
        }
        public void WaitForFinish(float time)
        {
            Monitor.Exit(TWE3.twi);
            Thread.Sleep((int)Math.Round(time)+10);
            Monitor.Enter(TWE3.twi);
        }
        protected Controller()
        {
            elapsedTime = 0.0f;
        }
    }
    [Serializable]
    public abstract class Controller<AP> : Controller where AP : AnimParam
    {
        public AP AnimParam { get; set; }
    }

    [Serializable]
    public abstract class Constraint : Animatable
    {
        private int _lastTime;
        protected int GetTime()
        {
            int ms = (int)Global.GameTime.TotalGameTime.TotalMilliseconds - _lastTime;
            _lastTime = (int)Global.GameTime.TotalGameTime.TotalMilliseconds;
            return ms;
        }
        protected void Start()
        {
            _lastTime = (int)Global.GameTime.TotalGameTime.TotalMilliseconds;
        }
    }
    [Serializable]
    public class Constraint<AP> : Constraint where AP : AnimParam
    {
        public AP AnimParam { get; set; }
    }

}
