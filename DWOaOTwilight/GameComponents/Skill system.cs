﻿using DWOaO.Misc;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace DWOaO
{
    [Serializable]
    public class EnergyReservoir : DistortedGameComponent
    {
        public Actor OwnerActor
        { get { return OwnerComponent as Actor; } set { OwnerComponent = value; } }

        public ReservoirWidget widget { get; private set; }

        public int Capacity { get; set; }

        private int _currentValue;
        public int CurrentValue
        {
            get { return _currentValue; }
            set { _currentValue = value > Capacity ? Capacity : value < 0 ? 0 : value; if (widget != null) widget.AnimateValues(); }
        }

        public void MakeFull()
        { CurrentValue = Capacity; }
        public int MaxPhaseConsumption { get; set; }
        public int AvialablePhaseConsumption { get; set; }
        public int RegenRate { get; set; }


        public Color EnergyColor { get; set; }

        public string Name { get; set; }

        public Level Level = new Level(20, null);

        public EnergyReservoir(Color color, string type, Actor owner, int offset)
        {
            GraphicIndicatorOffset = offset;
            OwnerActor = owner;
            EnergyColor = color;
            Name = type;
            if (type != "DEFAULT")
                widget = new ReservoirWidget(type, this);
            else
            {
                Capacity = 1; CurrentValue = 1; MaxPhaseConsumption = 1;
            }
        }

        public List<Skill> ReservoirSkills = new List<Skill>();
        public void AddSkill(Skill skill) { ReservoirSkills.Add(skill); skill.Reservoir = this; _components.Add(skill); }

        public int GraphicIndicatorOffset;
        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            AvialablePhaseConsumption = MaxPhaseConsumption;
            #region IndicateRegen
            if (GraphicIndicatorOffset != -1 && RegenRate != 0)
            {
                TextWidget tw = new TextWidget(Name + ": +" + (RegenRate).ToString(), "Jing Jing 14", EnergyColor);
                tw.UseGlobalCood = true;
                if (OwnerActor is ExtraIncarnator)
                UF.PutOn(tw, (OwnerActor as ExtraIncarnator).VisualIncarnator);
                //{ tw.X = (OwnerActor as ExtraIncarnator).VisualIncarnator.Right;tw.Y = (OwnerActor as ExtraIncarnator).VisualIncarnator.Bottom; }
                else
                UF.PutOn(tw, OwnerActor.Model);
                //{ tw.X = OwnerActor.Model.Right; tw.Y = OwnerActor.Model.Bottom; }
                tw.Y -= 50 * GraphicIndicatorOffset;
                Script.Start<Action<TextWidget>>(ind =>
                {
                    TWE3.twi.RegisterWidget(ind);
                    ind.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0, 1, 150);
                    ind.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -100), 3000).WaitForFinish(3000);
                    TWE3.twi.UnregisterWidget(ind);
                }, tw);
            }
            #endregion
            CurrentValue += RegenRate;
            if (OwnerActor as Omnit != null)
                foreach (var s in ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).RegisterSkillWatchers();
        }
        public override void OmniPhaseEnd()
        {
            base.OmniPhaseEnd();
            if (OwnerActor as Omnit != null)
                foreach (var s in ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).UnregisterSkillWatchers();
        }
    }

    [Serializable]
    public abstract class Skill : DistortedGameComponent
    {
        // регистрацией/дерегистрацией и размещением этого граф. компонента занимается панелька.
        public SkillWidget widget;

        public EnergyReservoir Reservoir { get; set; }

        public Actor Owner;

        public Level Level;

        public string FriendlyDescription { get; protected set; }

        public virtual void Deactivated()
        { }
    }

    [Serializable]
    public abstract class ActiveSkill : Skill
    {
        public int Range { get; set; }
        public int CoolDown { get; set; }
        public int CurrentCoolDown { get; set; }
        public int OmnipointConsumption { get; set; }
        public int ReservoirConsumption { get; set; }

        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            CurrentCoolDown = CurrentCoolDown == 0 ? 0 : CurrentCoolDown - 1;
        }

        public virtual void RegisterSkillWatchers()
        { }
        public virtual void UnregisterSkillWatchers()
        { }
        protected bool CheckRange(TwilightField target)
        {
            Point p1 = target.FieldPos, p2 = Owner.GetField().FieldPos;
            Vector2 dist = new Vector2(p1.X, p1.Y) - new Vector2(p2.X, p2.Y);
            if (dist.LengthSquared() <= Range * Range)
                return true;
            else
            {
                TWE3.Gtwi.GUI.ShowMessage("Не хватает радиуса действия!", Color.HotPink);
                return false;
            }
        }
        protected bool CheckAvialabilityToUse()
        {
            if (TWE3.Gtwi.ActiveSkill != null && TWE3.Gtwi.ActiveSkill != this) return false;
            if (TWE3.twi.GameControlDenied)
            { TWE3.Gtwi.GUI.ShowMessage("Инкарнация...Подождите...", Color.LightGray); return false; }
            if (CurrentCoolDown != 0)
            { TWE3.Gtwi.GUI.ShowMessage("Ресурс действия исчерпан. Регенерация: " + CurrentCoolDown.ToString(), Color.Cyan); return false; }
            if (!Owner.IsEnoughOmnipoints(OmnipointConsumption))
            { TWE3.Gtwi.GUI.ShowMessage("Недостаточно омнипоинтов!", Color.Red); return false; }
            if (Reservoir.CurrentValue < ReservoirConsumption)
            { TWE3.Gtwi.GUI.ShowMessage("Недостаточно энергии резервуара!", Color.Red); return false; }
            if (Reservoir.AvialablePhaseConsumption < ReservoirConsumption)
            { TWE3.Gtwi.GUI.ShowMessage("Недостаточно мощности резервуара!", Color.Red); return false; }
            return true;
        }

        protected void PerformConsumption()
        {
            Reservoir.AvialablePhaseConsumption -= ReservoirConsumption;
            Reservoir.CurrentValue -= ReservoirConsumption;
            Owner.Omnipoints.Value -= OmnipointConsumption;
            CurrentCoolDown = CoolDown;
            TWE3.Gtwi.GUI.ShowPanel(Owner);
        }

        protected virtual void ScriptAction()
        { }

        protected ActiveSkill()
        {
            CoolDown = 1;
        }
    }

    [Serializable]
    public abstract class PassiveSkill : Skill
    {
    }

    [Serializable]
    public abstract class DefensiveSkill : Skill
    {
        protected int OmnipointsReserve;

        protected int ComingDamage;
        protected float TimeToStrike;
        protected Actor Attacker;

        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            ComingDamage = 0;
            TimeToStrike = 0;
            Attacker = null;
        }

        protected virtual void DefenceAction()
        { }

        protected virtual void DestructionAction()
        { }

        private bool CheckDeath()
        {
            return Reservoir.CurrentValue - ComingDamage <= 0;
        }

        protected void PerformConsumption()
        {
            Reservoir.CurrentValue -= ComingDamage;
            Script.Start<Action<int>>((dmg) =>
            {
                TextWidget ind = new TextWidget((-dmg).ToString(), "Jing Jing Level", Color.Orange);
                ind.UseGlobalCood = true;
                TWE3.twi.RegisterWidget(ind);
                UF.PutOn(ind, Owner.Model);
                float d = 300;
                var dt =
                Owner.Model.Orientation == ActorModels.Orient.ToForward ? new Vector2(0, d) :
                Owner.Model.Orientation == ActorModels.Orient.ToUs ? new Vector2(0, -d) :
                Owner.Model.Orientation == ActorModels.Orient.ToLeft ? new Vector2(d, 0) :
                Owner.Model.Orientation == ActorModels.Orient.ToRight ? new Vector2(-d, 0) : Vector2.Zero;
                var dd1 = new Vector2(dt.X / 2, dt.Y - d);
                ind.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel(dd1, dt, 600);
                //ind.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0, 1, 1200);
                ScriptControl.Wait(1200);
                TWE3.twi.UnregisterWidget(ind);
            }, ComingDamage);
            ComingDamage = 0;
        }

        public void ActivateDefence()
        {
            Owner.Omnipoints.Value -= OmnipointsReserve;
            Owner.CurrentDefence = this;
        }

        public void PerformDefence(Actor attacker, int comingDamage, float timeToStrike)
        {
            Owner.Model.Orientation = UF.GetOrient(Owner.Model.Center, attacker.Model.Center);
            Attacker = attacker; ComingDamage = comingDamage; TimeToStrike = timeToStrike;
            if (CheckDeath())
                DestructionAction();
            else
                DefenceAction();
        }
    }
}