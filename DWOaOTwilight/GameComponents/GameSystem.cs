﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace DWOaO
{

    public static class GsysCfg
    {
        public const int INITIAL_OMNIPOINTS = 5;
    }

    // Базовый класс для всех игровых объектов;
    // Все производные игровые компоненты должны представлять себя на экране с помощью TwilightObject.
    [Serializable]
    public abstract class DistortedGameComponent
    {

        protected List<DistortedGameComponent> _components = new List<DistortedGameComponent>();

        public DistortedGameComponent OwnerComponent;

        public virtual void IncoPhaseStart()
        {
        }

        public virtual void IncoPhaseEnd()
        {
        }

        public virtual void OmniPhaseStart()
        {
            foreach (var component in _components)
                component.OmniPhaseStart();
        }
        public virtual void OmniPhaseEnd()
        {
            foreach (var component in _components)
                component.OmniPhaseEnd();
        }
    }

    public struct Level
    {
        private int _exp;
        public int EXP
        {
            get { return _exp; }
            set
            {
                Max = 10;
                _exp = value;
                if (Current != GVAL.OMNIT_MAX_LEVEL && _exp >= GVAL.OmnitReqEXPLevel(Current + 1))
                {
                    Current++; _exp = 0;
                }
            }
        }

        public NumberWidget widget;

        private int _max;
        public int Max
        {
            get { return _max; }
            set { _max = value; }
        }

        private int _level;
        public int Current
        {
            get { return _level; }
            set { _level = value > Max ? Max : value < 0 ? 0 : value; if (widget != null) widget.Value.Value = _level; }
        }

        public Level(int max, NumberWidget w)
        {
            widget = w;
            _max = max;
            _level = 1;
            _exp = 0;
            Current = 1;
        }
    }

    public class OmniPoints
    {
        public NumberWidget widget;

        private int _omnipoints;
        public int Value
        {
            get { return _omnipoints; }
            set { _omnipoints = Math.Max(value, 0); if (widget != null) widget.Value.Value = _omnipoints; }
        }

        public OmniPoints(NumberWidget w)
        {
            widget = w;
            Value = 0;
        }
    }
}