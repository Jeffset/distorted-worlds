﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using DWOaO.ActorModels;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;
using System.Linq;
using DWOaO.Misc;

namespace DWOaO.Life
{
    [Serializable]
    public class Maneuver_Uni : ActiveSkill
    {
        private List<FieldGoInfo> AvialableFields = new List<FieldGoInfo>();
        public class FieldGoInfo : TwilightObject
        {
            public TwilightField Field;
            public bool HasBeenPassed = false;
            public List<FieldGoInfo> Track = new List<FieldGoInfo>();
            public float MinTrack = -100000;
            int x; int y;

            public static FieldGoInfo Get(int x, int y)
            {
                var f = TWGE3.Gtwi.GameMap.FieldMatrix[x, y];
                if (f.GoInfo != null) return f.GoInfo;
                else return new FieldGoInfo(f);
            }

            public FieldGoInfo(TwilightField f)
                : base(new AnimTexture("cell"), Global.CellSize * 0.95f)
            {
                if (f == null) return;
                Level = Misc.TwilightLayer.ServiceLayer;
                RGBMulti.Value = Color.Cyan.ToVector3();
                Opacity.Value = 0.35f;
                Texture.Shader.BlendingOptions = BlendState.AlphaBlend;
                Position.Value = Misc.UF.FieldToAbs(f.FieldPos.X, f.FieldPos.Y);
                this.x = f.FieldPos.X; this.y = f.FieldPos.Y;
                Field = f; f.GoInfo = this;
            }
            public FieldGoInfo(int x, int y)
                : this(TWGE3.Gtwi.GameMap.FieldMatrix[x, y])
            { }
            public FieldGoInfo Near(Orient whereNear)
            {
                try
                {
                    switch (whereNear)
                    {
                        case Orient.ToUs: return FieldGoInfo.Get(x, y + 1);
                        case Orient.ToRight: return FieldGoInfo.Get(x + 1, y);
                        case Orient.ToLeft: return FieldGoInfo.Get(x - 1, y);
                        case Orient.ToForward: return FieldGoInfo.Get(x, y - 1);
                    }
                    return null;
                }
                catch (IndexOutOfRangeException e)
                {
                    return null;
                }
            }
        }

        private void ShowFieldsAvailable(float go, FieldGoInfo curr, FieldGoInfo prev)
        {
            if (curr == null) return;
            if (curr.MinTrack >= go || go < curr.Field.GetPassEnergy(prev.Field, Owner))
                return;
            if (curr.MinTrack == -100000)
            {
                curr.RGBMulti.Value = Owner.SideColor.ToVector3();
                TWGE3.twi.RegisterObject(curr);
                AvialableFields.Add(curr);
            }
            curr.MinTrack = go;
            curr.Track.Clear();
            if (prev != null)
            { curr.Track.AddRange(prev.Track); if (prev.Field != null) curr.Track.Add(prev); }
            for (int o = 0; o < 4; o++)
            {
                ShowFieldsAvailable(go - curr.Field.GetPassEnergy(prev.Field, Owner), curr.Near((Orient)o), curr);
            }
        }

        public int ManeuverDistance = GVAL.MANEUVER_DIST_DEFAULT;
        // Обработчик события в движке, который проверяет макродействие,
        // и если замечает его, то выполняет скилл немедленно или активирует скилл
        // и подписывается на нужные движковые события для настройки скилла и его последующего выполнения. 
        void MacroactionWatcher(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            TwilightField f = TWGE3.Gtwi.QueryField(pos);
            bool requestToUse = f.Settler == Owner;

            if (!requestToUse) return;

            if (!CheckAvialabilityToUse()) return;
            //if (mb == MouseButton.RMB) return;

            TWE3.Gtwi.ActivateSkill(this);
            TWE3.Gtwi.MouseEnterField += GameTwilight_MouseEnterField;
            TWE3.Gtwi.DragEnd += GameTwilight_DragEnd;
            ShowFieldsAvailable(ManeuverDistance, new FieldGoInfo(f), new FieldGoInfo(null));
            foreach (var fi in AvialableFields)
            {
                fi.Opacity.Value = 0.0f;
                var scr = Script.Start<Action<FieldGoInfo, int>>((finf, t) =>
                    {
                        ScriptControl.Wait((t == 0 ? 0 : t - 1) * 70);
                        finf.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, 0.35f, 100);
                    }, fi, fi.Track.Count);
            }
            TWE3.Gtwi.DragBegin -= MacroactionWatcher;
        }
        // Когда пользователь выбрал нужное ему поле. 
        TwilightField targetField;
        void GameTwilight_DragEnd(MouseButton mb, Vector2 old, Vector2 pos)
        {
            if (TWE3.Gtwi.GameControlDenied) return;
            if (mb != MouseButton.LMB) return;
            targetField = TWGE3.Gtwi.QueryField(pos);
            if (!AvialableFields.Contains(targetField.GoInfo))
            {
                Deactivated();
                TWE3.Gtwi.ActiveSkill = null;
                return;
            }
            targetField.GoInfo.Track.Add(targetField.GoInfo);
            TwiSound.IdleSpeak(Owner.SystemName);
            //HumanRunScript.HumanRun(Owner, f.GoInfo.Track.ToArray());
            ScriptAction();
            targetField.GoInfo.Track.Remove(targetField.GoInfo);
            Deactivated();
            PerformConsumption();
        }
        private float speed;
        private void _HumanRunMove(Actor act, Life.Maneuver_Uni.FieldGoInfo[] flds)
        {
            TWE3.Gtwi.GameControlFullyDenied = true;
            Vector2[] points = flds.Select(f => f.Position.Value).ToArray();
            var animScr = Script.Start<Action<Actor>>(_HumanRunAnim, act);
            act.Model.Mblur.Enabled = true;
            if (act.Model.IsEtherial) act.Model.Mblur.Length = 50;
            act.RegisterAsSettler(false);
            for (int i = 0; i < points.Length; i++)
            {
                points[i] -= new Vector2(0, act.Model.Height - Global.Cell_V + 40);
            }
            for (int i = 0; i < points.Length - 1; i++)
            {
                speed = (act.RunSpeed / (flds[i].Field.PassEnergy + flds[i].Field.PassEnergy) * 2);
                Orient o = Misc.UF.GetOrient(points[i], points[i + 1]);
                if (o == Orient.ToUs || o == Orient.ToForward) speed *= Global.PerspectiveFactor;
                float t = (points[i] - points[i + 1]).Length() / speed;
                if (act.Model.Orientation != o)
                { animScr.Terminate(); animScr = Script.Start<Action<Actor>>(_HumanRunAnim, act); }
                act.Model.Orientation = o;

                //bool isLiquid = flds[i + 1].Field.Attribute.SurfaceType == MapSystem.FieldSurfaceType.Liquid;
                //bool wasLiquid = flds[i].Field.Attribute.SurfaceType == MapSystem.FieldSurfaceType.Liquid;
                //AnimFloat2 dest = points[i + 1] + (wasLiquid ? new Vector2(0, 65) : Vector2.Zero);

                //var mdl = act.Model as Humanoid_mdl;
                //mdl.LeftFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, isLiquid ? 0f : 1f, 100);
                //mdl.RightFoot.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, isLiquid ? 0f : 1f, 100);
                act.Model.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, points[i + 1], t).WaitForFinish(t * 0.9f);
                //if (isLiquid)
                //    dest.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, 65), t / 2f).WaitForFinish(t / 2f - 20);

                foreach (var bone in act.Model.Bones) bone.Position.Update();
            }

            animScr.Terminate();
            act.Model.Mblur.Enabled = false;
            act.Model.RunIdleBreathScript();
            ScriptControl.Wait(100);
            act.RegisterAsSettler(true);
            TWE3.Gtwi.GameControlFullyDenied = false;
        }
        private void _HumanRunAnim(Actor act)
        {
            Humanoid_mdl A = act.Model as Humanoid_mdl;
            try
            {
                A.ResetState(100);
                while (true)
                {
                    float t = 100.0f / speed / 4.0f;
                    t = t < 120.0f ? 120.0f : t;
                    //A.ResetModelState();

                    switch (A.Orientation)
                    {
                        case Orient.ToUs:
                            #region ToUs
                            A.Head[-24, t].n();
                            A.Body[24, t].n();
                            A.RightLeg[-40, t].n();
                            A.RightFoot[25, t].n();
                            A.LeftLeg[40, t].n();
                            A.LeftFoot[-80, t].n();
                            A.LeftArm[-40, t].n();
                            A.LeftHand[-25, t].n();
                            A.RightHand[+30, t].n();
                            A.RightArm[-50, t].WaitForFinish(t);// правая вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);
                            A.RightFoot[+20, t].WaitForFinish(t); //к дефолту

                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Head[+24, t].n();
                            A.Body[-24, t].n();
                            A.LeftLeg[+40, t].n();
                            A.LeftFoot[-25, t].n();
                            A.RightLeg[-40, t].n();
                            A.RightFoot[+80, t].n();
                            A.RightArm[+60, t].n();
                            A.RightHand[+40, t].n();
                            A.LeftHand[-30, t].n();
                            A.LeftArm[+50, t].WaitForFinish(t);// левая вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);
                            A.LeftFoot[-20, t].WaitForFinish(t); //к дефолту
                            #endregion
                            break;
                        case Orient.ToRight:
                            #region ToRight
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Weapon[+60, t].n();
                            A.Head[-24, t].n();
                            A.Body[24, t].n();
                            A.RightLeg[-60, t].n();
                            A.RightFoot[20, t].n();
                            A.LeftLeg[0, t].n();
                            A.LeftFoot[30, t].n();
                            A.LeftArm[-40, t].n();
                            A.LeftHand[-25, t].n();
                            A.RightArm[40, t].WaitForFinish(t);//правую вперед

                            //A.Position.Ctrl<Anim.V2SmplCtrl>().To.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -100), t * 0.7f);
                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);//к дефолту
                            A.LeftFoot[+50, t].WaitForFinish(t);

                            //A.Position.Ctrl<Anim.V2SmplCtrl>().To.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, +100), t * 0.7f);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Weapon[-20, t].n();
                            A.Head[-24, t].n();
                            A.Body[+24, t].n();
                            A.RightLeg[+20, t].n();
                            A.RightFoot[+20, t].n();
                            A.LeftLeg[-100, t].n();
                            A.LeftFoot[40, t].n();
                            A.LeftArm[+40, t].n();
                            A.LeftHand[-20, t].n();
                            A.RightHand[-25, t].n();
                            A.RightArm[-40, t].WaitForFinish(t);//левую вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);//к дефолту
                            A.RightFoot[+50, t].WaitForFinish(t);
                            #endregion
                            break;
                        case Orient.ToLeft:
                            #region ToLeft
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Weapon[-60, t].n();
                            A.Head[+24, t].n();
                            A.Body[-24, t].n();
                            A.RightLeg[+60, t].n();
                            A.RightFoot[-20, t].n();
                            A.LeftLeg[-30, t].n();
                            A.LeftFoot[-30, t].n();// правую ступню вперед
                            A.LeftArm[+40, t].n();
                            A.LeftHand[+25, t].n();
                            A.RightArm[-40, t].WaitForFinish(t);//правую ногу вперед вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);//к дефолту
                            A.LeftFoot[-50, t].WaitForFinish(t);

                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Weapon[+20, t].n();
                            A.Head[+24, t].n();
                            A.Body[-24, t].n();
                            A.RightLeg[-20, t].n();
                            A.RightFoot[-20, t].n();
                            A.LeftLeg[+100, t].n();
                            A.LeftFoot[-40, t].n();
                            A.LeftArm[-40, t].n();// левую руку в пол оборота
                            A.LeftHand[+20, t].n();
                            A.RightHand[+25, t].n();
                            A.RightArm[+40, t].WaitForFinish(t);//левую вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);//к дефолту
                            A.RightFoot[-50, t].WaitForFinish(t);
                            #endregion
                            break;
                        case Orient.ToForward:
                            #region ToUs
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Head[-24, t].n();// башку пригнуть
                            A.Body[24, t].n();
                            A.RightLeg[-40, t].n();
                            A.RightFoot[25, t].n();
                            A.LeftLeg[40, t].n();
                            A.LeftFoot[-100, t].n();
                            A.LeftArm[-40, t].n();
                            A.LeftHand[-25, t].n();
                            A.RightHand[+10, t].n();
                            A.RightArm[-10, t].WaitForFinish(t);// правая вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);
                            A.RightFoot[+20, t].WaitForFinish(t); //к дефолту

                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.Head[+24, t].n();
                            A.Body[-24, t].n();
                            A.LeftLeg[+40, t].n();
                            A.LeftFoot[-25, t].n();
                            A.RightLeg[-40, t].n();
                            A.RightFoot[+100, t].n();
                            A.RightArm[+5, t].n();
                            A.RightHand[+5, t].n();
                            A.LeftHand[-30, t].n();
                            A.LeftArm[+50, t].WaitForFinish(t);// левая вперед

                            TwiSound.PlayStep(A);
                            t = 100.0f / speed / 4.0f;
                            t = t < 120.0f ? 120.0f : t;
                            A.ResetState(t);
                            A.LeftFoot[-20, t].WaitForFinish(t); //к дефолту
                            #endregion
                            break;
                    }
                }

            }
            catch (ThreadAbortException e)
            {

            }
            finally
            {
                TwiSound.PlayStep(A);
                A.ResetState(100);
            }
        }
        protected override void ScriptAction()
        {
            var act = Owner; var flds = targetField.GoInfo.Track.ToArray();
            var scr = Script.Start<Action<Actor, Life.Maneuver_Uni.FieldGoInfo[]>>(this._HumanRunMove, act, flds);
            if (act.Model.CurrentModelScript != null)
                act.Model.CurrentModelScript.Terminate();
            act.Model.CurrentModelScript = scr;
        }


        // Когда пользователь водит мышкой по полям.
        private List<TwilightObject> traces = new List<TwilightObject>();
        private TwilightObject target = new TwilightObject(new AnimTexture("target"), Global.CellSize);
        void GameTwilight_MouseEnterField(object oldObj, TwilightField newObj)
        {
            if (!AvialableFields.Contains(newObj.GoInfo))
            {
                foreach (var trace in traces)
                    TWGE3.twi.UnregisterObject(trace);
                traces.Clear();
                TWGE3.twi.UnregisterObject(target);
                return;
            }
            //for (int u = 0; u < traces.Count; u++)
            //    TwilightEngine3.Twilight.RegisterObject(traces[u]);
            newObj.GoInfo.Track.Add(newObj.GoInfo);
            int requiredTraceCount = 4 * (newObj.GoInfo.Track.Count + 1);
            int diff = requiredTraceCount - traces.Count;
            if (diff > 0)
                for (int q = 0; q < diff; q++)
                {
                    TwilightObject trace = new TwilightObject(new AnimTexture("trace"), new Vector2(25));
                    trace.Opacity.Value = 0.6f;
                    trace.RGBMulti.Value = Owner.SideColor.ToVector3();
                    trace.Texture.Shader.BlendingOptions = BlendState.AlphaBlend;
                    Misc.UF.PutOn(trace, newObj.GoInfo.Track[0]);
                    traces.Insert(0, trace);
                    trace.Level = Misc.TwilightLayer.ServiceLayer + 0.01f;
                    TWGE3.twi.RegisterObject(trace);
                }
            else
                for (int q = 0; q < -diff; q++)
                {
                    TWGE3.twi.UnregisterObject(traces[0]);
                    traces.RemoveAt(0);
                }

            for (int i = 0; i < newObj.GoInfo.Track.Count - 1; i++)
            {
                traces[i * 4].Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, Misc.UF.PutOnA(traces[i * 4], newObj.GoInfo.Track[i]), 200);
                traces[i * 4 + 1].Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3,
                    Vector2.Lerp(Misc.UF.PutOnA(traces[i * 4], newObj.GoInfo.Track[i]), newObj.GoInfo.Track[i + 1].Center - traces[i * 4].Size.Value / 2.0f, 0.25f), 200);
                traces[i * 4 + 2].Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3,
                    Vector2.Lerp(Misc.UF.PutOnA(traces[i * 4], newObj.GoInfo.Track[i]), newObj.GoInfo.Track[i + 1].Center - traces[i * 4].Size.Value / 2.0f, 0.5f), 200);
                traces[i * 4 + 3].Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3,
                    Vector2.Lerp(Misc.UF.PutOnA(traces[i * 4], newObj.GoInfo.Track[i]), newObj.GoInfo.Track[i + 1].Center - traces[i * 4].Size.Value / 2.0f, 0.75f), 200);
            }

            if (!target.IsRegistered)
            {
                target.Position.Value = newObj.GoInfo.Track[0].Position.Value;
                target.Opacity.Value = 0.6f;
                target.Texture.Shader.BlendingOptions = BlendState.AlphaBlend;
                target.RGBMulti.Value = Owner.SideColor.ToVector3();
                TWGE3.twi.RegisterObject(target);
            }
            target.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, newObj.Position.Value, 200);

            Owner.Model.Orientation = Misc.UF.GetOrient(newObj.GoInfo.Track[0].Position.Value, newObj.GoInfo.Track[1].Position.Value);

            newObj.GoInfo.Track.Remove(newObj.GoInfo);
        }


        // Эта функция вызывается при активизации омнита, aтут необходимо подписаться
        // на все нужные события движка для определения макроактивации скилла.
        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.DragBegin += MacroactionWatcher;
        }

        // При завершении омнифазы необходимо отписать все хандлеры макродействий
        public override void UnregisterSkillWatchers()
        {
            //base.GEOmniPhaseEnd();
            TWE3.Gtwi.DragBegin -= MacroactionWatcher;
        }

        // Метод, вызывемый движком при деактивации скилла (обычно по нажатию ПКМ).
        // Тут необходимо отписать все обработчики настройки скилла.
        public override void Deactivated()
        {
            if (TWE3.Gtwi.ActiveSkill != this) return;
            TWE3.Gtwi.ActiveSkill = null;
            TWE3.Gtwi.MouseEnterField -= GameTwilight_MouseEnterField;
            TWE3.Gtwi.DragEnd -= GameTwilight_DragEnd;
            foreach (var fi in AvialableFields)
            {
                TWE3.twi.UnregisterObject(fi);
                fi.Field.GoInfo = null;
            }
            AvialableFields.Clear();
            foreach (var trace in traces)
                TWGE3.twi.UnregisterObject(trace);
            traces.Clear();
            TWGE3.twi.UnregisterObject(target);

            TWE3.Gtwi.DragBegin += MacroactionWatcher;
        }

        public Maneuver_Uni(Actor owner)
        {
            Owner = owner;
            OmnipointConsumption = 1;
            CoolDown = 1;
            //widget = new SkillWidget("Maneuver", this);
        }
    }

    public abstract class OccupySpring_Base : ActiveSkill
    {
        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.MouseEnterSource += GameTwilight_MouseEnterSource;
            TWE3.Gtwi.MouseUp += GameTwilight_MouseUp;
        }

        protected EnergySource source;
        void GameTwilight_MouseUp(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            TwilightField f = TWGE3.Gtwi.QueryField(pos);
            bool requestToUse = f.Settler != null && f.Settler as EnergySource != null && (f.Settler as EnergySource).OwnerOmnit != TWE3.Gtwi.ActiveOmnit;
            requestToUse &= (f.Center - Owner.Model.GetField().Center).Length() <= Global.Cell_H;

            if (!requestToUse) return;
            if (!CheckAvialabilityToUse()) return;

            source = f.Settler as EnergySource;
            //HumanOccupyScript.HumanOccupy(Owner, es);
            ScriptAction();
            TWE3.twi.UnregisterObject(target);
            PerformConsumption();
            //es.EnergySourceBuilding
        }

        private TwilightObject target = new TwilightObject(new AnimTexture("target"), Global.CellSize);
        void GameTwilight_MouseEnterSource(object oldObj, EnergySource newObj)
        {
            bool requestToUse = (newObj.EnergySourceBuilding.Center - Owner.Model.GetField().Center).Length() <= Global.Cell_H;
            requestToUse &= newObj.OwnerOmnit != Owner.OwnerOmnit;
            if (!requestToUse) return;
            if (!CheckAvialabilityToUse()) return;

            TWE3.twi.RegisterObject(target);
            Owner.Model.Orientation = Misc.UF.GetOrient(Owner.Model.GetField().Center, newObj.EnergySourceBuilding.Center);
            target.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, 0.6f, 100);
            target.Texture.Shader.BlendingOptions = BlendState.AlphaBlend;
            target.RGBMulti.Value = Owner.SideColor.ToVector3();
            Misc.UF.PutOn(target, newObj.EnergySourceBuilding);

        }

        public override void UnregisterSkillWatchers()
        {
            //base.GEOmniPhaseEnd();
            TWE3.Gtwi.MouseEnterSource -= GameTwilight_MouseEnterSource;
            TWE3.Gtwi.MouseUp -= GameTwilight_MouseUp;
            TWE3.twi.UnregisterObject(target);
        }

        public override void Deactivated()
        {
            TWE3.Gtwi.MouseEnterSource -= GameTwilight_MouseEnterSource;
            TWE3.Gtwi.MouseUp -= GameTwilight_MouseUp;
        }

        public OccupySpring_Base(Actor owner)
        {
            Owner = owner;
            OmnipointConsumption = 2;
            ReservoirConsumption = 30;
            widget = new SkillWidget("Occupy", this);
        }
    }

    public class Incarnate_Uni : ActiveSkill
    {
        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.MouseDown += GameTwilight_MouseDown;
        }

        void GameTwilight_MouseDown(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            Incarnation inc = null; TwilightField f = TWE3.Gtwi.QueryField(pos);
            if (f.Settler != null)
                inc = f.Settler as Incarnation;
            if (inc == null) return; if (inc.OwnerOmnit != Owner) return;
            if (inc == TWE3.Gtwi.ActiveActor) return;
            if (inc.AlreadyUsed) { TWE3.Gtwi.GUI.ShowMessage("Воплощение уже было использовано!", Color.LightGray); return; }

            if (!CheckAvialabilityToUse()) return;
            PerformConsumption();
            {
                inc.IncoPhaseStart();
            }
        }

        public override void UnregisterSkillWatchers()
        {
            //base.GEOmniPhaseEnd();
            TWE3.Gtwi.MouseDown -= GameTwilight_MouseDown;
        }

        public Incarnate_Uni(Omnit owner)
        {
            Owner = owner;
            OmnipointConsumption = 0;
            ReservoirConsumption = 0;
            CoolDown = 0;
            CurrentCoolDown = 0;
        }
    }

    public class Create_Extra : ActiveSkill
    {
        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.DragEnd += GameTwilight_DragEnd;
        }

        void GameTwilight_DragEnd(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
            if (mb != MouseButton.RMB) return;
            if (UF.AbsToField(beginPos) != Owner.GetField().FieldPos) return;

            if (!CheckAvialabilityToUse()) return;
            var fb = Owner.GetField();
            var f = TWE3.Gtwi.QueryField(endPos);
            if (!CheckRange(f)) return;
            if (f.Attribute.SurfaceType == MapSystem.FieldSurfaceType.Liquid) return;
            if (f.Settler != null) return;

            #region SkillAction
            ExtraInc newInc = new ExtraInc(Owner.OwnerOmnit as Extral);
            newInc.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet(newInc.SystemName), newInc);
            //soldier.Sintes.AddSkill(new OccupySpring_Base(soldier));
            //newInc.Model.PlaceOnField(f.X - 2 + x * 4, f.Y - 2 + y * 4);
            //newInc.Model.Orientation = UF.GetOrient(newInc.Model.Center, MarcoJiff2.Model.Center);
            newInc.Model.RunIdleBreathScript();
            //
            newInc.Model.PlaceOnField(f.FieldPos.X, f.FieldPos.Y);
            TWE3.twi.RegisterObject(newInc.Model);
            newInc.Model.Orientation = UF.GetOrient(beginPos, endPos);
            newInc.Model.RunIdleBreathScript();
            var scr = Script.Start<Action<ActorModel, TwilightField, ExtraIncarnator>>((mdl, fld, inc) =>
            {
                TWE3.Gtwi.GameControlFullyDenied = true;
                TwiSound.PlaySnd("Throw2");
                mdl.selfGlowing.LightIntensity.Value = 0.0f;
                foreach (var bone in mdl.Bones)
                    bone.Opacity.Value = 0f;
                AnimFloat2 ballPos = new AnimFloat2(inc.IncoCore), ballRad = new AnimFloat2(20, 20);
                LightSource ballGlow = new LightSource(altitude: 10f, lightDistance: 300, lightIntensity: 0f);
                ballGlow.Position.Value = inc.IncoCore;
                TwiParticleSystem ball = new TwiParticleSystem
                    (200, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(ballPos, ballRad) },
                    particleTexName: "rg", minTime: 60, maxTime: 400, at: Misc.Anim.Linear);
                ballGlow.LightColor = ball.RGBMulti;
                TWE3.twi.RegisterObject(ball);
                TWE3.twi.RegisterLightSource(ballGlow);
                ball.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg
                    (Misc.Anim.Linear, inc.Sintes.EnergyColor.ToVector3(), inc.SideColor.ToVector3(), 1000);
                ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.5f, 1f);
                ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f, 1f);
                ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, new Vector2(50), 1000);
                ballGlow.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(Vector2.Lerp(ballPos.Value, fld.Center, 0.5f), fld.Center, 1000);
                ballPos.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(Vector2.Lerp(ballPos.Value, fld.Center, 0.5f) + new Vector2(0, -300), fld.Center, 1000)
                    .WaitForFinish(1200);
                mdl.selfGlowing.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1.0f, 100);
                AnimFloat2 radius = new AnimFloat2(10, 10);
                mdl.Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(Misc.Anim.Bezier3, 50, radius);
                ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
                ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
                TwiSound.PlaySnd("Create1");
                foreach (var bone in mdl.Bones)
                {
                    bone.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, 1f, 200);
                    bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, UF.PutOnA(bone, fld) - mdl.Position.Value, bone.BasePosition.Value, 300)
                        .WaitForFinish(100);
                }
                radius.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(0), 100).WaitForFinish(110);
                TWE3.twi.UnregisterObject(ball);
                TWE3.twi.UnregisterLightSource(ballGlow);
                mdl.PlaceOnField(fld.FieldPos.X, fld.FieldPos.Y);
                TWE3.Gtwi.GameControlFullyDenied = false;
                mdl.Actor.OmniPhaseStart();
            }, newInc.Model, f, Owner as ExtraIncarnator);
            #endregion

            PerformConsumption();
        }

        public Create_Extra(ExtraIncarnator owner)
        {
            Owner = owner;
            CoolDown = 0;
            OmnipointConsumption = 1;
            ReservoirConsumption = 60;
            Range = 3;
        }
    }
    public class Create_Intro : ActiveSkill
    {
        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.DragEnd += GameTwilight_DragEnd;
        }

        void GameTwilight_DragEnd(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
            if (mb != MouseButton.RMB) return;
            if (UF.AbsToField(beginPos) != Owner.GetField().FieldPos) return;

            if (!CheckAvialabilityToUse()) return;
            var fb = Owner.GetField();
            var f = TWE3.Gtwi.QueryField(endPos);
            if (!CheckRange(f)) return;
            if (f.Attribute.SurfaceType == MapSystem.FieldSurfaceType.Liquid) return;
            if (f.Settler != null) return;

            #region SkillAction
            IntroInc newInc = new IntroInc(Owner.OwnerOmnit as Intrin);
            newInc.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet(newInc.SystemName), newInc);
            //soldier.Sintes.AddSkill(new OccupySpring_Base(soldier));
            //newInc.Model.PlaceOnField(f.X - 2 + x * 4, f.Y - 2 + y * 4);
            //newInc.Model.Orientation = UF.GetOrient(newInc.Model.Center, MarcoJiff2.Model.Center);
            newInc.Model.RunIdleBreathScript();
            newInc.Model.PlaceOnField(f.FieldPos.X, f.FieldPos.Y);
            TWE3.twi.RegisterObject(newInc.Model);
            newInc.Model.Orientation = UF.GetOrient(beginPos, endPos);
            newInc.Model.RunIdleBreathScript();
            Owner.Model.Orientation = UF.GetOrient(beginPos, endPos);
            Owner.Model.CurrentModelScript.Terminate();
            var scr = Script.Start<Action<ActorModel, TwilightField, Humanoid_mdl>>((mdl, fld, omniMdl) =>
            {
                //omniMdl.Orientation = UF.GetOrient(beginPos, endPos);
                TWE3.Gtwi.GameControlFullyDenied = true;
                //TwiSound.PlaySnd("Throw2");
                mdl.selfGlowing.LightIntensity.Value = 0.0f;
                foreach (var bone in mdl.Bones)
                    bone.Opacity.Value = 0f;

                AnimFloat2 ballPos = new AnimFloat2((omniMdl as Humanoid_mdl).Weapon.ExtraMarkers[0].Value), ballRad = new AnimFloat2(20, 20);
                LightSource ballGlow = new LightSource(altitude: 10f, lightDistance: 300, lightIntensity: 0f);
                ballGlow.Position.Value = omniMdl.Actor.GetField().Center;
                TwiParticleSystem ball = new TwiParticleSystem
                    (200, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(ballPos, ballRad) },
                    particleTexName: "rg", minTime: 60, maxTime: 400, at: Misc.Anim.Linear);
                ballGlow.LightColor = ball.RGBMulti;
                TWE3.twi.RegisterObject(ball);
                TWE3.twi.RegisterLightSource(ballGlow);
                ball.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg
                    (Misc.Anim.Linear, (omniMdl.Actor as Intrin).Sintes.EnergyColor.ToVector3(), omniMdl.Actor.SideColor.ToVector3(), 1000);
                ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.5f, 1f);
                ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f, 1f);
                ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, new Vector2(50), 1000);
                
                float t = 600, t2 = 150;
                ballPos.Ctrl<Anim.V2SnapToCtrl>().Cfg(omniMdl.Weapon.ExtraMarkers[0]);
                Vector2 d1 = Vector2.Zero;
                switch (omniMdl.Orientation)
                {
                    case Orient.ToUs:
                        #region Us
                        d1 = new Vector2(200);
                        omniMdl.Body[-35, t].n();
                        omniMdl.LeftArm[-35, t].n();
                        omniMdl.LeftHand[-45, t].n();
                        omniMdl.RightArm[+50, t].n();
                        omniMdl.RightHand[-45, t].n();
                        omniMdl.LeftLeg[+40, t].n();
                        omniMdl.LeftFoot[-10, t].n();
                        omniMdl.RightLeg[-30, t].n();
                        omniMdl.RightFoot[+70, t].n();
                        omniMdl.Head[45, t].WaitForFinish(t);

                        TwiSound.PlaySnd("Throw2");
                        omniMdl.ResetState(t2);
                        omniMdl.Body[+35, t2].n();
                        omniMdl.LeftArm[+25, t2].n();
                        omniMdl.LeftHand[+50, t2].n();
                        omniMdl.RightArm[-60, t2].n();
                        omniMdl.RightHand[-30, t2].n();
                        omniMdl.LeftLeg[+20, t2].n();
                        omniMdl.LeftFoot[-60, t2].n();
                        omniMdl.RightLeg[-40, t2].n();
                        omniMdl.RightFoot[-10, t2].n();
                        omniMdl.Head[-45, t2].WaitForFinish(t2/2);
                        #endregion
                        break;
                    case Orient.ToRight:
                        #region Right
                        d1 = new Vector2(200);
                        omniMdl.Body[-35, t].n();
                        omniMdl.LeftArm[-35, t].n();
                        omniMdl.LeftHand[-45, t].n();
                        omniMdl.RightArm[+50, t].n();
                        omniMdl.RightHand[-45, t].n();
                        omniMdl.LeftLeg[+25, t].n();
                        omniMdl.LeftFoot[+30, t].n();
                        omniMdl.RightLeg[-30, t].n();
                        omniMdl.RightFoot[+70, t].n();
                        omniMdl.Head[45, t].WaitForFinish(t);

                        TwiSound.PlaySnd("Throw2");
                        omniMdl.ResetState(t2);
                        omniMdl.Body[+35, t2].n();
                        omniMdl.LeftArm[+25, t2].n();
                        omniMdl.LeftHand[+50, t2].n();
                        omniMdl.RightArm[-60, t2].n();
                        omniMdl.RightHand[-30, t2].n();
                        omniMdl.LeftLeg[-40, t2].n();
                        omniMdl.LeftFoot[+80, t2].n();
                        omniMdl.RightLeg[-40, t2].n();
                        omniMdl.RightFoot[-10, t2].n();
                        omniMdl.Head[-45, t2].WaitForFinish(t2/2);
                        #endregion
                        break;
                    case Orient.ToLeft:
                        #region Left
                        d1 = new Vector2(-200, -200);
                        omniMdl.Body[+35, t].n();
                        omniMdl.LeftArm[+35, t].n();
                        omniMdl.LeftHand[+45, t].n();
                        omniMdl.RightArm[-60, t].n();
                        omniMdl.RightHand[-20, t].n();
                        omniMdl.LeftLeg[-40, t].n();
                        omniMdl.LeftFoot[-20, t].n();
                        omniMdl.RightLeg[+60, t].n();
                        omniMdl.RightFoot[-90, t].n();
                        omniMdl.Head[-45, t].WaitForFinish(t);

                        TwiSound.PlaySnd("Throw2");
                        omniMdl.ResetState(t2);
                        omniMdl.Body[-35, t2].n();
                        omniMdl.LeftArm[-25, t2].n();
                        omniMdl.LeftHand[-50, t2].n();
                        omniMdl.RightArm[+40, t2].n();
                        omniMdl.RightHand[+20, t2].n();
                        omniMdl.LeftLeg[+40, t2].n();
                        omniMdl.LeftFoot[-80, t2].n();
                        omniMdl.RightLeg[+40, t2].n();
                        omniMdl.RightFoot[+10, t2].n();
                        omniMdl.Head[+45, t2].WaitForFinish(t2/2);
                        #endregion
                        break;
                    case Orient.ToForward:
                        #region Forward
                        d1 = new Vector2(-200, -200);
                        omniMdl.Body[+35, t].n();
                        omniMdl.RightArm[+35, t].n();
                        omniMdl.RightHand[+45, t].n();
                        omniMdl.LeftArm[-50, t].n();
                        omniMdl.LeftHand[+45, t].n();
                        omniMdl.RightLeg[-40, t].n();
                        omniMdl.RightFoot[+10, t].n();
                        omniMdl.LeftLeg[+30, t].n();
                        omniMdl.LeftFoot[-70, t].n();
                        omniMdl.Head[-45, t].WaitForFinish(t);

                        TwiSound.PlaySnd("Throw2");
                        omniMdl.ResetState(t2);
                        omniMdl.Body[-35, t2].n();
                        omniMdl.RightArm[-25, t2].n();
                        omniMdl.RightHand[-50, t2].n();
                        omniMdl.LeftArm[+60, t2].n();
                        omniMdl.LeftHand[+30, t2].n();
                        omniMdl.RightLeg[-20, t2].n();
                        omniMdl.RightFoot[+60, t2].n();
                        omniMdl.LeftLeg[+40, t2].n();
                        omniMdl.LeftFoot[+10, t2].n();
                        omniMdl.Head[+45, t2].WaitForFinish(t/2);
                        #endregion
                        break;
                }
                //ballPos.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(Vector2.Lerp(ballPos.Value, fld.Center, 0.5f) + new Vector2(0, -300), fld.Center, 1000)
                //    .WaitForFinish(1200);
                ballGlow.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(Vector2.Lerp(ballPos.Value, fld.Center, 0.5f), fld.Center, 1200);
                ballPos.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(ballPos.Value + d1, fld.Center, 1200).WaitForFinish(400);
                omniMdl.ResetState(1000, Misc.Anim.Bezier3);
                omniMdl.RunIdleBreathScript();
                ScriptControl.Wait(700);
                mdl.selfGlowing.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1.0f, 100);
                AnimFloat2 radius = new AnimFloat2(10, 10);
                mdl.Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(Misc.Anim.Bezier3, 50, radius);
                ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
                ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, mdl.Bones.Length * 100 + 500);
                TwiSound.PlaySnd("Create1");
                foreach (var bone in mdl.Bones)
                {
                    bone.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, 1f, 200);
                    bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Linear, UF.PutOnA(bone, fld) - mdl.Position.Value, bone.BasePosition.Value, 300)
                        .WaitForFinish(100);
                }
                radius.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(0), 100).WaitForFinish(110);
                TWE3.twi.UnregisterObject(ball);
                TWE3.twi.UnregisterLightSource(ballGlow);
                mdl.PlaceOnField(fld.FieldPos.X, fld.FieldPos.Y);
                TWE3.Gtwi.GameControlFullyDenied = false;
                mdl.Actor.OmniPhaseStart();
            }, newInc.Model, f, Owner.Model as Humanoid_mdl);
            #endregion

            PerformConsumption();
        }

        public override void UnregisterSkillWatchers()
        {
            TWE3.Gtwi.DragEnd -= GameTwilight_DragEnd;
        }

        public Create_Intro(Intrin owner)
        {
            Owner = owner;
            CoolDown = 0;
            OmnipointConsumption = 1;
            ReservoirConsumption = 20;
            Range = 3;
        }

    }
    public abstract class AttackClose_ExtraBase : ActiveSkill
    {
        public int MinConsumption = 5;
        public int Damage;

        protected Actor enemy;
        public AttackClose_ExtraBase(Actor owner)
        {
            Owner = owner;
            Range = 1;
            OmnipointConsumption = 1;
            ReservoirConsumption = MinConsumption;
        }

        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.MouseUp += Gtwi_MouseUp;
        }

        private void Gtwi_MouseUp(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            var f = TWE3.Gtwi.QueryField(pos);
            if (f.Settler == null) return;
            if (!(f.Settler is Actor)) return;
            var e = f.Settler as Actor;
            if (e.OwnerOmnit == Owner.OwnerOmnit) return;
            enemy = e;

            if (!CheckRange(f)) return;
            if (!CheckAvialabilityToUse()) return;

            var model = Owner.Model as ActorModels.Humanoid_mdl;
            model.Orientation = UF.GetOrient(Owner.GetField().Center, f.Center);
            PerformConsumption();
            ScriptAction();
        }

        public override void UnregisterSkillWatchers()
        {
            TWE3.Gtwi.MouseUp -= Gtwi_MouseUp;
        }
    }
    public abstract class AttackDistance_IntroBase : ActiveSkill
    {
        public int MinConsumption = 20;
        private int damage;

        public int Damage
        {
            get { return damage + 20 * Owner.GameLevel.Current; }
            set { damage = value; }
        }


        protected Actor enemy;

        TextWidget text = new TextWidget("0", "Jing Jing Points", Color.White);

        public override void RegisterSkillWatchers()
        {
            TWE3.Gtwi.DragBegin += GameTwilight_DragBegin;
            //TWE3.Gtwi.MouseUp += Gtwi_MouseUp;
        }

        void Gtwi_MouseUp(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            TWGE3.Gtwi.ActiveSkill = null;
            var f = TWGE3.Gtwi.QueryField(pos);
            if (f.Settler == null) return;
            Actor a = f.Settler as Actor;
            if ((Owner.GetField().Position.Value / new Vector2(Global.Cell_H, Global.Cell_V)
                - f.Position.Value / new Vector2(Global.Cell_H, Global.Cell_V)).Length() > Range)
                return;
            if (a == null) return;
            if (a.OwnerOmnit == Owner.OwnerOmnit) return;

            if (!CheckAvialabilityToUse()) return;

            enemy = a;

            PerformConsumption();
            ScriptAction();
            ReservoirConsumption = MinConsumption;
        }

        void GameTwilight_DragBegin(MouseButton mb, Vector2 pos)
        {
            if (mb != MouseButton.LMB) return;
            var f = TWE3.Gtwi.QueryField(pos);
            if (f.Settler == null) return;
            Actor a = f.Settler as Actor;
            if (a == null) return;
            if (a.OwnerOmnit == Owner.OwnerOmnit) return;

            if (!CheckRange(f)) return;
            if (!CheckAvialabilityToUse()) return;


            var model = Owner.Model as ActorModels.Humanoid_mdl;
            model.Orientation = UF.GetOrient(Owner.GetField().Center, f.Center);
            TWE3.Gtwi.ActivateSkill(this);
            TWE3.Gtwi.DragEnd += GameTwilight_DragEnd;
            TWE3.Gtwi.DragMove += GameTwilight_DragMove;
            TWE3.twi.RegisterWidget(text);
        }

        void GameTwilight_DragMove(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
            int minValue = MinConsumption;
            int maxValue = Reservoir.AvialablePhaseConsumption;
            text.Position.Value = endPos - TWE3.twi.Camera.Position.Value + new Vector2(0, -30);
            int val = (int)(endPos - beginPos).Length(); val = (int)MathHelper.Lerp(minValue, maxValue, (float)val / (float)Global.Cell_H / 2f);
            val = Math.Min(val, maxValue);
            text.Text = val.ToString();
            //ReservoirConsumption = val;
        }
        void GameTwilight_DragEnd(MouseButton mb, Vector2 beginPos, Vector2 endPos)
        {
            TWE3.twi.UnregisterWidget(text);
            TWE3.Gtwi.DragEnd -= GameTwilight_DragEnd;
            TWE3.Gtwi.DragMove -= GameTwilight_DragMove;
            TWE3.Gtwi.ActiveSkill = null;
            enemy = TWE3.Gtwi.QueryField(beginPos).Settler as Actor;
            PerformConsumption();
            ScriptAction();
            ReservoirConsumption = MinConsumption;
        }

        public override void UnregisterSkillWatchers()
        {
            TWE3.Gtwi.DragBegin -= GameTwilight_DragBegin;
            //TWE3.Gtwi.MouseUp -= Gtwi_MouseUp;
        }

        public AttackDistance_IntroBase(Intrin owner)
        {
            Owner = owner;
            Range = 6;
            OmnipointConsumption = 1;
            ReservoirConsumption = MinConsumption;
        }
    }

    public class DefaultDefence : DefensiveSkill
    {
        public DefaultDefence(Actor owner)
        {
            Owner = owner;
            OmnipointsReserve = 0;
        }
        protected override void DefenceAction()
        {
            var mdl = Owner.Model as Humanoid_mdl;
            if (mdl.CurrentModelScript != null) mdl.CurrentModelScript.Terminate();
            var scr = Script.Start<Action>(() =>
                {
                    float t = TimeToStrike / 2;
                    ScriptControl.Wait(t);
                    switch (mdl.Orientation)
                    {
                        case Orient.ToUs:
                            break;
                        case Orient.ToRight:
                            mdl.Body[-30, t].n();
                            mdl.RightArm[-30, t].n();
                            mdl.RightHand[-30, t].n();
                            mdl.LeftArm[-50, t].n();
                            mdl.LeftHand[-60, t].n();
                            mdl.RightLeg[-10, t].n();
                            mdl.RightFoot[+90, t].n();
                            mdl.LeftLeg[+30, t].n();
                            mdl.Head[+45, t].n();
                            mdl.Weapon[+100, t].WaitForFinish(t);
                            break;
                        case Orient.ToLeft:
                            mdl.Body[+30, t].n();
                            mdl.LeftArm[+30, t].n();
                            mdl.LeftHand[+30, t].n();
                            mdl.RightArm[50, t].n();
                            mdl.RightHand[60, t].n();
                            mdl.LeftLeg[+10, t].n();
                            mdl.LeftFoot[-90, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.Head[-45, t].n();
                            mdl.Weapon[-100, t].WaitForFinish(t);
                            break;
                        case Orient.ToForward:
                            break;
                    }
                    TwiSound.PlaySnd("Push1");
                    mdl.Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(Misc.Anim.Linear, t / 12, new Vector2(12));
                    ScriptControl.Wait(t);
                    mdl.Position.TerminateAnimation();
                    mdl.ResetState(t);
                    PerformConsumption();
                });
        }
        protected override void DestructionAction()
        {
            TWGE3.Gtwi.GameControlFullyDenied = true;
            var mdl = Owner.Model as Humanoid_mdl;
            if (mdl.CurrentModelScript != null) mdl.CurrentModelScript.Terminate();
            var scr = Script.Start<Action>(() =>
            {
                #region ActionScript
                {
                    ScriptControl.Wait(this.TimeToStrike);
                    float t = 200, t0 = 200;
                    //mdl.Orientation = Orient.ToRight;
                    var att = Misc.UF.GetOrient(mdl.Center, Attacker.Model.Center);
                    TwiSound.PlaySnd("Push1");
                    switch (att)
                    {
                        case Orient.ToForward:
                        case Orient.ToRight:
                            mdl.LeftLeg[30, t0 / 2, Misc.Anim.Linear].n();
                            mdl.Body[-20, t0 / 2, Misc.Anim.Linear].WaitForFinish(t0 / 2);
                            break;
                        case Orient.ToLeft:
                        case Orient.ToUs:
                            mdl.RightLeg[-30, t0 / 2, Misc.Anim.Linear].n();
                            mdl.Body[20, t0 / 2, Misc.Anim.Linear].WaitForFinish(t0 / 2);
                            break;
                    }
                    mdl.ResetState(t0, Misc.Anim.Linear).WaitForFinish(t0 / 2); t0 = 400;
                    int n = 3;
                    mdl.selfGlowing.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 150, t0 * n);
                    mdl.Shadow.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector3(0), t0 * n);

                    for (int i = 0; i < n; i++)
                    {
                        //t0 = 1200 / (i+1);
                        int a = i * ((i % 2 == 0) ? (-1) : (+1));
                        mdl.Body[6 * a, t0, Misc.Anim.Bezier3].n();
                        mdl.LeftLeg[-6 * a, t0, Misc.Anim.Bezier3].n();
                        mdl.RightLeg[-7 * a, t0, Misc.Anim.Bezier3].n();
                        mdl.RightArm[-10 * a, t0, Misc.Anim.Bezier3].n();
                        mdl.LeftArm[-12 * a, t0, Misc.Anim.Bezier3].WaitForFinish(t0);
                        //mdl.ResetState(t0, Misc.Anim.Bezier3).WaitForFinish(t0);
                    }
                    Script<Action<ScriptFireBall.FireBallParams>> scrfm = null;
                    for (int i = 0; i < 5; i++)
                    {
                        scrfm = ScriptFireBall.Start(new ScriptFireBall.FireBallParams
                        {
                            FromPosition = (mdl as Humanoid_mdl).Center + new Vector2(0, +50),// + new Vector2(Global.rnd.Next(-10, 10), Global.rnd.Next(-10, 10)),
                            ToPosition = mdl.Center + new Vector2(Global.rnd.Next(-250, 250), Global.rnd.Next(-100, 100)),
                            TossHeight = 350,
                            FromColor = Color.White.ToVector3(),
                            ToColor = mdl.Actor.SideColor.ToVector3(),
                            StartSize = new Vector2(10, 10),
                            EndSize = new AnimFloat2(40, 40),
                            Time = 1000
                        });
                    }
                    TwiSound.PlaySnd("Expl1");
                    AnimFloat2 explRad = new AnimFloat2(400, 400);
                    TwiParticleSystem expl = new TwiParticleSystem
                (1000, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(new AnimFloat2(mdl.Center + new Vector2(0, 35)), explRad) },
                particleTexName: "rg", minTime: 900, maxTime: 1000, at: Misc.Anim.Bezier3, parSize: 30);
                    TWE3.twi.RegisterObject(expl);
                    expl.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Linear, Color.White.ToVector3(), mdl.Actor.SideColor.ToVector3(), 900);
                    expl.PullInsideAllParticles();
                    expl.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0f, 0.2f, t);
                    // Faling Down;
                    mdl.selfGlowing.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0, t);
                    mdl.Body.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Linear, new Vector2(-20, 30), t);
                    mdl.Body[+90, t].n();
                    mdl.Weapon[+70, t].n();
                    mdl.RightArm[-180, t].n();
                    mdl.LeftArm[-90, t].n(); mdl.LeftHand[-40, t].n();
                    mdl.Head[-50, t].n();
                    mdl.RightLeg[+30, t].n();
                    mdl.LeftLeg[-30, t].WaitForFinish(t);
                    //
                    Attacker.GetExperience(Owner);
                    //
                    expl.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0f, 900);
                    var fld = mdl.GetField();
                    AnimFloat2 radius = new AnimFloat2(0, 50);
                    //mdl.Body.Position.Ctrl<Anim.V2RandomdCtrl>().Cfg(Misc.Anim.Bezier3, 50, radius);
                    mdl.Shadow.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0, 1000);
                    foreach (var bone in mdl.Bones)
                    {
                        bone.Texture.Shader.BlendingOptions = BlendState.Additive;
                        bone.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f, 0f, 1000);
                        bone.BasePosition.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, Vector2.Zero, 500);
                        bone.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, UF.PutOnA(bone, fld), 1000);
                    }
                    foreach (var bone in mdl.Bones)
                    {
                        bone.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, UF.PutOnA(bone, fld) + new Vector2(0, -Global.Cell_H * 0.2f), 1000)
                            .WaitForFinish(150);
                    }
                    //radius.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(0), 500).WaitForFinish(600);
                    fld.Settler = null;
                    TWGE3.Gtwi.UnregisterObject(mdl);
                    TWE3.twi.UnregisterObject(expl);
                    //ScriptControl.WaitForScript(scrf);
                    if (mdl.Actor is Incarnation) mdl.Actor.OwnerOmnit.UnregisterIncarnation(mdl.Actor as Incarnation);
                    else
                    {
                        TWGE3.Gtwi.GUI.ShowMessage(mdl.Actor.OwnerOmnit.FriendlyName + " поражен! Реинкарнация невозможна! Жопа." + TWE3.twi.ScreenSize.ToString(), Color.Red);
                        ScriptControl.Wait(1000);
                        TwilightObject bl = new TwilightObject(new AnimTexture("ShaderMaps\\solid"), TWE3.twi.Camera.Size.Value * 1.5f, Color.Black);
                        bl.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0, 1, 4000);
                        Misc.UF.PutOn(bl, TWE3.twi.Camera);
                        //bl.RestoreLocationOrigin = true;
                        //bl.Position.Value = -TWE3.twi.Camera.Position.Value;
                        bl.Level = TwilightLayer.WidgetLayer - 10f;
                        TWE3.twi.RegisterObject(bl);
                        TWGE3.Gtwi.GUI.HidePanel();
                        ScriptControl.Wait(4000);
                        XNAPlatform.Instance.Exit();
                    }
                    ScriptControl.Wait(2000);
                }
                #endregion
                TWGE3.Gtwi.GameControlFullyDenied = false;
            });
        }
    }
}