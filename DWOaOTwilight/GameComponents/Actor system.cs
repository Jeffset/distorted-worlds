﻿using DWOaO.ActorModels;
using DWOaO.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DWOaO
{
    [Serializable]
    public abstract class Actor : DistortedGameComponent
    {

        public Omnit OwnerOmnit
        {
            get
            {
                return (this as Incarnation != null) ? (OwnerComponent as Omnit) : (this as Omnit);
            }
            set { OwnerComponent = value; }
        }

        private ActorModel _model;
        public ActorModel Model
        {
            get { return _model; }
            set { _model = value; if (this is IntroInc) _model.IsEtherial = true; }
        }

        public CharacterWidget widget;

        public Color SideColor;
        public string SystemName;
        public string FriendlyName;

        public float RunSpeed;

        public DefensiveSkill CurrentDefence { get; set; }

        public EnergyReservoir DefaultSkills { get; protected set; }
        public List<EnergyReservoir> Reservoirs = new List<EnergyReservoir>();
        public EnergyReservoir Stasis { get; protected set; }
        public EnergyReservoir Chaos { get; protected set; }

        public Level GameLevel;
        public OmniPoints Omnipoints;


        private void InitDefaultSkills()
        {
            DefaultSkills = new EnergyReservoir(Color.Gray, "DEFAULT", this, -1);
            DefaultSkills.AddSkill(new Life.Maneuver_Uni(this));
            //
            Stasis.AddSkill(new Life.DefaultDefence(this));
        }

        public Actor()
        {
            Stasis = new EnergyReservoir(Color.LimeGreen, "STASIS", this, 1);
            Chaos = new EnergyReservoir(Color.OrangeRed, "CHAOS", this, 2);
            Reservoirs.AddRange(new EnergyReservoir[] { Stasis, Chaos });
            Stasis.MaxPhaseConsumption = ReservoirWidget.NOPHASELIMIT;
            InitDefaultSkills();
            _components.Add(DefaultSkills);
            _components.AddRange(Reservoirs);
            var widgetLevel = new NumberWidget("Jing Jing Level", 60, this, Color.Yellow);
            //
            GameLevel = new Level(1, widgetLevel);
            //
            RunSpeed = 0.3f;
            //
        }

        public bool IsEnoughOmnipoints(int quantity)
        {
            return Omnipoints.Value - quantity >= 0;
        }

        public void ActivateDefaultDefence()
        {
            (Stasis.ReservoirSkills[0] as DefensiveSkill).ActivateDefence();
        }

        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            (Stasis.ReservoirSkills[0] as DefensiveSkill).ActivateDefence();
            //TwilightEngine3.GameTwilight.ActiveActor = this;
        }
        public override void OmniPhaseEnd()
        {
            base.OmniPhaseEnd();
            //TwilightEngine3.GameTwilight.GUI.HidePanel();
            VisualSelect(false);
        }

        public virtual void VisualSelect(bool select)
        {
            if (select)
            {
                Model.Shadow.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1.0f, 1000);
                Model.selfGlowing.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 3.0f * Global.Cell_H, 1000);
            }
            else
            {
                Model.Shadow.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.5f, 1000);
                Model.selfGlowing.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 2f * Global.Cell_H, 1000);
            }
        }

        public void RegisterAsSettler(bool register)
        {
            Point f = Misc.UF.AbsToField(new Vector2(Model.Center.X, Model.Bottom - 2));
            TWE3.Gtwi.GameMap.FieldMatrix[f.X, f.Y].Settler = register ? this : null;
        }
        public virtual TwilightField GetField()
        { return Model.GetField(); }

        public void GetExperience(Actor destructedEnemy)
        {
            if (destructedEnemy is Incarnation)
            {
                int exp = 50;
                int old = GameLevel.Current;
                GameLevel.EXP += exp;
                //AnimFloat2 ballPos = destructedEnemy.Model.Center;
                //AnimFloat2 ballRad = 
                //TwiParticleSystem expVisual = new TwiParticleSystem(500, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(ballPos, ballRad) }
                
                {
                    string text = "EXP +" + exp.ToString();
                    if (old + 1 == GameLevel.Current)
                    {
                        text += "\n Level " + GameLevel.Current;
                        Script.Start<Action>(() =>
                        {
                            TwilightObject t = new TwilightObject(new AnimTexture("LevelUp"), new Vector2(Global.Cell_H, 0));
                            t.Texture.Shader.BlendingOptions = BlendState.Additive;
                            t.RGBMulti.Value = Color.Yellow.ToVector3();
                            t.Opacity.Value = 0.7f;
                            t.Level = 228;
                            TWE3.twi.RegisterObject(t);
                            UF.PutOn(t, this.Model);
                            t.Size.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, Global.Cell_V * 4), 1000);
                            t.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -Global.Cell_V * 2), 1000).WaitForFinish(1000);
                            TWE3.twi.UnregisterObject(t);
                        });
                    }
                    TextWidget tw = new TextWidget(text, "Jing Jing 14", Color.Yellow);
                    tw.UseGlobalCood = true;
                    UF.PutOn(tw, Model);
                    //{ tw.X = OwnerActor.Model.Right; tw.Y = OwnerActor.Model.Bottom; }
                    //tw.Y -= 50 * GraphicIndicatorOffset;
                    Script.Start<Action<TextWidget>>(ind =>
                    {
                        TWE3.twi.RegisterWidget(ind);
                        ind.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0, 1, 150);
                        ind.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -100), 3000).WaitForFinish(3000);
                        TWE3.twi.UnregisterWidget(ind);
                    }, tw);
                }
            }
        }
    }

    [Serializable]
    public abstract class Omnit : Actor
    {
        public List<Type> GlobalSkills = new List<Type>();

        public List<EnergySource> EnergySupply = new List<EnergySource>();

        public Omnit()
        {
            GameLevel.Max = 20;
            var widgetOmnipoints = new NumberWidget("Jing Jing Points", 50, this, Color.White);
            Omnipoints = new OmniPoints(widgetOmnipoints);
        }
        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            Omnipoints.Value = GsysCfg.INITIAL_OMNIPOINTS + GameLevel.Current;
            TWE3.Gtwi.ActiveActor = this;
            TwiSound.SysSpeak(SystemName, "IncoPhaseStart");
            TWE3.Gtwi.GUI.ShowPanel(this);
            VisualSelect(true);
            TWE3.twi.Camera.MoveCameraOnObject(this.Model);
            TWE3.Gtwi.GUI.ShowMessage(FriendlyName + " начинает омнифазу...", SideColor, 2);
        }
        public override void IncoPhaseEnd()
        {
            TWE3.Gtwi.ActiveActor = this;
            foreach (var r in Reservoirs)
                foreach (var s in r.ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).RegisterSkillWatchers();
            foreach (var s in DefaultSkills.ReservoirSkills)
                if (s as ActiveSkill != null && !(s is Life.Incarnate_Uni)) (s as ActiveSkill).RegisterSkillWatchers();
            TWE3.Gtwi.GUI.ShowPanel(this);
            VisualSelect(true);
            //TwilightEngine3.Twilight.Camera.MoveCameraOnObject(this.Model);
        }
        public override void IncoPhaseStart()
        {
            foreach (var r in Reservoirs)
                foreach (var s in r.ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).UnregisterSkillWatchers();
            foreach (var s in DefaultSkills.ReservoirSkills)
                if (s as ActiveSkill != null && !(s is Life.Incarnate_Uni)) (s as ActiveSkill).UnregisterSkillWatchers();
            VisualSelect(false);
        }

        public void RegisterIncarnation(Incarnation inc)
        {
            _components.Add(inc);
            inc.OwnerOmnit = this;
            inc.Omnipoints = this.Omnipoints;
        }
        public void UnregisterIncarnation(Incarnation inc)
        {
            _components.Remove(inc);
        }
    }

    [Serializable]
    public class Incarnation : Actor
    {
        public bool AlreadyUsed { get; set; }

        public Incarnation(Omnit owner)
        {
            OwnerOmnit = owner;

            SystemName = owner.SystemName + "_Inc";
            SideColor = owner.SideColor;

            GameLevel.Max = 5;

            Stasis.MaxPhaseConsumption = 10000000;

            owner.RegisterIncarnation(this);
            widget = new CharacterWidget(this);
        }
        public override void OmniPhaseStart()
        {
            base.OmniPhaseStart();
            AlreadyUsed = false;
        }
        public override void IncoPhaseStart()
        {
            AlreadyUsed = true;
            if (TWE3.Gtwi.ActiveActor as Incarnation != null)
                TWE3.Gtwi.ActiveActor.IncoPhaseEnd();
            OwnerComponent.IncoPhaseStart();
            foreach (var r in Reservoirs)
                foreach (var s in r.ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).RegisterSkillWatchers();
            foreach (var s in DefaultSkills.ReservoirSkills)
                if (s as ActiveSkill != null) (s as ActiveSkill).RegisterSkillWatchers();
            TWGE3.Gtwi.ActiveActor = this;
            TWE3.Gtwi.GUI.ShowPanel(this);
            TWE3.Gtwi.ActiveActor = this;
            VisualSelect(true);
        }
        public override void IncoPhaseEnd()
        {
            OwnerOmnit.IncoPhaseEnd();
            foreach (var r in Reservoirs)
                foreach (var s in r.ReservoirSkills)
                    if (s as ActiveSkill != null) (s as ActiveSkill).UnregisterSkillWatchers();
            foreach (var s in DefaultSkills.ReservoirSkills)
                if (s as ActiveSkill != null) (s as ActiveSkill).UnregisterSkillWatchers();
            VisualSelect(false);
        }
    }

    [Serializable]
    public class ExtraInc : Incarnation
    {
        public EnergyReservoir Sintes { get; protected set; }

        public ExtraInc(Extral owner)
            : base(owner)
        {
            Sintes = new EnergyReservoir(Color.Cyan, "SINTES", this, 0);
            Reservoirs.Add(Sintes);
            _components.Add(Sintes);
            //
            Stasis.Capacity = 160;
            Stasis.MakeFull();
            //
            Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet(this.SystemName), this);
            Sintes.AddSkill(new Life.OccupySpring_ExInc(this));
            Sintes.Capacity = 120;
            Sintes.CurrentValue = 120;
            Sintes.MaxPhaseConsumption = 30;
            Model.RunIdleBreathScript();
            Chaos.Capacity = 120;
            Chaos.MakeFull();
            Chaos.MaxPhaseConsumption = 40;
            Chaos.AddSkill(new Life.AttackClose_ExtraInc(this));
        }

        public override void OmniPhaseStart()
        {
            var gcs = TWE3.Gtwi.QueryAnyGameElemsAround(GetField(), GVAL.EXTRAINC_STASIS_REGEN_RANGE, typeof(EnergySource));
            int power = 0;
            foreach (var es in gcs)
                if (OwnerOmnit.EnergySupply.Contains(es as EnergySource)) power++;
            Stasis.RegenRate = GVAL.EXTRAINC_STASIS_REGEN_RATE_BASE * power;
            base.OmniPhaseStart();
        }
    }
    [Serializable]
    public class IntroInc : Incarnation
    {
        public IntroInc(Intrin owner)
            : base(owner)
        {
            this.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet(this.SystemName), this);
            this.Model.Orientation = ActorModels.Orient.ToUs;//UF.GetOrient(this.Model.Center, MarcoJiff2.Model.Center);
            this.Model.RunIdleBreathScript();
            //
            Stasis.Capacity = 120;
            Stasis.MakeFull();
            //
            this.Chaos.Capacity = 120;
            this.Chaos.MakeFull();
            this.Chaos.MaxPhaseConsumption = 40;
            this.Chaos.AddSkill(new Life.AttackClose_IntroInc(this));
        }

        public override void OmniPhaseStart()
        {

            Stasis.RegenRate = OwnerOmnit.EnergySupply.Count * GVAL.INTRAINC_STASIS_REGEN_RATE_BASE;
            base.OmniPhaseStart();
        }
    }
    [Serializable]
    public class Intrin : Omnit
    {
        public EnergyReservoir Sintes { get; protected set; }

        public Intrin(string systemName, string friendlyName, Color sideColor, int chaosCapacity, int stasisCapacity, int sintesCapacity,
            int chaosMaxPhase, int chaosRegen, int sintesMaxPhase)
        {
            Sintes = new EnergyReservoir(Color.Cyan, "SINTES", this, 0);
            Reservoirs.Add(Sintes);
            _components.Add(Sintes);

            SystemName = systemName;
            FriendlyName = friendlyName;
            SideColor = sideColor;
            Chaos.Capacity = chaosCapacity;
            Chaos.CurrentValue = chaosCapacity;
            Chaos.MaxPhaseConsumption = chaosMaxPhase;
            Chaos.RegenRate = chaosRegen;
            Stasis.Capacity = stasisCapacity;
            Stasis.CurrentValue = stasisCapacity;
            Sintes.Capacity = sintesCapacity;
            Sintes.CurrentValue = sintesCapacity;
            Sintes.MaxPhaseConsumption = sintesMaxPhase;
            widget = new CharacterWidget(this);
        }
        public override void OmniPhaseStart()
        {
            Sintes.RegenRate = EnergySupply.Count * GVAL.INTRIN_SINTES_REGEN_RATE_BASE;
            Stasis.RegenRate = EnergySupply.Count * GVAL.INTRIN_STASIS_REGEN_RATE_BASE;
            base.OmniPhaseStart();
        }
    }
    [Serializable]
    public class Extral : Omnit
    {
        public override void OmniPhaseStart()
        {
            var gcs = TWE3.Gtwi.QueryAnyGameElemsAround(GetField(), GVAL.EXTRAL_STASIS_REGEN_RANGE, typeof(EnergySource));
            int power = 0;
            foreach (var es in gcs)
                if (EnergySupply.Contains(es as EnergySource)) power++;
            Stasis.RegenRate = GVAL.EXTRAL_STASIS_REGEN_RATE_BASE * power;
            //Stasis.RegenRate
            base.OmniPhaseStart();
        }
        public Extral(string systemName, string friendlyName, Color sideColor, int chaosCapacity, int stasisCapacity, int chaosMaxPhase,
            int chaosRegen)
        {
            SystemName = systemName;
            FriendlyName = friendlyName;
            SideColor = sideColor;
            Chaos.Capacity = chaosCapacity;
            Chaos.CurrentValue = chaosCapacity;
            Chaos.MaxPhaseConsumption = chaosMaxPhase;
            Chaos.RegenRate = chaosRegen;
            Stasis.Capacity = stasisCapacity;
            Stasis.CurrentValue = stasisCapacity;
            widget = new CharacterWidget(this);
        }
    }

    [Serializable]
    public class ExtraIncarnator : ExtraInc
    {
        private TwilightField field;

        public TwilightObject VisualIncarnator;
        public LightSource Ambient;
        public LightSource Light;
        public TwilightObject VisualOwnerShow;
        public ExtraIncarnator(Extral owner, int x, int y)
            : base(owner)
        {
            SystemName = "ExtraIncarnator";
            widget = new CharacterWidget(this);

            TwilightField f = TWE3.Gtwi.GameMap.FieldMatrix[x, y];
            field = f;
            VisualIncarnator = new TwilightObject(new AnimTexture(@"fld\" + f.Attribute.Style + @"\ExtraIncarnator"), null);
            VisualOwnerShow = new TwilightObject(new AnimTexture(@"fld\" + f.Attribute.Style + @"\ExtraIncarnatorOwn"), null);

            Ambient = new LightSource(lightIntensity: 0.8f, lightDistance: 120.0f * 3.0f);
            Ambient.LightColor.Value = Color.White.ToVector3();

            Light = new LightSource(altitude: 2.0f, lightDistance: 120.0f * 2.0f);
            Light.LightColor.Value = OwnerOmnit.SideColor.ToVector3();

            Reservoirs.Remove(Chaos); _components.Remove(Chaos); Chaos = null;
            _components.Remove(DefaultSkills.ReservoirSkills[0]); DefaultSkills.ReservoirSkills.Clear();

            UF.PutOn(VisualIncarnator, f);
            UF.PutOn(VisualOwnerShow, VisualIncarnator);
            Ambient.Position.Value = VisualIncarnator.Center;
            Light.Position.Value = VisualIncarnator.Center;
            VisualOwnerShow.RGBMulti.Value = owner.SideColor.ToVector3();
            VisualOwnerShow.Texture.Shader.BlendingOptions = BlendState.Additive;
            VisualOwnerShow.Opacity.Value = 0.6f;
            VisualIncarnator.Level = TwilightLayer.BuildingLayer;
            VisualOwnerShow.Level = TwilightLayer.BuildingLayer + 0.5f;
            TWE3.twi.RegisterObject(VisualIncarnator);
            TWE3.twi.RegisterObject(VisualOwnerShow);
            TWE3.twi.RegisterLightSource(Ambient);
            TWE3.twi.RegisterLightSource(Light);

            f.Settler = this;
        }

        public override void VisualSelect(bool select)
        {
            if (select)
            {
                VisualOwnerShow.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1.0f, 1000);
                Light.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 3.0f * Global.Cell_H, 1000);
            }
            else
            {
                VisualOwnerShow.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.6f, 1000);
                Light.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1f * Global.Cell_H, 1000);
            }
        }
        public override TwilightField GetField()
        {
            return field;
        }

        public override void OmniPhaseStart()
        {
            Sintes.RegenRate = OwnerOmnit.EnergySupply.Count * GVAL.EXTRAINCARNATOR_SINTES_REGEN_RATE;
            base.OmniPhaseStart();
        }
        public Vector2 IncoCore
        { get { return VisualIncarnator.Position.Value + new Vector2(100, 62); } }
    }
}