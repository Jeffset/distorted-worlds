﻿using Microsoft.Xna.Framework;
using System;

namespace DWOaO
{
    [Serializable]
    public abstract class Building : DistortedGameComponent
    {

    }

    [Serializable]
    public class EnergySource : Building
    {
        public Omnit OwnerOmnit
        { get { return OwnerComponent as Omnit; } set { OwnerComponent = value; } }
        public TwilightEnergySource EnergySourceBuilding { get; set; }

        public void OccupyEnergySource(Actor owner)
        {
            if (OwnerOmnit != null)
                OwnerOmnit.EnergySupply.Remove(this);
            OwnerOmnit = (owner as Incarnation == null) ? owner as Omnit : (owner as Incarnation).OwnerOmnit;
            EnergySourceBuilding.OccupyMechanism.Level = Misc.TwilightLayer.BuildingLayer;
            TWE3.Gtwi.RegisterObject(EnergySourceBuilding.OccupyMechanism);
            EnergySourceBuilding.OccupyMechanism.RGBMulti.Value = OwnerOmnit.SideColor.ToVector3();
            EnergySourceBuilding.OccupyMechanism.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, 1f, 1500);
            TWE3.Gtwi.RegisterLightSource(EnergySourceBuilding.LightOccupy);
            EnergySourceBuilding.LightOccupy.LightDistance.Ctrl<Anim.FCtrl>().
                Cfg(Misc.Anim.Bezier3, 0.0f, EnergySourceBuilding.LightOccupy.LightDistance.Value, 1500);
            EnergySourceBuilding.LightOccupy.LightColor.Value = OwnerOmnit.SideColor.ToVector3();
            OwnerOmnit.EnergySupply.Add(this);
        }

        public EnergySource(TwilightEnergySource s)
        {
            EnergySourceBuilding = s;
        }
    }
}