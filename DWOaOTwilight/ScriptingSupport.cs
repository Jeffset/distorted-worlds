﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DWOaO
{
    public static class ScriptControl
    {
        public static void Wait(float time)
        {
            Monitor.Exit(TWE3.twi);
            Thread.Sleep((int)time);
            Monitor.Enter(TWE3.twi);
        }
        public static void WaitForScript(Script scr)
        {
            Monitor.Exit(TWE3.twi);
            scr.ScriptThread.Join();
            Monitor.Enter(TWE3.twi);
        }
    }
    public abstract class Script
    {
        public Thread ScriptThread { get; protected set; }
        public void Terminate()
        {
            if (ScriptThread.ThreadState == ThreadState.Stopped) return;
            Monitor.Enter(TWE3.twi);
            ScriptThread.Abort();
            Monitor.Exit(TWE3.twi);
        }
        public static Script<Deleg> Start<Deleg>(Deleg script, params object[] parameters)
        {
            return new Script<Deleg>(script, parameters);
        }
    }
    public class Script<Deleg> : Script
    {
        private class _ScriptStart { public object[] args; public Deleg scriptFct; }

        public Script(Deleg script, object[] parameters)
        {
            ScriptThread = new Thread(o =>
            {
                Monitor.Enter(TWE3.twi);
                var deleg = (o as _ScriptStart).scriptFct as Delegate;
                deleg.Method.Invoke(deleg.Target, (o as _ScriptStart).args);
                Monitor.Exit(TWE3.twi);
            });
            ScriptThread.IsBackground = true;
            //ScriptThread.Priority = ThreadPriority.;
            ScriptThread.Start(new _ScriptStart { args = parameters, scriptFct = script });
        }
    }

    public delegate void ActorScriptDeleg(ActorModels.ActorModel actor, float f);
    public delegate void TWScript(TwilightObject o);
}