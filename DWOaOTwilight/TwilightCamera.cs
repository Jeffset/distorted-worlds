﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO
{
    [Serializable]
    public class TwilightCamera : TwilightObject
    {
        public TwilightCamera(int width = -1, int height = -1)
        {
            if (width == -1) Width = (int)TWE3.twi.ScreenSize.X;
            else
                Width = width;
            if (height == -1) Height = (int)TWE3.twi.ScreenSize.Y;
            else
                Height = height;
        }

        public Rectangle FieldBounds
        {
            get
            {
                Rectangle v = Bounds;
                Rectangle f = new Rectangle();
                f.X = (int)((v.X) / Global.Cell_H);
                f.Y = (int)((v.Y) / Global.Cell_V);
                f.Width = (int)((v.Right - 1) / Global.Cell_H) + 1 - f.X;
                f.Height = (int)((v.Bottom - 1) / Global.Cell_V) + 1 - f.Y;
                return f;
            }
        }

        public void MoveCameraOnObject(TwilightObject target)
        {
            var scr = Script.Start<Action<TwilightCamera, TwilightObject>>((c, t) =>
            {
                TWGE3.Gtwi.GameControlFullyDenied = true;
                Vector2 p = Misc.UF.PutOnA(c, t);
                c.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, p, 1000).WaitForFinish(1200);
                TWGE3.Gtwi.GameControlFullyDenied = false;
                TWE3.twi.ConfigCameraCtrl();
            }, this, target);
        }
    }
}
