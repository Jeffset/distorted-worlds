﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Threading;

namespace DWOaO
{
    public class GamePanel
    {
        // объект, свойства и возможности которого показываются в данный момент на панельке.
        public DistortedGameComponent DisplayedObject;

        public TwilightObject BackGround { get; private set; }

        private TextWidget _messageWidget; private Script _messageScript;
        public void ShowMessage(string message, Color color, int count = 3)
        {
            _messageWidget = new TextWidget(message, "BuxtonSketch", color);
            _messageWidget.Position.Value = new Vector2((TWE3.twi.ScreenSize.X - _messageWidget.Width) / 2, 40);
            //_messageWidget.Position.Value = new Vector2(TwilightEngine3.Twilight.ScreenSize.X/);
            if (_messageScript != null) _messageScript.Terminate();
            _messageScript = Script.Start<Action<TextWidget, int>>((tw, c) =>
                {
                    try
                    {
                        TWE3.twi.RegisterWidget(tw);
                        tw.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, 1.0f, 300).WaitForFinish(500 * c);
                        tw.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 1.0f, 0.0f, 1000).WaitForFinish(1000);
                    }
                    catch (ThreadAbortException)
                    { }
                    finally
                    {
                        TWE3.twi.UnregisterWidget(tw);
                    }
                }, _messageWidget, count);
        }

        public void ShowPanel(Actor act)
        {
            if (DisplayedObject != null) HidePanel();
            TWE3.twi.RegisterWidget(BackGround);
            DisplayedObject = act;
            for (int i = 0; i < act.Reservoirs.Count; i++)
            {
                var res = act.Reservoirs[i];
                res.widget.Position.Value = new Vector2(200, TWE3.twi.ScreenSize.Y - 50) + new Vector2(res.widget.Width + 50, 0) * i;
                res.widget.AnimateValues();
                TWE3.Gtwi.RegisterWidget(res.widget);
            }
            act.Omnipoints.widget.Position.Value = new Vector2(5, TWE3.twi.ScreenSize.Y - 188);
            TWE3.Gtwi.RegisterWidget(act.Omnipoints.widget);
            act.GameLevel.widget.Position.Value = new Vector2(120, TWE3.twi.ScreenSize.Y - 90);
            TWE3.Gtwi.RegisterWidget(act.GameLevel.widget);
            TWE3.Gtwi.RegisterWidget(act.widget);
        }

        public void HidePanel()
        {
            TWE3.twi.UnregisterWidget(BackGround);
            Actor act = DisplayedObject as Actor;
            for (int i = 0; i < act.Reservoirs.Count; i++)
            {
                var res = act.Reservoirs[i];
                TWE3.Gtwi.UnregisterWidget(res.widget);
            }
            TWE3.Gtwi.UnregisterWidget(act.Omnipoints.widget);
            TWE3.Gtwi.UnregisterWidget(act.GameLevel.widget);
            TWE3.Gtwi.UnregisterWidget(act.widget);
        }

        public GamePanel()
        {
            BackGround = new TwilightObject(new AnimTexture(@"GUI\back"), new Vector2(TWE3.twi.ScreenSize.X, 111));
            BackGround.Position.Value = new Vector2(0, TWE3.twi.ScreenSize.Y - 110);
            BackGround.Level = Misc.TwilightLayer.WidgetLayer - 1.0f;
            BackGround.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(TWE3.twi.Camera.Position);
        }
    }
}