﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Text;

namespace DWOaO
{
    // Класс графического компонента системы скиллов.
    // Используется любыми скиллами для представления на панельке.
    // Обладает настраивемыми параметрами.
    [Serializable]
    public class SkillWidget : TwilightWidget
    {
        public Skill GameSkill { get; private set; }

        AnimTexture SkillIcon;
        AnimFloat3 IconRGBMulti;

        private void _RegisterEventHandlers()
        {
            Enter += SkillWidget_Enter;
            Leave += SkillWidget_Leave;
        }

        void SkillWidget_Leave(TwilightWidget sender)
        {
            IconRGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Bezier3, Color.Gray.ToVector3(), 450);
        }

        void SkillWidget_Enter(TwilightWidget sender)
        {
            IconRGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Bezier3, Color.White.ToVector3(), 450);
        }

        public SkillWidget(string iconName, Skill gameSkill)
        {
            Level = Misc.TwilightLayer.WidgetLayer - 0.01f;
            GameSkill = gameSkill;
            IconRGBMulti = new AnimFloat3(Color.Gray.ToVector3());
            RGBMulti.Value = gameSkill != null ? gameSkill.Owner.SideColor.ToVector3() : Color.Gray.ToVector3();
            if (gameSkill == null) Opacity.Value = 0.5f;
            Texture = new AnimTexture(@"SkillIcons\glow70x70"); // это свечение вокруг виджета (разм. 70 х 70)
            SkillIcon = new AnimTexture(@"SkillIcons\" + iconName);// это иконка скилла на черном фоне (разм. 50 х 50)
            Size.Value = new Vector2(70.0f);
            animParams.AddRange(new AnimParam[] { IconRGBMulti });
            _RegisterEventHandlers();
        }

        public SkillWidget(Color sideColor)
            : this("emptySlot", null)
        { }

        public override void Render()
        {
            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            XNAPlatform.Sprite.Draw(Texture.Texture, new Rectangle((int)Position.X, (int)Position.Y, 70, 70), Misc.UF.RGBA(RGBMulti, Opacity));
            XNAPlatform.Sprite.End();

            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.Opaque);
            XNAPlatform.Sprite.Draw
                (SkillIcon.Texture, new Rectangle((int)Position.X + 10, (int)Position.Y + 10, 50, 50), Misc.UF.RGBA(GameSkill.Reservoir.EnergyColor.ToVector3(), Opacity));
            XNAPlatform.Sprite.End();
        }
    }

    [Serializable]
    public class ReservoirWidget : TwilightWidget
    {
        public const int NOPHASELIMIT = 11111110;

        public EnergyReservoir Reservoir { get; private set; }

        public DistortedFont Font;

        public AnimTexture NameAndIndicator;
        public AnimTexture IndicatorLine;

        public AnimFloat CurrentValue { get; private set; }
        public AnimFloat AvailableValue { get; private set; }

        private Rectangle? _CalculateSourceRectCurrVal()
        {
            return new Rectangle(0, 0, (int)(IndicatorLine.Width * CurrentValue.Value / Reservoir.Capacity), IndicatorLine.Height);
        }
        private Rectangle? _CalculateSourceRectAvailVal()
        {
            float v = Math.Max(CurrentValue.Value - AvailableValue.Value, 0);
            return new Rectangle(0, 0, (int)(IndicatorLine.Width * v / Reservoir.Capacity), IndicatorLine.Height);
        }

        public void AnimateValues()
        {
            CurrentValue.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, Reservoir.CurrentValue, 1000);
            AvailableValue.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, Reservoir.AvialablePhaseConsumption, 1000);
        }

        public ReservoirWidget(string name, EnergyReservoir reservoir)// name может быть "STASIS", "CHAOS", "SINTES"
        {
            Reservoir = reservoir;
            Font = new DistortedFont("Jing Jing 14");
            RGBMulti.Value = Reservoir.EnergyColor.ToVector3();
            Size.Value = new Vector2(172, 45);
            CurrentValue = new AnimFloat(1.0f);
            AvailableValue = new AnimFloat(1.0f);
            NameAndIndicator = new AnimTexture(@"GUI\" + name);
            IndicatorLine = new AnimTexture(@"GUI\ReservIndic");
            animParams.AddRange(new AnimParam[] { CurrentValue, AvailableValue });
            LeftClick += ReservoirWidget_LeftClick;
            RightClick += ReservoirWidget_RightClick;
        }

        void ReservoirWidget_RightClick(TwilightWidget sender)
        {
            ShowSkillbar(false);
        }

        bool IsSkillBarShown = false;
        public void ShowSkillbar(bool show)
        {
            if (IsSkillBarShown == show) return;

            for (int i = 0; i < Reservoir.ReservoirSkills.Count; i++)
            {
                var w = Reservoir.ReservoirSkills[i].widget;
                if (show)
                {
                    w.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, 1.0f, 200);
                    w.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3,
                        new Vector2(183 + w.Width * i, TWE3.twi.ScreenSize.Y),
                        new Vector2(183 + w.Width * i, TWE3.twi.ScreenSize.Y - 120),
                        200);
                    TWE3.twi.RegisterWidget(w);
                }
                else
                {
                    Script.Start<Action<TwilightObject, int>>((o, count) =>
                        {
                            o.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0.0f, 200);
                            o.Position.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3,
                        new Vector2(183 + o.Width * count, TWE3.twi.ScreenSize.Y),
                        200).WaitForFinish(250);
                            TWE3.twi.UnregisterWidget(o);
                        }, w, i);
                }
                IsSkillBarShown = show;
            }
        }
        void ReservoirWidget_LeftClick(TwilightWidget sender)
        {
            ShowSkillbar(true);
        }

        public override void Render()
        {
            string numbs;
            if (Reservoir.MaxPhaseConsumption != NOPHASELIMIT)
                numbs = string.Format("{1}/{0}/{2}", (int)CurrentValue.Value, Math.Min((int)AvailableValue.Value, (int)CurrentValue.Value), Reservoir.Capacity);
            else
                numbs = string.Format("{0}/{1}", (int)CurrentValue.Value, Reservoir.Capacity);
            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            XNAPlatform.Sprite.Draw(IndicatorLine.Texture, Position.Value, _CalculateSourceRectCurrVal(), Misc.UF.RGBA(RGBMulti, Opacity));
            XNAPlatform.Sprite.Draw(IndicatorLine.Texture, Position.Value, _CalculateSourceRectAvailVal(), Misc.UF.RGBA(RGBMulti.Value * 0.5f, Opacity));
            XNAPlatform.Sprite.Draw(NameAndIndicator.Texture, Bounds, Misc.UF.RGBA(Reservoir.OwnerActor.SideColor.ToVector3(), Opacity));
            XNAPlatform.Sprite.DrawString(Font.Font, numbs, Position.Value + new Vector2(88, 0), Reservoir.EnergyColor);
            XNAPlatform.Sprite.End();
        }
    }

    [Serializable]
    public class CharacterWidget : TwilightWidget
    {
        public Actor Actor { get; private set; }
        public AnimTexture Frame { get; private set; }

        public CharacterWidget(Actor act)
        {
            Actor = act;
            Texture = new AnimTexture(@"mdl\" + act.SystemName + @"\Icon");
            Texture.Shader.BlendingOptions = BlendState.Opaque;
            Frame = new AnimTexture(@"mdl\" + act.SystemName + @"\NameFrame");
            Position.Value = new Vector2(0, TWE3.twi.ScreenSize.Y - 150);
        }

        public override void Render()
        {
            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.Additive);
            XNAPlatform.Sprite.Draw(Frame.Texture, Position.Value, Misc.UF.RGBA(Actor.SideColor.ToVector3(), Opacity));
            XNAPlatform.Sprite.End();

            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            XNAPlatform.Sprite.Draw(Texture.Texture, Position.Value, Misc.UF.RGBA(Color.White.ToVector3(), Opacity));
            XNAPlatform.Sprite.End();
        }
    }

    [Serializable]
    public class NumberWidget : TwilightWidget
    {
        public Actor Actor;
        public AnimFloat Value { get; private set; }
        public AnimFloat3 TextColor { get; private set; }

        public DistortedFont Font;

        public NumberWidget(string fontName, int size, Actor actor, Color textColor)
        {
            Size.Value = new Vector2(size);
            Actor = actor;
            Font = new DistortedFont(fontName);
            Texture = new AnimTexture(@"GUI\GlowingMalevich");
            Value = new AnimFloat(12);
            TextColor = new AnimFloat3(textColor.ToVector3());
            animParams.AddRange(new AnimParam[] { TextColor, Value });
        }

        public override void Render()
        {
            XNAPlatform.Sprite.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied);
            XNAPlatform.Sprite.Draw(Texture.Texture, Bounds, Misc.UF.RGBA(Actor.SideColor.ToVector3(), Opacity));
            int value = (int)Value.Value; Vector2 pos = Position.Value + (value >= 10 ? new Vector2(11, 9) : new Vector2(18, 9));
            XNAPlatform.Sprite.DrawString(Font.Font, value.ToString(), pos, Misc.UF.RGBA(TextColor, Opacity));
            XNAPlatform.Sprite.End();
        }
    }

    [Serializable]
    public class TextWidget : TwilightWidget
    {
        public bool UseGlobalCood { get; set; } = false;
        public string Text { get; set; }
        public DistortedFont Font { get; set; }

        public TextWidget(string text, string fontName, Color color)
        {
            Text = text;
            Font = new DistortedFont(fontName);
            RGBMulti.Value = color.ToVector3();
            Size.Value = Font.Font.MeasureString(Text);
        }
        public override void Render()
        {
            const int d = 3;
            XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            Vector2 p = UseGlobalCood ? Position.Value - TWE3.twi.Camera.Position.Value : Position.Value;
            XNAPlatform.Sprite.DrawString(Font.Font, Text, p + new Vector2(d, d), Misc.UF.RGBA(Color.Black, Opacity));
            XNAPlatform.Sprite.DrawString(Font.Font, Text, p + new Vector2(-d, d), Misc.UF.RGBA(Color.Black, Opacity));
            XNAPlatform.Sprite.DrawString(Font.Font, Text, p + new Vector2(d, -d), Misc.UF.RGBA(Color.Black, Opacity));
            XNAPlatform.Sprite.DrawString(Font.Font, Text, p + new Vector2(-d, -d), Misc.UF.RGBA(Color.Black, Opacity));
            XNAPlatform.Sprite.DrawString(Font.Font, Text, p, Misc.UF.RGBA(RGBMulti.Value, Opacity));
            XNAPlatform.Sprite.End();
        }
    }

    [Serializable]
    public class IndicatorWidget : TwilightWidget
    {

    }
}