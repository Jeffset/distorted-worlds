﻿using System;

namespace DWOaO
{
    public delegate void TwWidgetEventHandler(TwilightWidget sender);

    public abstract class TwilightWidget : TwilightObject
    {
        private bool _leftDown;
        private bool _rightDown;

        public event TwWidgetEventHandler LeftClick;
        public event TwWidgetEventHandler RightClick;

        public event TwWidgetEventHandler Enter;
        public event TwWidgetEventHandler Leave;

        internal virtual void _MouseDown(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LMB: _leftDown = true;
                    break;
                case MouseButton.RMB: _rightDown = true;
                    break;
            }
        }
        internal virtual void _MouseUp(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LMB: if (_leftDown) { _leftDown = false; if (LeftClick != null) LeftClick(this); }
                    break;
                case MouseButton.RMB: if (_rightDown) { _rightDown = false; if (RightClick != null) RightClick(this); }
                    break;
            }
        }

        internal virtual void _MouseEnter()
        {
            if (Enter != null) Enter(this);
        }

        internal virtual void _MouseLeave()
        {
            _leftDown = false; _rightDown = false;
            if (Leave != null) Leave(this);
        }

        public TwilightWidget()
        {
            Level = Misc.TwilightLayer.WidgetLayer;
        }
    }
}