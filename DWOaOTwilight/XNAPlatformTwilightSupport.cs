using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using System.IO;

namespace DWOaO
{
    public class XNAPlatform : Microsoft.Xna.Framework.Game
    {
        public bool ShouldBeLoaded { get; set; }
        public string LoadFileName { get; set; }

        public static XNAPlatform Instance;

        public TWE3 GameEngine { get; set; }

        public static GraphicsDeviceManager GraphicsManager { get; private set; }
        public static SpriteBatch Sprite { get; private set; }
        public static ContentManager ContentManager { get; private set; }

        public XNAPlatform(Misc.GraphicMode graphicsMode)
        {
            Instance = this;
            GraphicsManager = new GraphicsDeviceManager(this);
            GraphicsManager.GraphicsProfile = GraphicsProfile.HiDef;
            IsMouseVisible = true;
            IsFixedTimeStep = false;
            Content.RootDirectory = "Content";
            GraphicsManager.PreferredBackBufferFormat = SurfaceFormat.Color;
            GraphicsManager.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            GraphicsManager.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            switch (graphicsMode)
            {
                case DWOaO.Misc.GraphicMode.Fullscreen:
                    {
                        GraphicsManager.PreferMultiSampling = true;
                        GraphicsManager.IsFullScreen = true;
                        GraphicsManager.SynchronizeWithVerticalRetrace = true;
                    }
                    break;
                case DWOaO.Misc.GraphicMode.Windowed:
                    {
                        GraphicsManager.PreferMultiSampling = true;
                        GraphicsManager.IsFullScreen = false;
                    }
                    break;
            }
        }

        protected override void Initialize()
        {
            ContentManager = this.Content;
            Sprite = new SpriteBatch(GraphicsDevice);
            base.Initialize();
            if (ShouldBeLoaded)
                GameEngine = TWE3.Construct(LoadFileName);
            else
            {
                GameEngine.Construct();
            }
        }

        protected override void LoadContent()
        {
            font = Content.Load<SpriteFont>(@"Fonts\Jing Jing 14");
        }

        protected override void Update(GameTime gameTime)
        {
            Monitor.Enter(TWE3.twi);
            GameEngine.Update(gameTime);
            Monitor.Exit(TWE3.twi);
        }

        private SpriteFont font;
        protected override void Draw(GameTime gameTime)
        {
            Monitor.Enter(TWE3.twi);
            GraphicsDevice.SetRenderTarget(null);
            GameEngine.Render();
#if DEBUG
            Sprite.Begin();
            Sprite.DrawString(font, ((int)Math.Round(1000.0f / (gameTime.ElapsedGameTime.TotalMilliseconds + 1))).ToString(), new Vector2(5), Color.Gray);
            Sprite.End();
#endif
            Monitor.Exit(TWE3.twi);
        }
    }
}
