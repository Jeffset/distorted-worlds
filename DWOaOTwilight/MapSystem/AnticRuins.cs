﻿using DWOaO.Misc;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DWOaO.MapSystem
{
    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Земля (по умолчанию)", IsDefault = true, SurfaceType = FieldSurfaceType.Mud)]
    public class AnticRuinsGround : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 2.0f; } }
        public AnticRuinsGround()
        {
        }

        public override Color GetAvColor()
        {
            return Color.YellowGreen;
        }
    }

    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Каменистая дорога", FieldMask = "FieldMask2", SurfaceType = FieldSurfaceType.Mud)]
    public class AnticRuinsStoneroad : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 1.5f; } }
        public AnticRuinsStoneroad()
        {

        }

        public override Color GetAvColor()
        {
            return Color.Yellow;
        }
    }

    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Мраморная мостовая", FieldMask = "FieldMask3")]
    public class AnticRuinsMarbleroad : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 1.0f; } }
        public AnticRuinsMarbleroad()
        {

        }

        public override Color GetAvColor()
        {
            return Color.Wheat;
        }
    }

    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Красная плесень", FieldMask = "FieldMask4", SurfaceType= FieldSurfaceType.Mud)]
    public class AnticRuinsMush : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 1.0f; } }
        public AnticRuinsMush()
        {

        }
        public override Color GetAvColor()
        {
            return Color.HotPink;
        }
    }

    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Серая мостовая", FieldMask = "FieldMask2")]
    public class AnticRuinsBrickroad : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 1.0f; } }
        public AnticRuinsBrickroad()
        {

        }

        public override Color GetAvColor()
        {
            return Color.CornflowerBlue;
        }
    }
    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Обрыв", FieldMask = "FieldMask2")]
    public class AnticRuinsFall : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 10f; } }
        public AnticRuinsFall()
        {

        }

        public override Color GetAvColor()
        {
            return Color.Gray;
        }
    }

    [Serializable]
    [EditorFieldInfo("AnticRuins", FriendlyName = "Водная поверхность", FieldMask = "FieldMask1", SurfaceType = FieldSurfaceType.Liquid)]
    public class AnticRuinsWater : TwilightField
    {
        public override float BasePassEnergy
        { get { return 1.5f; } }
        public override float PassEnergy
        { get { return 2.5f; } }

        private void _InitMapsAndShaders()
        {
            displEffect = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\Displacement");
            displMap = XNAPlatform.ContentManager.Load<Texture2D>(@"ShaderMaps\dt");
            displacedNormalMap = new RenderTarget2D(XNAPlatform.GraphicsManager.GraphicsDevice, Texture.NormalMap.Width, Texture.NormalMap.Height);


        }
        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            _InitMapsAndShaders();
        }
        public AnticRuinsWater()
        {
            _InitMapsAndShaders();
        }
        [NonSerialized]
        private Effect displEffect;
        [NonSerialized]
        private Texture2D displMap;
        [NonSerialized]
        private RenderTarget2D displacedNormalMap;
        [NonSerialized]
        private bool _needLiquify = false;
        public override void Update()
        {
            base.Update();
            if (!_needLiquify) return;
            //Texture2D texture = Texture.Texture;
            displEffect.Parameters["displMap"].SetValue(displMap);
            displEffect.Parameters["maskMap"].SetValue(FieldMask.Texture);
            displEffect.Parameters["factor"].SetValue(2.0f);
            displEffect.Parameters["strength"].SetValue(0.07f);
            displEffect.Parameters["scrollX"].SetValue((float)((int)Global.GameTime.TotalGameTime.TotalMilliseconds % 4000) / 4000.0f);
            XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(displacedNormalMap);
            XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.LinearClamp, null, null, displEffect);
            XNAPlatform.Sprite.Draw(Texture.NormalMap, Vector2.Zero, Color.FromNonPremultiplied(new Vector4(RGBMulti.Value, Opacity.Value)));
            XNAPlatform.Sprite.End();
            XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(null);
            //Texture.NormalMap = displacedNormalMap;
            _needLiquify = false;
        }
        public override void Render()
        {
            //base.Render();
            Texture2D srcNrmlMap = Texture.NormalMap;
            Texture.NormalMap = displacedNormalMap;
            base.Render();
            Texture.NormalMap = srcNrmlMap;
            _needLiquify = true;
        }

        public override Color GetAvColor()
        {
            return Color.Blue;
        }
    }

    [Serializable]
    [EditorMapObjectInfo("AnticRuins", FriendlyName = "Чистый Источник", ObjectType = MapObjectType.FieldAssoc)]
    public class AnticRuinsEnergySource : TwilightEnergySource
    {
        [OnDeserialized]
        void OnDeserialized(StreamingContext c)
        {
            OnObjectRegistered();
            Texture.Shader.BlendingOptions = BlendState.NonPremultiplied;
        }
        public AnticRuinsEnergySource()
        {
            //RGBMulti.Value = Color.Cyan.ToVector3();
            Light = new LightSource(lightIntensity: 0.8f, lightDistance: 120.0f * 2.0f);
            Light.LightColor.Value = Color.White.ToVector3();
            //Light.Altitude.Value = 120; 
            Light.Position.Value = Center;//new Vector2(Texture.Width / 2, Texture.Height * (1.0f / Global.PerspectiveFactor));
            Light.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(this.Position);

            LightOccupy = new LightSource(altitude: 2.0f, lightDistance: 120.0f);
            LightOccupy.LightColor.Value = Color.Black.ToVector3();
            LightOccupy.Position.Value = Center;//new Vector2(Texture.Width / 2, Texture.Height * (1.0f / Global.PerspectiveFactor));
            LightOccupy.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(this.Position);
        }
        public override void Update()
        {
            base.Update();
            Point p = UF.AbsToField(Position.Value);
            Position.Value = UF.FieldToAbs(p.X, p.Y);
            //Light.Update();
            //LightOccupy.Update();
        }
    }

    [Serializable]
    [EditorMapObjectInfo("AnticRuins", FriendlyName = "Замшелая скала", ObjectType = MapObjectType.Free)]
    public class AnticRuinsRock : TwilightMapObject
    {
        public AnticRuinsRock()
        {
            this.Level = TwilightLayer.ActorLayer;
        }
    }
    [Serializable]
    [EditorMapObjectInfo("AnticRuins", FriendlyName = "Дзярэўе", ObjectType = MapObjectType.Free)]
    public class AnticRuinsTree : TwilightMapObject
    {
        public AnticRuinsTree()
        {
            this.Level = TwilightLayer.ActorLayer;
        }
    }

    [Serializable]
    [EditorMapObjectInfo("AnticRuins", FriendlyName = "Болотная скала", ObjectType = MapObjectType.Free)]
    public class AnticRuinsMushRock : TwilightMapObject
    {
        public AnticRuinsMushRock()
        {
            this.Level = TwilightLayer.ActorLayer;
        }
    }

    [Serializable]
    [EditorMapObjectInfo("AnticRuins", FriendlyName = "Болотная скала1", ObjectType = MapObjectType.Free)]
    public class AnticRuinsRock1 : TwilightMapObject
    {
        public AnticRuinsRock1()
        {
            this.Level = TwilightLayer.ActorLayer;
        }
    }
}
