﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DWOaO.MapSystem
{
    [Serializable]
    public struct FieldMaskConfig
    {
        public Type ForField;
        public string BackField;
        public const float Border = 0.15f;
        public bool c1, c2, c3, c4;
        public bool t, r, l, b;
        public Rectangle T, R, L, B;
        public Rectangle C1, C2, C3, C4;

        public bool IsClear()
        {
            return (l && r && t && b && c1 && c2 && c3 && c4);
        }

        public FieldMaskConfig(Type fieldType, string back)
        {
            ForField = fieldType;
            BackField = back;
            t = false; r = false; l = false; b = false;
            c1 = false; c2 = false; c3 = false; c4 = false;
            int Bd = (int)Math.Round(Global.Cell_H * Border);
            int Wd = (int)Math.Round(Global.Cell_H - 2.0f * Global.Cell_H * Border);
            T = new Rectangle(Bd, 0, Wd, Bd);
            R = new Rectangle(Wd + Bd, Bd, Bd, Wd);
            B = new Rectangle(Bd, Wd + Bd, Wd, Bd);
            L = new Rectangle(0, Bd, Bd, Wd);
            C1 = new Rectangle(0, 0, Bd, Bd);
            C2 = new Rectangle(Wd + Bd, 0, Bd, Bd);
            C3 = new Rectangle(Wd + Bd, Wd + Bd, Bd, Bd);
            C4 = new Rectangle(0, Wd + Bd, Bd, Bd);
        }
    }

    public static class TwilightFieldTextureChache
    {
        private static Dictionary<FieldMaskConfig, Texture2D[]> _chache =
            new Dictionary<FieldMaskConfig, Texture2D[]>();
        //public static readonly string DefaultTextureName 
        public static AnimTexture GetFieldTexture(FieldMaskConfig config, AnimTexture baseTexture, AnimTexture texture, string fieldMaskName, out AnimTexture maskResult)
        {
            if (_chache.ContainsKey(config))
            {
                Texture2D[] t = _chache[config];
                maskResult = new AnimTexture(t[1]);
                return new AnimTexture(t[0], t[2]);
            }
            AnimTexture maskMap = new AnimTexture(Global.Cell_H, Global.Cell_H);
            AnimTexture maskSide = new AnimTexture(fieldMaskName + "_side");
            AnimTexture maskCorner = new AnimTexture(fieldMaskName + "_corner");
            XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(maskMap.Texture as RenderTarget2D);
            XNAPlatform.GraphicsManager.GraphicsDevice.Clear(Color.White);
            XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied);
            Vector2 pos = new Vector2(Global.Cell_H) / 2.0f;
            var config2 = config;
            if (!config.t && !config.r)
            {
                XNAPlatform.Sprite.Draw(maskCorner.Texture, pos, null, Color.White, (float)Math.PI, pos, 1.0f, SpriteEffects.None, 1.0f);
                config2.t = true; config2.r = true;
            }
            if (!config.r && !config.b)
            {
                XNAPlatform.Sprite.Draw(maskCorner.Texture, pos, null, Color.White, -(float)Math.PI / 2.0f, pos, 1.0f, SpriteEffects.None, 1.0f);
                config2.b = true; config2.r = true;
            }
            if (!config.b && !config.l)
            {
                XNAPlatform.Sprite.Draw(maskCorner.Texture, pos, null, Color.White, 0, pos, 1.0f, SpriteEffects.None, 1.0f);
                config2.b = true; config2.l = true;
            }
            if (!config.l && !config.t)
            {
                XNAPlatform.Sprite.Draw(maskCorner.Texture, pos, null, Color.White, +(float)Math.PI / 2.0f, pos, 1.0f, SpriteEffects.None, 1.0f);
                config2.t = true; config2.l = true;
            }
            if (!config2.t) XNAPlatform.Sprite.Draw(maskSide.Texture, pos, null, Color.White, (float)Math.PI, pos, 1.0f, SpriteEffects.None, 1.0f);
            if (!config2.r) XNAPlatform.Sprite.Draw(maskSide.Texture, pos, null, Color.White, -(float)Math.PI / 2.0f, pos, 1.0f, SpriteEffects.None, 1.0f);
            if (!config2.b) XNAPlatform.Sprite.Draw(maskSide.Texture, pos, null, Color.White, 0.0f, pos, 1.0f, SpriteEffects.None, 1.0f);
            if (!config2.l) XNAPlatform.Sprite.Draw(maskSide.Texture, pos, null, Color.White, +(float)Math.PI / 2.0f, pos, 1.0f, SpriteEffects.None, 1.0f);
            XNAPlatform.Sprite.End();
            AnimTexture field = new AnimTexture(Global.Cell_H, Global.Cell_H, true);
            Effect maskShader = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\Masking");
            {
                XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(field.Texture as RenderTarget2D);
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque);//, null, null, null, maskShader);
                XNAPlatform.Sprite.Draw(baseTexture.Texture, new Vector2(), Color.White);
                XNAPlatform.Sprite.End();
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, maskShader);
                maskShader.Parameters["maskMap"].SetValue(maskMap.Texture);
                XNAPlatform.Sprite.Draw(texture.Texture, new Vector2(), Color.White);
                XNAPlatform.Sprite.End();
            }
            {
                XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(field.NormalMap as RenderTarget2D);
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.Opaque);//, null, null, null, maskShader);
                XNAPlatform.Sprite.Draw(baseTexture.NormalMap, new Vector2(), Color.White);
                XNAPlatform.Sprite.End();
                XNAPlatform.Sprite.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, null, null, maskShader);
                maskShader.Parameters["maskMap"].SetValue(maskMap.Texture);
                XNAPlatform.Sprite.Draw(texture.NormalMap, new Vector2(), Color.White);
                XNAPlatform.Sprite.End();
            }
            //TwilightEngine.GraphicsManager.GraphicsDevice.SetRenderTarget(
            var old = field.NormalMap;
            field.NormalMap = field.GenerateNormalFromBump(field.NormalMap);
            old.Dispose();
            //var f = new System.IO.FileStream("F:\\normal.png", System.IO.FileMode.OpenOrCreate);
            //XNAPlatform.GraphicsManager.GraphicsDevice.SetRenderTarget(null);
            //field.NormalMap.SaveAsPng(f, field.NormalMap.Width, field.NormalMap.Height);
            //f.Close();
            _chache[config] = new Texture2D[3] { field.Texture, maskMap.Texture, field.NormalMap };
            maskResult = maskMap;
            return field;
        }
    }
}
namespace DWOaO
{
    [Serializable]
    public abstract class TwilightField : Lightable
    {
        [NonSerialized]
        internal Life.Maneuver_Uni.FieldGoInfo GoInfo;
        public virtual float BasePassEnergy { get; set; }
        public virtual float PassEnergy { get; set; }
        public float GetPassEnergy(TwilightField prev, Actor forWhom)
        {
            //if (prev == null) return PassEnergy;
            //Vector2 diff = Position.Value - prev.Position.Value;
            //if (diff.Y < 0)
            //{ if (fieldMaskConfig.t) return PassEnergy + BasePassEnergy; }
            //else if (diff.X > 0)
            //{ if (fieldMaskConfig.r) return PassEnergy + BasePassEnergy; }
            //else if (diff.Y > 0)
            //{ if (fieldMaskConfig.b) return PassEnergy + BasePassEnergy; }
            //else if (diff.X < 0)
            //{ if (fieldMaskConfig.l) return PassEnergy + BasePassEnergy; }
            if (Settler != null && Settler != forWhom)
                return 1000;
            else return PassEnergy;
        }

        public DistortedGameComponent Settler;

        public Point FieldPos
        { get { return Misc.UF.AbsToField(Position.Value); } }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            Attribute = GetType().GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true)[0] as MapSystem.EditorFieldInfoAttribute;
            CalculateTexture();
        }

        public MapSystem.FieldMaskConfig fieldMaskConfig;
        public AnimTexture FieldMask;

        [NonSerialized]
        public MapSystem.EditorFieldInfoAttribute Attribute;
        public string Name { get; private set; }

        public TwilightField()
        {
            Type type = this.GetType();
            Attribute = type.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorFieldInfoAttribute), true)[0] as MapSystem.EditorFieldInfoAttribute;
            fieldMaskConfig = new MapSystem.FieldMaskConfig(type, "Default");
            Name = type.Name;

            CalculateTexture();
            Size.Value = new Vector2(Global.Cell_H, Global.Cell_V);
        }
        public string GetName()
        { return Name.Substring(Attribute.Style.Length); }
        public void CalculateTexture()
        {
            //if (fieldMaskConfig.IsClear()) fieldMaskConfig.BackField = this.Attribute.IsDefault ? "Default" : this.GetName();
            if (Texture != null && animParams.Contains(Texture))
                animParams.Remove(Texture);
            string TextureName = "fld\\" + Attribute.Style + "\\" + Name.Substring(Attribute.Style.Length);

            if (Attribute.IsDefault)
            {
                Texture = new AnimTexture(@"fld\" + Attribute.Style + "\\Default", true);
                Texture.NormalMap = Texture.GenerateNormalFromBump(Texture.NormalMap);
                FieldMask = new AnimTexture(@"ShaderMaps\solid");
            }
            else
            {
                AnimTexture workTexture = new AnimTexture(TextureName, true);
                AnimTexture baseTexture = new AnimTexture(@"fld\" + Attribute.Style + "\\" + fieldMaskConfig.BackField, true);

                string fieldMaskName = @"fld\" + Attribute.Style + "\\" + Attribute.FieldMask;
                Texture = MapSystem.TwilightFieldTextureChache.GetFieldTexture(fieldMaskConfig, baseTexture, workTexture, fieldMaskName, out FieldMask);
            }

            Texture.Shader.BlendingOptions = BlendState.Opaque;
            animParams.Add(Texture);
        }

        public virtual Color GetAvColor()
        {
            return Color.White;
        }
    }

    [Serializable]
    public abstract class TwilightMapObject : TwilightObject
    {
        //[OnDeserialized]
        //private void OnDeserialized(StreamingContext context)
        //{
        //    normalMappingShader = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\3DMapLighting");
        //    Matrix projection =
        //        Matrix.CreateOrthographicOffCenter(0, XNAPlatform.GraphicsManager.GraphicsDevice.Viewport.Width, XNAPlatform.GraphicsManager.GraphicsDevice.Viewport.Height, 0, 0, 1);
        //    Matrix halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);
        //    normalMappingShader.Parameters["transformMatrix"].SetValue(halfPixelOffset * projection);
        //}

        [NonSerialized]
        public MapSystem.EditorMapObjectInfoAttribute Attribute;

        public TwilightMapObject()
        {
            Type type = this.GetType();
            Attribute = type.GetCustomAttributes(typeof(DWOaO.MapSystem.EditorMapObjectInfoAttribute), true)[0] as MapSystem.EditorMapObjectInfoAttribute;
            CalculateTexture();
            //normalMappingShader = XNAPlatform.ContentManager.Load<Effect>(@"Shaders\3DMapLighting");
        }

        protected void CalculateTexture()
        {
            string TextureName = "fld\\" + Attribute.Style + "\\" + GetType().Name.Substring(Attribute.Style.Length);
            Texture = new AnimTexture(TextureName);
            Size.Value = new Vector2(Texture.Width, Texture.Height);
            Texture.Shader.BlendingOptions = BlendState.NonPremultiplied;
            animParams.Add(Texture);
        }
    }

    [Serializable]
    public abstract class TwilightEnergySource : TwilightMapObject
    {
        public LightSource Light;
        public LightSource LightOccupy;
        public TwilightObject OccupyMechanism;

        public Point FieldPos
        { get { return Misc.UF.AbsToField(Position.Value); } }

        public EnergySource EnergySource { get; set; }

        public TwilightEnergySource()
        {
            string TextureName = "fld\\" + Attribute.Style + "\\" + GetType().Name.Substring(Attribute.Style.Length) + "_OM";
            OccupyMechanism = new TwilightObject(new AnimTexture(TextureName), null);
            OccupyMechanism.Y = Height - OccupyMechanism.Height;
            OccupyMechanism.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(Position);
            OccupyMechanism.Texture.Shader.BlendingOptions = BlendState.Additive;
            //animParams.Add(Texture);
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            OccupyMechanism.Texture.Shader.BlendingOptions = BlendState.Additive;
        }

        public override void OnObjectRegistered()
        {
            base.OnObjectRegistered();
            //TwilightGameEngine3.Twilight.RegisterObject(OccupyMechanism);
            TWE3.twi.RegisterLightSource(Light);
            //TwilightEngine3.Twilight.RegisterLightSource(LightOccupy);
        }
        public override void OnObjectUnregistered()
        {
            base.OnObjectUnregistered();
            //TwilightGameEngine3.Twilight.UnregisterObject(OccupyMechanism);
            TWE3.twi.UnregisterLightSource(Light);
            //TwilightEngine3.Twilight.UnregisterLightSource(LightOccupy);
        }
    }
}
