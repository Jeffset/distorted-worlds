﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DWOaO.MapSystem
{
    public enum FieldSurfaceType
    {
        Hard = 0,
        Mud,
        Liquid
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class EditorFieldInfoAttribute : Attribute
    {
        public string FriendlyName { get; set; }
        public string Style { get; private set; }
        public string FieldMask { get; set; }
        public bool IsDefault { get; set; }
        public FieldSurfaceType SurfaceType { get; set; }
        public EditorFieldInfoAttribute(string style)
        {
            Style = style;
        }
    }

    public enum MapObjectType
    { Free, FieldAssoc }

    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class EditorMapObjectInfoAttribute : Attribute
    {
        public MapObjectType ObjectType { get; set; }
        public string FriendlyName { get; set; }
        public string Style { get; private set; }
        public EditorMapObjectInfoAttribute(string style)
        {
            Style = style;
        }
    }
}
