﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DWOaO;
using DWOaO.ActorModels;
using Microsoft.Xna.Framework;

namespace DWOaO.Life
{
    public sealed class AttackClose_Bomber : AttackClose_ExtraBase
    {
        public AttackClose_Bomber(Extral bomber)
            : base(bomber)
        { Damage = 120; CoolDown = 1; }

        protected override void ScriptAction()
        {
            #region SkillAction
            {
                Owner.Model.CurrentModelScript.Terminate();
                var scr = Script.Start<Action>(() =>
                {
                    var mdl = Owner.Model as Humanoid_mdl;
                    mdl.Mblur.Length = 30;
                    mdl.Mblur.Enabled = true;
                    TWE3.Gtwi.GameControlFullyDenied = true;
                    int t = 500, t2 = 150, t3 = 500;
                    //ScriptControl.Wait(t);
                    //mdl.ResetState(t * .1f, Misc.Anim.Bezier3).WaitForFinish(t * .1f);
                    if (enemy.CurrentDefence == null) enemy.ActivateDefaultDefence();
                    enemy.CurrentDefence.PerformDefence(Owner, Damage, t + t2);
                    Vector2 d1 = new Vector2();

                    switch (mdl.Orientation)
                    {
                        case Orient.ToUs:
                            #region Us
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+40, t].n();
                            mdl.LeftFoot[-10, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(0, -Global.Cell_V), new Vector2(0, Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[+20, t2].n();
                            mdl.LeftFoot[-60, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToRight:
                            #region Right
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+25, t].n();
                            mdl.LeftFoot[+30, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(Global.Cell_H / 4, -Global.Cell_V), new Vector2(Global.Cell_H / 2, 0), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[-40, t2].n();
                            mdl.LeftFoot[+80, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToLeft:
                            #region Left
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+20, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-5, t].n();
                            mdl.RightArm[+20, t].n();
                            mdl.RightHand[+5, t].n();
                            mdl.LeftLeg[+50, t].n();
                            mdl.LeftFoot[-80, t].n();
                            mdl.RightLeg[-20, t].n();
                            mdl.RightFoot[0, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(-Global.Cell_H / 4, -Global.Cell_V), new Vector2(-Global.Cell_H / 2, 1), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.LeftArm[+120, t2].n();
                            mdl.LeftHand[+120, t2].n();
                            mdl.RightArm[-50, t2].n();
                            mdl.RightHand[10, t2].n();
                            mdl.LeftLeg[+35, t2].n();
                            mdl.LeftFoot[0, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-20, t2].n();
                            mdl.Weapon[40, t2].n();
                            mdl.Head[-15, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToForward:
                            #region Forward
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+35, t].n();
                            mdl.RightArm[+35, t].n();
                            mdl.RightHand[+45, t].n();
                            mdl.LeftArm[-50, t].n();
                            mdl.LeftHand[+45, t].n();
                            mdl.RightLeg[-40, t].n();
                            mdl.RightFoot[+10, t].n();
                            mdl.LeftLeg[+30, t].n();
                            mdl.LeftFoot[-70, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                           (new Vector2(0, -Global.Cell_V), new Vector2(0, -Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.RightArm[-25, t2].n();
                            mdl.RightHand[-50, t2].n();
                            mdl.LeftArm[+60, t2].n();
                            mdl.LeftHand[+30, t2].n();
                            mdl.RightLeg[-20, t2].n();
                            mdl.RightFoot[+60, t2].n();
                            mdl.LeftLeg[+40, t2].n();
                            mdl.LeftFoot[+10, t2].n();
                            mdl.Head[+45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                    }
                    TwiSound.PlayStep(mdl);
                    mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().ReverseCfg(t3);
                    mdl.ResetState(t3, Misc.Anim.Bezier3).WaitForFinish(t3);
                    mdl.RunIdleBreathScript();
                    TwiSound.PlayStep(mdl);

                    mdl.Mblur.Enabled = false;
                    TWE3.Gtwi.GameControlFullyDenied = false;
                });
            }
            #endregion
        }
    }

    public sealed class AttackClose_ExtraInc : AttackClose_ExtraBase
    {
        public AttackClose_ExtraInc(ExtraInc inc)
            : base(inc)
        {
            Damage = 60;
        }

        protected override void ScriptAction()
        {
            #region SkillAction
            {
                Owner.Model.CurrentModelScript.Terminate();
                var scr = Script.Start<Action>(() =>
                {
                    var mdl = Owner.Model as Humanoid_mdl;
                    mdl.Mblur.Length = 30;
                    mdl.Mblur.Enabled = true;
                    TWE3.Gtwi.GameControlFullyDenied = true;
                    int t = 500, t2 = 150, t3 = 500;
                    //ScriptControl.Wait(t);
                    //mdl.ResetState(t * .1f, Misc.Anim.Bezier3).WaitForFinish(t * .1f);
                    if (enemy.CurrentDefence == null) enemy.ActivateDefaultDefence();
                    enemy.CurrentDefence.PerformDefence(Owner, Damage, t + t2 * 2);
                    Vector2 d1 = new Vector2();

                    switch (mdl.Orientation)
                    {
                        case Orient.ToUs:
                            #region Us
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+40, t].n();
                            mdl.LeftFoot[-10, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(0, -Global.Cell_V), new Vector2(0, Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[+20, t2].n();
                            mdl.LeftFoot[-60, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToRight:
                            #region Right
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+25, t].n();
                            mdl.LeftFoot[+30, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(Global.Cell_H / 4, -Global.Cell_V), new Vector2(Global.Cell_H / 2, 0), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[-40, t2].n();
                            mdl.LeftFoot[+80, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToLeft:
                            #region Left
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+20, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-5, t].n();
                            mdl.RightArm[+20, t].n();
                            mdl.RightHand[+5, t].n();
                            mdl.LeftLeg[+50, t].n();
                            mdl.LeftFoot[-80, t].n();
                            mdl.RightLeg[-20, t].n();
                            mdl.RightFoot[0, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(-Global.Cell_H / 4, -Global.Cell_V), new Vector2(-Global.Cell_H / 2, 1), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.LeftArm[+120, t2].n();
                            mdl.LeftHand[+120, t2].n();
                            mdl.RightArm[-50, t2].n();
                            mdl.RightHand[10, t2].n();
                            mdl.LeftLeg[+35, t2].n();
                            mdl.LeftFoot[0, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-20, t2].n();
                            mdl.Weapon[40, t2].n();
                            mdl.Head[-15, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToForward:
                            #region Forward
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+35, t].n();
                            mdl.RightArm[+35, t].n();
                            mdl.RightHand[+45, t].n();
                            mdl.LeftArm[-50, t].n();
                            mdl.LeftHand[+45, t].n();
                            mdl.RightLeg[-40, t].n();
                            mdl.RightFoot[+10, t].n();
                            mdl.LeftLeg[+30, t].n();
                            mdl.LeftFoot[-70, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                           (new Vector2(0, -Global.Cell_V), new Vector2(0, -Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.RightArm[-25, t2].n();
                            mdl.RightHand[-50, t2].n();
                            mdl.LeftArm[+60, t2].n();
                            mdl.LeftHand[+30, t2].n();
                            mdl.RightLeg[-20, t2].n();
                            mdl.RightFoot[+60, t2].n();
                            mdl.LeftLeg[+40, t2].n();
                            mdl.LeftFoot[+10, t2].n();
                            mdl.Head[+45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                    }
                    TwiSound.PlayStep(mdl);
                    mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().ReverseCfg(t3);
                    mdl.ResetState(t3, Misc.Anim.Bezier3).WaitForFinish(t3);
                    mdl.RunIdleBreathScript();
                    TwiSound.PlayStep(mdl);

                    mdl.Mblur.Enabled = false;
                    TWE3.Gtwi.GameControlFullyDenied = false;
                });
            }
            #endregion
        }
    }
}