﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DWOaO;
using DWOaO.ActorModels;
using Microsoft.Xna.Framework;

namespace DWOaO.Life
{
    public sealed class DistanceAttack_Marco : AttackDistance_IntroBase
    {
        public DistanceAttack_Marco(Intrin marco)
            : base(marco) { Damage = 120; CoolDown = 1; }

        protected override void ScriptAction()
        {
            #region SkillAction
            {
                Owner.Model.CurrentModelScript.Terminate();
                var scr = Script.Start<Action>(() =>
                {
                    var mdl = Owner.Model as Humanoid_mdl;
                    mdl.Mblur.Length = 30;
                    mdl.Mblur.Enabled = true;
                    TWGE3.Gtwi.GameControlFullyDenied = true;
                    AnimFloat2 ballPos = new AnimFloat2(mdl.Weapon.ExtraMarkers[0].Value);
                    AnimFloat2 ballRad = new AnimFloat2(10, 10);
                    ballPos.Ctrl<Anim.V2SnapToCtrl>().Cfg(mdl.Weapon.ExtraMarkers[0]);
                    LightSource ballGlow = new LightSource(altitude: 10f, lightDistance: 300, lightIntensity: 0f);
                    ballGlow.Position.Value = mdl.Weapon.ExtraMarkers[0].Value + new Vector2(0, 100f);
                    ballGlow.Position.Ctrl<Anim.V2SnapToCtrl>().Cfg(mdl.Weapon.ExtraMarkers[0]);
                    TwiParticleSystem ball = new TwiParticleSystem(300, new Tuple<AnimFloat2, AnimFloat2>[] { Tuple.Create(ballPos, ballRad) },
                        particleTexName: "rg30", minTime: 50, maxTime: 600, opacity: 0f, parSize: 20);
                    TWE3.twi.RegisterObject(ball);
                    ball.PullInsideAllParticles();
                    ball.RGBMulti.Value = mdl.Actor.SideColor.ToVector3();
                    ballGlow.LightColor = ball.RGBMulti;
                    //ballGlow.LightIntensity = ball.Opacity;
                    TWE3.twi.RegisterLightSource(ballGlow);
                    float t = 650, t2 = 200, t3 = 500, t4 = 300;
                    ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0, 0.5f, t);
                    ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 1f, t);
                    ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(40, 40), t * 2);
                    mdl.selfGlowing.LightDistance.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 120f, t);
                    //ScriptControl.Wait(t);
                    //mdl.ResetState(t * .1f, Misc.Anim.Bezier3).WaitForFinish(t * .1f);
                    Vector2 d1 = new Vector2();
                    switch (mdl.Orientation)
                    {
                        case Orient.ToUs:
                            #region Us
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+40, t].n();
                            mdl.LeftFoot[-10, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Throw1");
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[+20, t2].n();
                            mdl.LeftFoot[-60, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2 / 2);
                            #endregion
                            break;
                        case Orient.ToRight:
                            #region Right
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+25, t].n();
                            mdl.LeftFoot[+30, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Throw1");
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[-40, t2].n();
                            mdl.LeftFoot[+80, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2 / 2);
                            #endregion
                            break;
                        case Orient.ToLeft:
                            #region Left
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+35, t].n();
                            mdl.LeftArm[+35, t].n();
                            mdl.LeftHand[+45, t].n();
                            mdl.RightArm[-60, t].n();
                            mdl.RightHand[-20, t].n();
                            mdl.LeftLeg[-40, t].n();
                            mdl.LeftFoot[-20, t].n();
                            mdl.RightLeg[+60, t].n();
                            mdl.RightFoot[-90, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Throw1");
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.LeftArm[-25, t2].n();
                            mdl.LeftHand[-50, t2].n();
                            mdl.RightArm[+40, t2].n();
                            mdl.RightHand[+20, t2].n();
                            mdl.LeftLeg[+40, t2].n();
                            mdl.LeftFoot[-80, t2].n();
                            mdl.RightLeg[+40, t2].n();
                            mdl.RightFoot[+10, t2].n();
                            mdl.Head[+45, t2].WaitForFinish(t2 / 2);
                            #endregion
                            break;
                        case Orient.ToForward:
                            #region Forward
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+35, t].n();
                            mdl.RightArm[+35, t].n();
                            mdl.RightHand[+45, t].n();
                            mdl.LeftArm[-50, t].n();
                            mdl.LeftHand[+45, t].n();
                            mdl.RightLeg[-40, t].n();
                            mdl.RightFoot[+10, t].n();
                            mdl.LeftLeg[+30, t].n();
                            mdl.LeftFoot[-70, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Throw1");
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.RightArm[-25, t2].n();
                            mdl.RightHand[-50, t2].n();
                            mdl.LeftArm[+60, t2].n();
                            mdl.LeftHand[+30, t2].n();
                            mdl.RightLeg[-20, t2].n();
                            mdl.RightFoot[+60, t2].n();
                            mdl.LeftLeg[+40, t2].n();
                            mdl.LeftFoot[+10, t2].n();
                            mdl.Head[+45, t2].WaitForFinish(t2 / 2);
                            #endregion
                            break;
                    }
                    if (enemy.CurrentDefence == null) enemy.ActivateDefaultDefence();
                    enemy.CurrentDefence.PerformDefence(Owner, Damage, t3);
                    ball.RGBMulti.Ctrl<Anim.V3SmplCtrl>().Cfg(Misc.Anim.Linear, mdl.Actor.SideColor.ToVector3(), Reservoir.EnergyColor.ToVector3(), t3);
                    ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 1f, t3);
                    ballPos.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(ballPos.Value + d1, enemy.Model.Center, t3);
                    TwiSound.PlayStep(mdl);
                    ballGlow.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2(ballPos.Value + d1, enemy.Model.Center + new Vector2(0, 100f), t3)
                        .WaitForFinish(t2 / 2 + t3);


                    ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 2f, t4);
                    ball.Opacity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Linear, 0.0f, t3 * 2);
                    ballRad.Ctrl<Anim.V2SmplCtrl>().Cfg(Misc.Anim.Bezier3, new Vector2(300), t3);
                    mdl.ResetState(600f, Misc.Anim.Bezier3);//
                    mdl.selfGlowing.LightDistance.Ctrl<Anim.FCtrl>().CfgReverse(600f).WaitForFinish(t3);
                    mdl.RunIdleBreathScript();
                    TwiSound.PlayStep(mdl);
                    ballGlow.LightIntensity.Ctrl<Anim.FCtrl>().Cfg(Misc.Anim.Bezier3, 0f, t4).WaitForFinish(t3 * 2 - t4); ;

                    TWE3.twi.UnregisterObject(ball);
                    TWE3.twi.UnregisterLightSource(ballGlow);
                    mdl.Mblur.Enabled = false;
                    TWGE3.Gtwi.GameControlFullyDenied = false;
                });
            }
            #endregion
        }
    }

    public sealed class OccupySpring_Marco : OccupySpring_Base
    {
        public OccupySpring_Marco(Intrin marco)
            : base(marco) { }

        protected override void ScriptAction()
        {
            var act = Owner;
            if (act.Model.CurrentModelScript != null) act.Model.CurrentModelScript.Terminate();
            act.Model.CurrentModelScript = Script.Start<Action>(() =>
                {
                    TWE3.Gtwi.GameControlFullyDenied = true;
                    Humanoid_mdl A = Owner.Model as Humanoid_mdl;
                    float t = 600;
                    switch (A.Orientation)
                    {
                        case Orient.ToUs:
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, 30), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                            }
                            A.Body[-15, t, Misc.Anim.Bezier3].n();
                            A.Head[+20, t, Misc.Anim.Bezier3].n();
                            A.RightLeg[+20, t, Misc.Anim.Bezier3].n();
                            A.RightFoot[+45, t, Misc.Anim.Bezier3].n();
                            A.LeftLeg[+17, t, Misc.Anim.Bezier3].n();
                            A.LeftFoot[-30, t, Misc.Anim.Bezier3].n();
                            A.LeftArm[+20, t, Misc.Anim.Bezier3].n();
                            A.LeftHand[+20, t, Misc.Anim.Bezier3].n();
                            A.Weapon[+90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                            source.OccupyEnergySource(act);
                            ScriptControl.Wait(t);
                            TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -30), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                            }
                            A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            break;
                        case Orient.ToRight:
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(5, 20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                            }
                            A.Body[+55, t, Misc.Anim.Bezier3].n();
                            A.LeftLeg[-90, t, Misc.Anim.Bezier3].n();
                            A.LeftFoot[+70, t, Misc.Anim.Bezier3].n();
                            A.RightLeg[-115, t, Misc.Anim.Bezier3].n();
                            A.RightFoot[+60, t, Misc.Anim.Bezier3].n();
                            A.Head[-40, t, Misc.Anim.Bezier3].n();
                            A.LeftArm[-60, t, Misc.Anim.Bezier3].n();
                            A.Weapon[+60, t, Misc.Anim.Bezier3].WaitForFinish(t);
                            source.OccupyEnergySource(act);
                            ScriptControl.Wait(t);
                            TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, -20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                            }
                            A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            break;
                        case Orient.ToLeft:
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, +20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                            }
                            A.Body[-55, t, Misc.Anim.Bezier3].n();
                            A.LeftLeg[+90, t, Misc.Anim.Bezier3].n();
                            A.LeftFoot[-70, t, Misc.Anim.Bezier3].n();
                            A.RightLeg[+80, t, Misc.Anim.Bezier3].n();
                            A.RightFoot[-50, t, Misc.Anim.Bezier3].n();
                            A.Head[+40, t, Misc.Anim.Bezier3].n();
                            A.LeftArm[+60, t, Misc.Anim.Bezier3].n();
                            A.LeftHand[-30, t, Misc.Anim.Bezier3].n();
                            A.Weapon[-90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                            source.OccupyEnergySource(act);
                            ScriptControl.Wait(t);
                            TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(+5, -20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                            }
                            A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            break;
                        case Orient.ToForward:
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                            }
                            A.Body[+15, t, Misc.Anim.Bezier3].n();
                            A.Head[-20, t, Misc.Anim.Bezier3].n();
                            A.LeftLeg[-20, t, Misc.Anim.Bezier3].n();
                            A.LeftFoot[-45, t, Misc.Anim.Bezier3].n();
                            A.RightLeg[-17, t, Misc.Anim.Bezier3].n();
                            A.RightFoot[+30, t, Misc.Anim.Bezier3].n();
                            A.RightArm[-20, t, Misc.Anim.Bezier3].n();
                            A.RightHand[-20, t, Misc.Anim.Bezier3].n();
                            A.Weapon[+20, t, Misc.Anim.Bezier3].WaitForFinish(t);
                            source.OccupyEnergySource(act);
                            ScriptControl.Wait(t);
                            TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                            if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                            {
                                A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, +20), t);
                                A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                            }
                            A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                            break;
                    }
                    act.Model.ResetModelState();
                    TWE3.Gtwi.GameControlFullyDenied = false;
                    (act.Model as Humanoid_mdl).RunIdleBreathScript();
                });
        }
    }
    public sealed class OccupySpring_ExInc : OccupySpring_Base
    {
        public OccupySpring_ExInc(ExtraInc sol)
            : base(sol) { }

        protected override void ScriptAction()
        {
            var act = Owner;
            if (act.Model.CurrentModelScript != null) act.Model.CurrentModelScript.Terminate();
            act.Model.CurrentModelScript = Script.Start<Action>(() =>
            {
                TWE3.Gtwi.GameControlFullyDenied = true;
                Humanoid_mdl A = Owner.Model as Humanoid_mdl;
                float t = 600;
                switch (A.Orientation)
                {
                    case Orient.ToUs:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, 30), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[-15, t, Misc.Anim.Bezier3].n();
                        A.Head[+20, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[+20, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+45, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[+17, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-30, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[+20, t, Misc.Anim.Bezier3].n();
                        A.LeftHand[+20, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -30), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToRight:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(5, 20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[+55, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[-90, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[+70, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[-115, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+60, t, Misc.Anim.Bezier3].n();
                        A.Head[-40, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[-60, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+60, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToLeft:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(-5, +20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[-55, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[+90, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-70, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[+80, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[-50, t, Misc.Anim.Bezier3].n();
                        A.Head[+40, t, Misc.Anim.Bezier3].n();
                        A.LeftArm[+60, t, Misc.Anim.Bezier3].n();
                        A.LeftHand[-30, t, Misc.Anim.Bezier3].n();
                        A.Weapon[-90, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(+5, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                    case Orient.ToForward:
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, -20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, 40.0f, t);
                        }
                        A.Body[+15, t, Misc.Anim.Bezier3].n();
                        A.Head[-20, t, Misc.Anim.Bezier3].n();
                        A.LeftLeg[-20, t, Misc.Anim.Bezier3].n();
                        A.LeftFoot[-45, t, Misc.Anim.Bezier3].n();
                        A.RightLeg[-17, t, Misc.Anim.Bezier3].n();
                        A.RightFoot[+30, t, Misc.Anim.Bezier3].n();
                        A.RightArm[-20, t, Misc.Anim.Bezier3].n();
                        A.RightHand[-20, t, Misc.Anim.Bezier3].n();
                        A.Weapon[+20, t, Misc.Anim.Bezier3].WaitForFinish(t);
                        source.OccupyEnergySource(act);
                        ScriptControl.Wait(t);
                        TwiSound.SysSpeak(act.SystemName, "SourceOccuped");
                        if (act.Model.GetField().Attribute.SurfaceType != MapSystem.FieldSurfaceType.Liquid)
                        {
                            A.Position.Ctrl<Anim.V2SmplCtrl>().CfgAdd(Misc.Anim.Bezier3, new Vector2(0, +20), t);
                            A.selfGlowing.Altitude.Ctrl<Anim.FCtrl>().CfgAdd(Misc.Anim.Bezier3, -40.0f, t);
                        }
                        A.ResetState(t, Misc.Anim.Bezier3).WaitForFinish(t);
                        break;
                }
                act.Model.ResetModelState();
                TWE3.Gtwi.GameControlFullyDenied = false;
                (act.Model as Humanoid_mdl).RunIdleBreathScript();
            });
        }
    }

    public sealed class AttackClose_IntroInc : AttackClose_ExtraBase
    {
        public AttackClose_IntroInc(IntroInc inc)
            : base(inc)
        {
            Damage = 60;
        }

        protected override void ScriptAction()
        {
            #region SkillAction
            {
                Owner.Model.CurrentModelScript.Terminate();
                var scr = Script.Start<Action>(() =>
                {
                    var mdl = Owner.Model as Humanoid_mdl;
                    mdl.Mblur.Length = 30;
                    mdl.Mblur.Enabled = true;
                    TWE3.Gtwi.GameControlFullyDenied = true;
                    int t = 500, t2 = 150, t3 = 500;
                    //ScriptControl.Wait(t);
                    //mdl.ResetState(t * .1f, Misc.Anim.Bezier3).WaitForFinish(t * .1f);
                    if (enemy.CurrentDefence == null) enemy.ActivateDefaultDefence();
                    enemy.CurrentDefence.PerformDefence(Owner, Damage, t + t2 * 2);
                    Vector2 d1 = new Vector2();

                    switch (mdl.Orientation)
                    {
                        case Orient.ToUs:
                            #region Us
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+40, t].n();
                            mdl.LeftFoot[-10, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(0, -Global.Cell_V), new Vector2(0, Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[+20, t2].n();
                            mdl.LeftFoot[-60, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToRight:
                            #region Right
                            d1 = new Vector2(200);
                            mdl.Body[-35, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-45, t].n();
                            mdl.RightArm[+50, t].n();
                            mdl.RightHand[-45, t].n();
                            mdl.LeftLeg[+25, t].n();
                            mdl.LeftFoot[+30, t].n();
                            mdl.RightLeg[-30, t].n();
                            mdl.RightFoot[+70, t].n();
                            mdl.Head[45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(Global.Cell_H / 4, -Global.Cell_V), new Vector2(Global.Cell_H / 2, 0), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[+35, t2].n();
                            mdl.LeftArm[+25, t2].n();
                            mdl.LeftHand[+50, t2].n();
                            mdl.RightArm[-60, t2].n();
                            mdl.RightHand[-30, t2].n();
                            mdl.LeftLeg[-40, t2].n();
                            mdl.LeftFoot[+80, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-10, t2].n();
                            mdl.Head[-45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToLeft:
                            #region Left
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+20, t].n();
                            mdl.LeftArm[-35, t].n();
                            mdl.LeftHand[-5, t].n();
                            mdl.RightArm[+20, t].n();
                            mdl.RightHand[+5, t].n();
                            mdl.LeftLeg[+50, t].n();
                            mdl.LeftFoot[-80, t].n();
                            mdl.RightLeg[-20, t].n();
                            mdl.RightFoot[0, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                            (new Vector2(-Global.Cell_H / 4, -Global.Cell_V), new Vector2(-Global.Cell_H / 2, 1), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.LeftArm[+120, t2].n();
                            mdl.LeftHand[+120, t2].n();
                            mdl.RightArm[-50, t2].n();
                            mdl.RightHand[10, t2].n();
                            mdl.LeftLeg[+35, t2].n();
                            mdl.LeftFoot[0, t2].n();
                            mdl.RightLeg[-40, t2].n();
                            mdl.RightFoot[-20, t2].n();
                            mdl.Weapon[40, t2].n();
                            mdl.Head[-15, t2].WaitForFinish(t2);
                            #endregion
                            break;
                        case Orient.ToForward:
                            #region Forward
                            d1 = new Vector2(-200, -200);
                            mdl.Body[+35, t].n();
                            mdl.RightArm[+35, t].n();
                            mdl.RightHand[+45, t].n();
                            mdl.LeftArm[-50, t].n();
                            mdl.LeftHand[+45, t].n();
                            mdl.RightLeg[-40, t].n();
                            mdl.RightFoot[+10, t].n();
                            mdl.LeftLeg[+30, t].n();
                            mdl.LeftFoot[-70, t].n();
                            mdl.Head[-45, t].WaitForFinish(t);

                            TwiSound.PlaySnd("Sword1");
                            mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().Cfg2Rel
                           (new Vector2(0, -Global.Cell_V), new Vector2(0, -Global.Cell_V / 2), t2 * 2).WaitForFinish(t2);
                            mdl.ResetState(t2);
                            mdl.Body[-35, t2].n();
                            mdl.RightArm[-25, t2].n();
                            mdl.RightHand[-50, t2].n();
                            mdl.LeftArm[+60, t2].n();
                            mdl.LeftHand[+30, t2].n();
                            mdl.RightLeg[-20, t2].n();
                            mdl.RightFoot[+60, t2].n();
                            mdl.LeftLeg[+40, t2].n();
                            mdl.LeftFoot[+10, t2].n();
                            mdl.Head[+45, t2].WaitForFinish(t2);
                            #endregion
                            break;
                    }
                    TwiSound.PlayStep(mdl);
                    mdl.Position.Ctrl<Anim.V2BezierPathCtrl>().ReverseCfg(t3);
                    mdl.ResetState(t3, Misc.Anim.Bezier3).WaitForFinish(t3);
                    mdl.RunIdleBreathScript();
                    TwiSound.PlayStep(mdl);

                    mdl.Mblur.Enabled = false;
                    TWE3.Gtwi.GameControlFullyDenied = false;
                });
            }
            #endregion
        }
    }
}
