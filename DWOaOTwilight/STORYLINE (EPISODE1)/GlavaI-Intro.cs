﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Graphics;
using DWOaO.Misc;

namespace DWOaO.Life.Storyline.Episode1
{
    [Serializable]
    [DistortedWorldMission(0, "Рождение интрина")]
    public class Glava_I_Intro : TWGE3
    {
        Intrin MarcoJiff2;
        Extral BomberIl228;

        public Glava_I_Intro()
        {
        }

        private void InitMarco()
        {
            MarcoJiff2 = new Intrin("MarcoJiff2", "Марко Джифф-2", Color.FromNonPremultiplied(10, 255, 180, 255),
                140, 400, 180, 60, 20, 60);
            MarcoJiff2.DefaultSkills.AddSkill(new Incarnate_Uni(MarcoJiff2));
            MarcoJiff2.Sintes.AddSkill(new OccupySpring_Marco(MarcoJiff2));
            MarcoJiff2.Chaos.AddSkill(new DistanceAttack_Marco(MarcoJiff2));
            MarcoJiff2.Sintes.AddSkill(new Create_Intro(MarcoJiff2));
            MarcoJiff2.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet("MarcoJiff2"), MarcoJiff2);
            var MarcoModel = MarcoJiff2.Model as ActorModels.Humanoid_mdl;
            MarcoModel.Orientation = (ActorModels.Orient.ToRight);
            MarcoModel.PlaceOnField(6, 15);
            var q = QueryAnyGameElemsAround(MarcoModel.GetField(), 6, typeof(EnergySource));
            foreach (EnergySource es in q)
            {
                es.OccupyEnergySource(MarcoJiff2);
            }
            //MarcoModel.PlaceOnField(3+5, 5);

            MarcoModel.RunIdleBreathScript();
        }

        private void InitBomber()
        {
            BomberIl228 = new Extral("BomberIl228", "Бомбер Ил-228", Color.OrangeRed, 80, 800, 40, 20);
            BomberIl228.SideColor = Color.OrangeRed;//Color.FromNonPremultiplied(10, 255, 180, 255);
            //BomberIl228.Chaos.AddSkill(new OccupySpring_Base(BomberIl228));
            BomberIl228.DefaultSkills.AddSkill(new Incarnate_Uni(BomberIl228));
            BomberIl228.Chaos.AddSkill(new AttackClose_Bomber(BomberIl228));

            BomberIl228.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet("BomberIl228"), BomberIl228);

            var BomberModel = BomberIl228.Model as ActorModels.Humanoid_mdl;
            BomberModel.Orientation = (ActorModels.Orient.ToLeft);
            BomberModel.PlaceOnField(17, 14);
            var q = QueryAnyGameElemsAround(BomberModel.GetField(), 9, typeof(EnergySource));
            foreach (EnergySource es in q)
            {
                es.OccupyEnergySource(BomberIl228);
            }
            //BomberModel.PlaceOnField(3+5-1, 5);
            BomberModel.RunIdleBreathScript();

            ExtraIncarnator ei = new ExtraIncarnator(BomberIl228, 17, 13);//new ExtraIncarnator(BomberIl228, 28, 14);
            ei.Sintes.Capacity = 300;
            ei.Sintes.CurrentValue = 300;
            ei.Sintes.MaxPhaseConsumption = 120;
            ei.Sintes.AddSkill(new Create_Extra(ei));

            InitBomberSoldierIncs();
            InitMarcoSoldierIncs();
        }

        private void InitBomberSoldierIncs()
        {
            for (int x = 0; x < 2; x++)
            {
                for (int y = 0; y < 2; y++)
                {
                    var f = BomberIl228.GetField().FieldPos;
                    ExtraInc soldier = new ExtraInc(BomberIl228);
                    soldier.Model.PlaceOnField(f.X - 2 + x * 4, f.Y - 2 + y * 4);
                    soldier.Model.Orientation = UF.GetOrient(soldier.Model.Center, MarcoJiff2.Model.Center);
                    soldier.Model.RunIdleBreathScript();
                }
            }
            //soldier
        }
        private void InitMarcoSoldierIncs()
        {
            for (int x = 0; x < 2; x++)
            {
                for (int y = 0; y < 2; y++)
                {
                    var f = MarcoJiff2.GetField().FieldPos;
                    IntroInc soldier = new IntroInc(MarcoJiff2);
                    soldier.Model = new ActorModels.Humanoid_mdl(new ActorModels.ActorModelTextureSet(soldier.SystemName), soldier);
                    Point p = new Point(f.X - 2 + x * 4, f.Y - 2 + y * 4);
                    while (GameMap.FieldMatrix[p.X, p.Y].Settler != null)
                        p.X++;
                    soldier.Model.PlaceOnField(p.X, p.Y);
                    soldier.Model.Orientation = ActorModels.Orient.ToUs;//UF.GetOrient(soldier.Model.Center, MarcoJiff2.Model.Center);
                    soldier.Model.RunIdleBreathScript();
                }
            }
            //soldier
        }

        public override void Construct()
        {
            base.Construct();
            InitMarco();
            InitBomber();
            //InitBomberSoldierIncs();
            Omnit1 = BomberIl228;
            Omnit2 = MarcoJiff2;
            //Test();
            foreach (var l in LightSources)
                l.NeedReRenderLightMap = true;
            NewTurn();
            //GUI.ShowPanel(MarcoJiff2);
        }

        private void Test()
        {
            TwiParticleSystem ps = new TwiParticleSystem(300,
            new Tuple<AnimFloat2, AnimFloat2>[] {
                Tuple.Create((MarcoJiff2.Model as ActorModels.Humanoid_mdl).Weapon.ExtraMarkers[0],new AnimFloat2(60,60))/*,
                Tuple.Create(new AnimFloat2(MarcoJiff2.Model.Right,MarcoJiff2.Model.Top),new AnimFloat2(50,50)),
                Tuple.Create(new AnimFloat2(MarcoJiff2.Model.Left,MarcoJiff2.Model.Bottom),new AnimFloat2(50,50)),
                Tuple.Create(new AnimFloat2(MarcoJiff2.Model.Right,MarcoJiff2.Model.Bottom),new AnimFloat2(50,50))*/
            }, opacity: 0.35f, minTime: 50, maxTime: 200);
            ps.RGBMulti.Value = MarcoJiff2.SideColor.ToVector3();
            //foreach (var c in ps.SystemCenters)
            //    c.Item1.Ctrl<Anim.V2SnapToCtrl>().Cfg(MarcoJiff2.Model.Position);
            RegisterObject(ps);
        }
    }
}