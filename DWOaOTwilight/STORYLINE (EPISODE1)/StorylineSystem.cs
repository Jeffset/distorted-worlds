﻿using System;

namespace DWOaO.Life
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class DistortedWorldMissionAttribute : Attribute
    {
        public int MissionNumber { get; private set; }
        public string FriendlyName { get; private set; }

        public DistortedWorldMissionAttribute(int missionNumber, string friendlyName)
        {
        }
    }
}